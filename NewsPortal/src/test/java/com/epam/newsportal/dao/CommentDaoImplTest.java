package com.epam.newsportal.dao;

import com.epam.newsportal.domain.Comment;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Timestamp;
import java.util.List;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;

/**
 * Public class <code>CommentDaoImplTest</code> content
 * list of public Data Base Unit tests, which check
 * right work of Data Access layer class <code>CommentDao</code>
 * methods.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.dao.CommentDao
 * @see com.epam.newsportal.domain.Comment
 * @since 1.0
 */

@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@ContextConfiguration(locations = {"/spring.test.config.xml"})
@DatabaseSetup(value = "/data.before.operations.xml")
@DatabaseTearDown(value = "/clean.database.xml")
public class CommentDaoImplTest {

    /**
     * reference on CommentDao class, to use it all public methods
     */
    @Autowired
    private CommentDao commentDao;

    /**
     * comment object to save data after AuthorDao operations
     */
    @Autowired
    private Comment comment;

    /**
     * path to dataSets for comparison after DELETE operation
     */
    private static final String DATA_AFTER_DELETE_PATH = "/data.after.delete.xml";

    /**
     * comment table name in data base
     */
    private static final String COMMENT_TABLE_NAME = "COMMENTS";

    /**
     * Public Data Base Unit test, that verify work create()
     * method in the CommentDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void create() throws Exception {
        long insertedCommentId = 0;

        comment.setCommentText("FourthText");
        comment.setCreationDate(Timestamp.valueOf("2015-03-30 03:45:24"));
        comment.setNewsId(Long.valueOf(2));
        insertedCommentId = commentDao.create(comment);

        assertThat(commentDao.read(insertedCommentId), allOf(
                hasProperty("commentText", is("FourthText")),
                hasProperty("creationDate", is(Timestamp.valueOf("2015-03-30 03:45:24"))),
                hasProperty("newsId", is(2L))
        ));
    }

    /**
     * Public Data Base Unit test, that verify work read()
     * method in the CommentDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void read() throws Exception {

        comment = commentDao.read(1);
        assertThat(comment, allOf(
                hasProperty("commentText", is("FirstText")),
                hasProperty("creationDate", is(Timestamp.valueOf("2015-03-30 03:45:24"))),
                hasProperty("newsId", is(2L))
        ));
    }

    /**
     * Public Data Base Unit test, that verify work readAll()
     * method in the CommentDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void readAll() throws Exception {

        List<Comment> commentList = commentDao.readAll();

        assertThat(commentList.get(1), allOf(
                hasProperty("commentText", is("SecondText")),
                hasProperty("creationDate", is(Timestamp.valueOf("2015-03-30 03:45:24"))),
                hasProperty("newsId", is(2L))
        ));
    }

    /**
     * Public Data Base Unit test, that verify work update()
     * method in the CommentDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void update() throws Exception {

        comment.setCommentId(Long.valueOf(1));
        comment.setCommentText("SuperNewText");
        comment.setCreationDate(Timestamp.valueOf("2015-03-29 03:45:24"));
        comment.setNewsId(Long.valueOf(2));
        commentDao.update(comment);

        assertThat(comment = commentDao.read(1), allOf(
                hasProperty("commentText", is("SuperNewText")),
                hasProperty("creationDate", is(Timestamp.valueOf("2015-03-29 03:45:24"))),
                hasProperty("newsId", is(2L))
        ));
    }

    /**
     * Public Data Base Unit test, that verify work delete()
     * method in the CommentDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    @ExpectedDatabase(value = DATA_AFTER_DELETE_PATH, table = COMMENT_TABLE_NAME,
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void delete() throws Exception {

        commentDao.delete(1);
    }
}
