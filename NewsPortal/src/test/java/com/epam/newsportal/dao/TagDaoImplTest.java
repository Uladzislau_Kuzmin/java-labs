package com.epam.newsportal.dao;

import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.domain.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Public class <code>TagDaoImplTest</code> content
 * list of public Data Base Unit tests, which check
 * right work of Data Access layer class <code>TagDao</code>
 * methods.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.dao.TagDao
 * @see com.epam.newsportal.domain.Tag
 * @since 1.0
 */

@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@ContextConfiguration(locations = {"/spring.test.config.xml"})
@DatabaseSetup(value = "/data.before.operations.xml")
@DatabaseTearDown(value = "/clean.database.xml")
public class TagDaoImplTest {

    /**
     * reference on NewsDao class, to use it all public methods
     */
    @Autowired
    private TagDao tagDao;

    /**
     * tag object to save data after AuthorDao operations
     */
    @Autowired
    private Tag tag;

    /**
     * author object to save data after AuthorDao operations
     */
    @Autowired
    private Author author;

    /**
     * news object to save data after AuthorDao operations
     */
    @Autowired
    private News news;

    /**
     * path to dataSets for comparison after CREATE operation
     */
    private static final String DATA_AFTER_CREATE_PATH = "/data.after.create.xml";

    /**
     * path to dataSets for comparison after DELETE operation
     */
    private static final String DATA_AFTER_DELETE_PATH = "/data.after.delete.xml";

    /**
     * table names in data base
     */
    private static final String TAG_TABLE_NAME = "TAG";
    private static final String NEWS_TAG_TABLE_NAME = "NEWS_TAG";

    /**
     * Public Data Base Unit test, that checks work create()
     * method in the TagDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void create() throws Exception {
        long insertedTagId = 0;

        tag.setTagName("CustomerTag");
        insertedTagId = tagDao.create(tag);

        assertThat(tagDao.read(insertedTagId).getTagName(), is("CustomerTag"));
    }

    /**
     * Public Data Base Unit test, that checks work read()
     * method in the TagDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void read() throws Exception {

        assertThat(tagDao.read(1).getTagName(), is("FirstTag"));
    }

    /**
     * Public Data Base Unit test, that checks work readAll()
     * method in the TagDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void readAll() throws Exception {

        List<Tag> tagList = tagDao.readAll();
        assertThat(tagList.get(1).getTagName(), is("SecondTag"));
    }

    /**
     * Public Data Base Unit test, that checks work update()
     * method in the TagDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void update() throws Exception {

        tag.setTagId(Long.valueOf(1));
        tag.setTagName("SuperNewTag");
        tagDao.update(tag);

        assertThat(tagDao.read(1).getTagName(), is("SuperNewTag"));
    }

    /**
     * Public Data Base Unit test, that checks work delete()
     * method in the TagDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    @ExpectedDatabase(value = DATA_AFTER_DELETE_PATH, table = TAG_TABLE_NAME,
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void delete() throws Exception {

        tagDao.delete(1);
    }

    /**
     * Public Data Base Unit test, that checks work searchTagByAuthor()
     * method in the TagDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void searchTagByAuthor() throws Exception {
        List<Tag> tagList = null;

        author.setAuthorId(Long.valueOf(1));
        tagList = tagDao.searchTagByAuthor(author);

        assertThat(tagList.get(0).getTagName(), is("FirstTag"));
    }

    /**
     * Public Data Base Unit test, that checks work createTagForNews()
     * method in the TagDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    @ExpectedDatabase(value = DATA_AFTER_CREATE_PATH, table = NEWS_TAG_TABLE_NAME,
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void createTagForNews() throws Exception {

        news.setNewsId(Long.valueOf(3));
        tag.setTagId(Long.valueOf(3));
        tagDao.createTagForNews(news, tag);
    }
}
