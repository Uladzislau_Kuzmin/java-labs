package com.epam.newsportal.dao;

import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.List;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;

/**
 * Public class <code>AuthorDaoImplTest</code> content
 * list of public Data Base Unit tests, which check
 * right work of Data Access layer class <codes>AuthorDao</code>
 * methods.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.dao.AuthorDao
 * @see com.epam.newsportal.domain.Author
 * @since 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@ContextConfiguration(locations = {"/spring.test.config.xml"})
@DatabaseSetup(value = "/data.before.operations.xml")
@DatabaseTearDown(value = "/clean.database.xml")
public class AuthorDaoImplTest {

    /**
     * reference on AuthorDao class, to use it all public methods
     */
    @Autowired
    private AuthorDao authorDao;

    /**
     * author object to save data after AuthorDao operations
     */
    @Autowired
    private Author author;

    /**
     * news object to save data after AuthorDao operations
     */
    @Autowired
    private News news;

    /**
     * path to dataSets for comparison after CREATE operation
     */
    private static final String DATA_AFTER_CREATE_PATH = "/data.after.create.xml";

    /**
     * path to dataSets for comparison after DELETE operation
     */
    private static final String DATA_AFTER_DELETE_PATH = "/data.after.delete.xml";

    /**
     * table names in data base
     */
    private static final String AUTHOR_TABLE_NAME = "AUTHOR";
    private static final String NEWS_AUTHOR_TABLE_NAME = "NEWS_AUTHOR";

    /**
     * Public Data Base Unit test, that verify work create()
     * method in the AuthorDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void create() throws Exception {
        long insertedAuthorId = 0;

        author.setAuthorName("Taylor");
        insertedAuthorId = authorDao.create(author);
        System.out.println(insertedAuthorId);
        assertThat(authorDao.read(insertedAuthorId).getAuthorName(), is("Taylor"));
    }

    /**
     * Public Data Base Unit test, that verify work read()
     * method in the AuthorDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void read() throws Exception {

        author = authorDao.read(1);

        assertThat(author.getAuthorName(), is("Bob"));
    }

    /**
     * Public Data Base Unit test, that verify work readAll()
     * method in the AuthorDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void readAll() throws Exception {

        List<Author> authorList = authorDao.readAll();

        assertThat(authorList.size(), is(4));
        assertThat(authorList.get(2), allOf(
                hasProperty("authorId", is(3L)),
                hasProperty("authorName", is("Robby"))
        ));
    }

    /**
     * Public Data Base Unit test, that verify work update()
     * method in the AuthorDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void update() throws Exception {

        author.setAuthorId(Long.valueOf(1));
        author.setAuthorName("SuperNewBob");
        authorDao.update(author);

        assertThat(authorDao.read(1).getAuthorName(), Matchers.is("SuperNewBob"));
    }

    /**
     * Public Data Base Unit test, that verify work delete()
     * method in the AuthorDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    @ExpectedDatabase(value = DATA_AFTER_DELETE_PATH, table = AUTHOR_TABLE_NAME,
                assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
        public void delete() throws Exception {

        authorDao.delete(1);
    }

    /**
     * Public Data Base Unit test, that verify work createAuthorForNews()
     * method in the AuthorDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    @ExpectedDatabase(value = DATA_AFTER_CREATE_PATH, table = NEWS_AUTHOR_TABLE_NAME,
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void createAuthorForNews() throws Exception {

        news.setNewsId(Long.valueOf(3));
        author.setAuthorId(Long.valueOf(3));
        authorDao.createAuthorForNews(news, author);
    }


}
