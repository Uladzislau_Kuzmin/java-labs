package com.epam.newsportal.dao;

import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.domain.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;


import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Public class <code>NewsDaoImplTest</code> content
 * list of public Data Base Unit tests, which check
 * right work of Data Access layer class <code>NewsDao</code>
 * methods.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.dao.NewsDao
 * @see com.epam.newsportal.domain.News
 * @since 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@ContextConfiguration(locations = {"/spring.test.config.xml"})
@DatabaseSetup(value = "/data.before.operations.xml")
@DatabaseTearDown(value = "/clean.database.xml")
public class NewsDaoImplTest {

    /**
     * reference on NewsDao class, to use it all public methods
     */
    @Autowired
    private NewsDao newsDao;

    /**
     * news object to save data after AuthorDao operations
     */
    @Autowired
    private News news;

    /**
     * author object to save data after AuthorDao operations
     */
    @Autowired
    private Author author;

    /**
     * tag object to save data after AuthorDao operations
     */
    @Autowired
    private Tag tag;

    /**
     * path to dataSets for comparison after DELETE operation
     */
    private static final String DATA_AFTER_DELETE_PATH = "/data.after.delete.xml";

    /**
     * news name in data base
     */
    private static final String NEWS_TABLE_NAME = "NEWS";

    /**
     * Public Data Base Unit test, that verify work create()
     * method in the NewsDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void create() throws Exception {
        long insertedNewsId = 0;

        news.setShortText("SH4");
        news.setFullText("FL4");
        news.setTitle("T4");
        news.setCreationDate(Timestamp.valueOf("2015-03-30 03:45:24"));
        news.setModificationDate(Date.valueOf("2015-03-30"));
        insertedNewsId = newsDao.create(news);

        news = newsDao.read(insertedNewsId);
        assertThat(newsDao.read(insertedNewsId), allOf(
                hasProperty("shortText", is("SH4")),
                hasProperty("fullText", is("FL4")),
                hasProperty("title", is("T4")),
                hasProperty("creationDate", is(Timestamp.valueOf("2015-03-30 03:45:24"))),
                hasProperty("modificationDate", is(Date.valueOf("2015-03-30")))
        ));
    }

    /**
     * Public Data Base Unit test, that verify work read()
     * method in the NewsDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void read() throws Exception {

        assertThat(newsDao.read(1), allOf(
                hasProperty("shortText", is("SH1")),
                hasProperty("fullText", is("FL1")),
                hasProperty("title", is("T1")),
                hasProperty("creationDate", is(Timestamp.valueOf("2015-03-30 03:45:24"))),
                hasProperty("modificationDate", is(Date.valueOf("2015-03-30")))
        ));
    }

    /**
     * Public Data Base Unit test, that verify work readAll()
     * method in the NewsDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void readAll() throws Exception {
        List<News> newsList = newsDao.readAll();

        assertThat(newsList.size(), is(3));
        assertThat(newsList.get(2), allOf(
                hasProperty("shortText", is("SH3")),
                hasProperty("fullText", is("FL3")),
                hasProperty("title", is("T3")),
                hasProperty("creationDate", is(Timestamp.valueOf("2015-03-30 03:45:24"))),
                hasProperty("modificationDate", is(Date.valueOf("2015-03-30")))
        ));
    }

    /**
     * Public Data Base Unit test, that verify work update()
     * method in the NewsDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void update() throws Exception {

        news.setNewsId(Long.valueOf(1));
        news.setShortText("NEW_SH1");
        news.setFullText("NEW_FL1");
        news.setTitle("NEW_T1");
        news.setCreationDate(Timestamp.valueOf("2015-03-29 03:45:24"));
        news.setModificationDate(Date.valueOf("2015-03-29"));
        newsDao.update(news);

        assertThat(newsDao.read(1), allOf(
                hasProperty("shortText", is("NEW_SH1")),
                hasProperty("fullText", is("NEW_FL1")),
                hasProperty("title", is("NEW_T1")),
                hasProperty("creationDate", is(Timestamp.valueOf("2015-03-29 03:45:24"))),
                hasProperty("modificationDate", is(Date.valueOf("2015-03-29")))
        ));
    }

    /**
     * Public Data Base Unit test, that verify work delete()
     * method in the NewsDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    @ExpectedDatabase(value = DATA_AFTER_DELETE_PATH, table = NEWS_TABLE_NAME,
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void delete() throws Exception {

        newsDao.delete(1);
    }

    /**
     * Public Data Base Unit test, that verify work searchNewsByAuthor()
     * method in the NewsDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void searchNewsByAuthor() throws Exception {
        List<News> newsList = null;

        author.setAuthorId(Long.valueOf(1));
        newsList = newsDao.searchNewsByAuthor(author);

        assertThat(newsList.get(0).getNewsId(), is(1L));
        assertThat(newsList.get(1).getNewsId(), is(2L));
    }

    /**
     * Public Data Base Unit test, that verify work searchNewsByTag()
     * method in the NewsDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void searchNewsByTag() throws Exception {
        List<News> newsList = null;

        tag.setTagId(Long.valueOf(1));
        newsList = newsDao.searchNewsByTag(tag);

        assertThat(newsList.get(0).getNewsId(), is(1L));
        assertThat(newsList.get(1).getNewsId(), is(2L));
    }

    /**
     * Public Data Base Unit test, that verify work sortedNewsByComments()
     * method in the NewsDaoImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void sortedNewsByComments() throws Exception {
        List<News> newsList = null;

        newsList = newsDao.sortedNewsByComments();
        assertThat(newsList.get(0).getNewsId(), is(2L));
        assertThat(newsList.get(1).getNewsId(), is(3L));
        assertThat(newsList.get(2).getNewsId(), is(1L));
    }
}
