package com.epam.newsportal.service;

import com.epam.newsportal.dao.AuthorDao;
import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.service.implementation.AuthorServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

/**
 * Public class <code>AuthorServiceImplTest</code> content
 * list of public Mockito Unit tests, which check
 * right work of Service layer class <code>AuthorServiceImpl</code>
 * methods.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.service.AuthorService
 * @see com.epam.newsportal.service.implementation.AuthorServiceImpl
 * @since 1.0
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration("/spring.config.xml")
public class AuthorServiceImplTest {

    /**
     * mock object, which will be inject into AuthorServiceImpl class
     */
    @Mock
    private AuthorDao authorDao;

    /**
     * service layer object, which we will testing
     */
    @Autowired
    @InjectMocks
    private AuthorServiceImpl authorService;

    /**
     * Public Mockito Unit test, that verify work create()
     * method in the AuthorServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void create() throws Exception {
        Author author = new Author();
        author.setAuthorName("Stalin");

        authorService.create(author);
        verify(authorDao, times(1)).create(author);
    }

    /**
     * Public Mockito Unit test, that verify work read()
     * method in the AuthorServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void read() throws Exception {
        Author author;
        when(authorDao.read(anyLong())).thenReturn(new Author());

        author = authorService.read(anyLong());
        verify(authorDao, times(1)).read(anyLong());
        assertNotNull(author);

    }

    /**
     * Public Mockito Unit test, that verify work readAll()
     * method in the AuthorServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void readAll() throws Exception {
        List<Author> authorList;
        when(authorDao.readAll()).thenReturn(new ArrayList<Author>());

        authorList = authorService.readAll();
        verify(authorDao, times(1)).readAll();
        assertNotNull(authorList);
    }

    /**
     * Public Mockito Unit test, that verify work update()
     * method in the AuthorServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void update() throws Exception {

        Author author = new Author();
        author.setAuthorName("Stalin");

        authorService.update(author);
        verify(authorDao, times(1)).update(author);
    }

    /**
     * Public Mockito Unit test, that verify work delete()
     * method in the AuthorServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void delete() throws Exception {

        authorService.delete(anyLong());
        verify(authorDao, times(1)).delete(anyLong());
    }

    /**
     * Public Mockito Unit test, that verify work createAuthorForNews()
     * method in the AuthorServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void createAuthorForNews() throws Exception {

        News news = new News();
        news.setShortText("Say no to war!");

        Author author = new Author();
        author.setAuthorName("Stalin");

        authorService.createAuthorForNews(news, author);
        verify(authorDao, times(1)).createAuthorForNews(news, author);
    }
}
