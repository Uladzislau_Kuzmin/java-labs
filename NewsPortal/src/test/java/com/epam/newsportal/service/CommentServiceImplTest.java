package com.epam.newsportal.service;

import com.epam.newsportal.dao.CommentDao;
import com.epam.newsportal.domain.Comment;
import com.epam.newsportal.service.implementation.CommentServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

/**
 * Public class <code>CommentServiceImplTest</code> content
 * list of public Mockito Unit tests, which check
 * right work of Service layer class <code>CommentServiceImpl</code>
 * methods.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.service.CommentService
 * @see com.epam.newsportal.service.implementation.CommentServiceImpl
 * @since 1.0
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration("/spring.config.xml")
public class CommentServiceImplTest {

    /**
     * mock object, which will be inject into CommentServiceImpl class
     */
    @Mock
    private CommentDao commentDao;

    /**
     * service layer object, which we will testing
     */
    @Autowired
    @InjectMocks
    private CommentServiceImpl commentService;

    /**
     * Public Mockito Unit test, that verify work create()
     * method in the CommentServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void create() throws Exception {
        Comment comment = new Comment();
        comment.setCommentText("I am super heavy comment text");

        commentService.create(comment);
        verify(commentDao, times(1)).create(comment);
    }

    /**
     * Public Mockito Unit test, that verify work read()
     * method in the CommentServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void read() throws Exception {
        Comment comment;
        when(commentDao.read(anyLong())).thenReturn(new Comment());

        comment = commentDao.read(anyLong());
        verify(commentDao, times(1)).read(anyLong());
        assertNotNull(comment);

    }

    /**
     * Public Mockito Unit test, that verify work readAll()
     * method in the CommentServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void readAll() throws Exception {
        List<Comment> authorList;
        when(commentDao.readAll()).thenReturn(new ArrayList<Comment>());

        authorList = commentService.readAll();
        verify(commentDao, times(1)).readAll();
        assertNotNull(authorList);
    }

    /**
     * Public Mockito Unit test, that verify work update()
     * method in the CommentServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void update() throws Exception {

        Comment comment = new Comment();
        comment.setCommentText("I am super heavy comment text");

        commentService.update(comment);
        verify(commentDao, times(1)).update(comment);
    }

    /**
     * Public Mockito Unit test, that verify work delete()
     * method in the CommentServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void delete() throws Exception {

        commentService.delete(anyLong());
        verify(commentDao, times(1)).delete(anyLong());
    }
}
