package com.epam.newsportal.service;


import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.domain.Tag;
import com.epam.newsportal.service.implementation.NewsManagerServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Public class <code>NewsManagementServiceTest</code> content
 * list of public Mockito Unit tests, which check right work
 * of Service layer class <code>NewsManagerServiceImpl</code>
 * methods.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.service.NewsManagementService
 * @see com.epam.newsportal.service.implementation.NewsManagerServiceImpl
 * @since 1.0
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration("/spring.config.xml")
public class NewsManagementServiceTest {

    /**
     * authorService object, which will be inject into CommentServiceImpl class
     */
    @Mock
    private AuthorService authorService;

    /**
     * newsService object, which will be inject into CommentServiceImpl class
     */
    @Mock
    private NewsService newsService;

    /**
     * commentService object, which will be inject into CommentServiceImpl class
     */
    @Mock
    private CommentService commentService;

    /**
     * tagService object, which will be inject into CommentServiceImpl class
     */
    @Mock
    private TagService tagService;

    /**
     * service manager object, which we will testing
     */
    @InjectMocks
    @Autowired
    private NewsManagerServiceImpl newsManagerService;

    /**
     * Public Mockito Unit test, that verify work saveNews()
     * method in the NewsManagerServiceImpl class. This method
     * consists of calls few simple service methods.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void saveNews() throws Exception {

        News news = new News();
        news.setShortText("Say no to war!");

        Author author = new Author();
        author.setAuthorName("Stalin");

        List<Tag> tagList = new ArrayList<Tag>();
        for (int i = 0; i < 3; i++) {
            tagList.add(new Tag());
        }

        newsManagerService.saveNews(news, author, tagList);

        verify(newsService, times(1)).create(news);
        verify(authorService, times(1)).create(author);
        for (Tag elem : tagList) {
            verify(tagService, times(tagList.size())).create(elem);
        }

        verify(authorService, times(1)).createAuthorForNews(news, author);
        for (Tag elem : tagList) {
            verify(tagService, times(tagList.size())).createTagForNews(news, elem);
        }
    }
}
