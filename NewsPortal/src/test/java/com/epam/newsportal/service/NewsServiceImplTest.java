package com.epam.newsportal.service;

import com.epam.newsportal.dao.NewsDao;
import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.domain.Tag;
import com.epam.newsportal.service.implementation.NewsServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

/**
 * Public class <code>NewsServiceImplTest</code> content
 * list of public Mockito Unit tests, which check
 * right work of Service layer class <code>NewsManagerServiceImpl</code>
 * methods.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.service.NewsService
 * @see com.epam.newsportal.service.implementation.NewsManagerServiceImpl
 * @since 1.0
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration("/spring.config.xml")
public class NewsServiceImplTest {

    /**
     * mock object, which will be inject into NewsServiceImpl class
     */
    @Mock
    private NewsDao newsDao;

    /**
     * service layer object, which we will testing
     */
    @Autowired
    @InjectMocks
    private NewsServiceImpl newsService;

    /**
     * Public Mockito Unit test, that verify work create()
     * method in the NewsServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void create() throws Exception {
        News news = new News();
        news.setShortText("Say no to war!");

        newsService.create(news);
        verify(newsDao, times(1)).create(news);
    }

    /**
     * Public Mockito Unit test, that verify work read()
     * method in the NewsServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void read() throws Exception {
        News news;
        when(newsDao.read(anyLong())).thenReturn(new News());

        news = newsService.read(anyLong());
        verify(newsDao, times(1)).read(anyLong());
        assertNotNull(news);

    }

    /**
     * Public Mockito Unit test, that verify work readAll()
     * method in the NewsServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void readAll() throws Exception {
        List<News> tagList;
        when(newsDao.readAll()).thenReturn(new ArrayList<News>());

        tagList = newsService.readAll();
        verify(newsDao, times(1)).readAll();
        assertNotNull(tagList);
    }

    /**
     * Public Mockito Unit test, that verify work update()
     * method in the NewsServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void update() throws Exception {

        News news = new News();
        news.setShortText("Say no to war!");

        newsService.update(news);
        verify(newsDao, times(1)).update(news);
    }

    /**
     * Public Mockito Unit test, that verify work delete()
     * method in the NewsServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void delete() throws Exception {

        newsService.delete(anyLong());
        verify(newsDao, times(1)).delete(anyLong());
    }

    /**
     * Public Mockito Unit test, that verify work searchNewsByAuthor()
     * method in the NewsServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void searchNewsByAuthor() throws Exception {
        List<News> newsList = null;

        Author author = new Author();
        author.setAuthorName("Stalin");

        when(newsDao.searchNewsByAuthor(author)).thenReturn(new ArrayList<News>());

        newsList = newsService.searchNewsByAuthor(author);
        verify(newsDao, times(1)).searchNewsByAuthor(author);
        assertNotNull(newsList);
    }

    /**
     * Public Mockito Unit test, that verify work searchNewsByTag()
     * method in the NewsServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void searchNewsByTag() throws Exception {
        List<News> newsList = null;

        Tag tag = new Tag();
        tag.setTagName("I see you!");

        when(newsDao.searchNewsByTag(tag)).thenReturn(new ArrayList<News>());

        newsList = newsService.searchNewsByTag(tag);
        verify(newsDao, times(1)).searchNewsByTag(tag);
        assertNotNull(newsList);
    }

    /**
     * Public Mockito Unit test, that verify work sortedNewsByComments()
     * method in the NewsServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void sortedNewsByComments() throws Exception {
        List<News> newsList = null;

        when(newsDao.sortedNewsByComments()).thenReturn(new ArrayList<News>());

        newsList = newsService.sortedNewsByComments();
        verify(newsDao, times(1)).sortedNewsByComments();
        assertNotNull(newsList);
    }
}
