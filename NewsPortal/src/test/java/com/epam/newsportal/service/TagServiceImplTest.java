package com.epam.newsportal.service;

import com.epam.newsportal.dao.TagDao;
import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.domain.Tag;
import com.epam.newsportal.service.implementation.TagServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Public class <code>TagServiceImplTest</code> content
 * list of public Mockito Unit tests, which check
 * right work of Service layer class <code>TagServiceImpl</code>
 * methods.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.service.TagService
 * @see com.epam.newsportal.service.implementation.TagServiceImpl
 * @since 1.0
 */
@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTest {

    /**
     * mock object, which will be inject into TagServiceImpl class
     */
    @Mock
    private TagDao tagDao;

    /**
     * service layer object, which we will testing
     */
    @Autowired
    @InjectMocks
    private TagServiceImpl tagService;

    /**
     * Public Mockito Unit test, that verify work create()
     * method in the TagServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void create() throws Exception {
        Tag tag = new Tag();
        tag.setTagName("Beautiful life");

        tagService.create(tag);
        verify(tagDao, times(1)).create(tag);
    }

    /**
     * Public Mockito Unit test, that verify work read()
     * method in the TagServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void read() throws Exception {
        Tag tag;
        when(tagDao.read(anyLong())).thenReturn(new Tag());

        tag = tagService.read(anyLong());
        verify(tagDao, times(1)).read(anyLong());
        assertNotNull(tag);

    }

    /**
     * Public Mockito Unit test, that verify work readAll()
     * method in the TagServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void readAll() throws Exception {
        List<Tag> tagList;
        when(tagDao.readAll()).thenReturn(new ArrayList<Tag>());

        tagList = tagService.readAll();
        verify(tagDao, times(1)).readAll();
        assertNotNull(tagList);
    }

    /**
     * Public Mockito Unit test, that verify work update()
     * method in the TagServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void update() throws Exception {

        Tag tag = new Tag();
        tag.setTagName("Beautiful life");

        tagService.update(tag);
        verify(tagDao, times(1)).update(tag);
    }

    /**
     * Public Mockito Unit test, that verify work delete()
     * method in the TagServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void delete() throws Exception {

        tagService.delete(anyLong());
        verify(tagDao, times(1)).delete(anyLong());
    }

    /**
     * Public Mockito Unit test, that verify work searchTagByAuthor()
     * method in the TagServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void searchTagByAuthor() throws Exception {
        List<Tag> tagList = null;

        Author author = new Author();
        author.setAuthorName("Stalin!");

        when(tagDao.searchTagByAuthor(author)).thenReturn(new ArrayList<Tag>());

        tagList = tagService.searchTagByAuthor(author);
        verify(tagDao, times(1)).searchTagByAuthor(author);
        assertNotNull(tagList);
    }

    /**
     * Public Mockito Unit test, that verify work createTagForNews()
     * method in the TagServiceImpl class.
     *
     * @throws Exception any exception that might throw up
     */
    @Test
    public void createTagForNews() throws Exception {
        News news = new News();
        news.setShortText("Say no to war!");

        Tag tag = new Tag();
        tag.setTagName("I see you");

        tagService.createTagForNews(news, tag);
        verify(tagDao, times(1)).createTagForNews(news, tag);
    }

}
