package com.epam.newsportal.dao;

import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.domain.Tag;
import com.epam.newsportal.exception.DaoLayerTechnicalException;

import java.util.List;

/**
 * The second level of the hierarchy Data Access layer
 * interfaces. This interface <code>NewsDao</code>
 * describes the behavior of a particular dao layer
 * which working with instance of <code>News</code>.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.dao.GenericDao
 * @see com.epam.newsportal.domain.News
 * @since 1.0
 */
public interface NewsDao extends GenericDao<News> {

    /**
     * Public abstract method which one defining sort operation of  News table
     * from database by count of comments.
     *
     * @return sorted list of News instance
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     * @see com.epam.newsportal.domain.News
     */
    public List<News> sortedNewsByComments() throws DaoLayerTechnicalException;

    /**
     * Public abstract method which one defining search operation from News table
     * in data base by sent Tag instance.
     *
     * @param tag element of Tag instance to search for news
     * @return list of News which was found by the Tag instance
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     * @see com.epam.newsportal.domain.Tag
     */
    public List<News> searchNewsByTag(Tag tag) throws DaoLayerTechnicalException;

    /**
     * Public abstract method which one defining search operation from News table
     * in data base by sent Author instance.
     *
     * @param author element of Author instance to search for News list
     * @return list of News which was find by the Author instance
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     * @see com.epam.newsportal.domain.Author
     */
    public List<News> searchNewsByAuthor(Author author) throws DaoLayerTechnicalException;
}

