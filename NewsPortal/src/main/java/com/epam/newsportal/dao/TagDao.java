package com.epam.newsportal.dao;

import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.domain.Tag;
import com.epam.newsportal.exception.DaoLayerTechnicalException;

import java.util.List;

/**
 * The second level of the hierarchy Data Access layer
 * interfaces. This interface <code>TagDao</code>
 * describes the behavior of a particular dao layer
 * which working with instance of <code>Tag</code>.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.dao.GenericDao
 * @see com.epam.newsportal.domain.Tag
 * @since 1.0
 */
public interface TagDao extends GenericDao<Tag> {

    /**
     * Public abstract method which one defining search operation from Tag table
     * in data base by sent Author instance.
     *
     * @param author element of Author instance to search for Tag list
     * @return list of Tag which was find by the Author instance
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     * @see com.epam.newsportal.domain.Tag
     */
    public abstract List<Tag> searchTagByAuthor(Author author) throws DaoLayerTechnicalException;

    /**
     * Public abstract method which one defining operation
     * binding the News instance with the Tag instance
     * in the table NewsTag in data base.
     *
     * @param news News instance for binding with Tag instance
     * @param tag  Tag instance for binding with News instance
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     * @see com.epam.newsportal.domain.Tag
     * @see com.epam.newsportal.domain.News
     */
    public abstract void createTagForNews(News news, Tag tag) throws DaoLayerTechnicalException;
}
