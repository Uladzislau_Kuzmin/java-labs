package com.epam.newsportal.dao.implementation;

import com.epam.newsportal.dao.AuthorDao;
import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.exception.DaoLayerTechnicalException;
import com.epam.newsportal.managers.LoggerMessageManager;
import com.epam.newsportal.managers.TransactionManager;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Public class <code>AuthorDaoImpl</code> is an element
 * of Data Access layer and working with Author data base
 * instance. This is a  single ton realization of
 * <code>AuthorDao</code> interface and it performs all
 * operations described in this interface.
 * Realized pattern Singleton.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.dao.CommentDao
 * @see com.epam.newsportal.domain.Author
 * @since 1.0
 */
@Repository
public class AuthorDaoImpl implements AuthorDao {

    /**
     * Constant that content AUTHOR_ID title from table AUTHOR
     */
    private static final String COLUMN_AUTHOR_ID = "AUTHOR_ID";

    /**
     * object which used for connecting to the database
     */
    @Autowired
    private BasicDataSource basicDataSource;

    /**
     * object in which we saved received data
     */
    @Autowired
    private Author author;

    /**
     * Override public method used to add record to the table Author.
     *
     * @param author instance which will be add to the data base
     * @return unique identifier inserted instance
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public long create(Author author) throws DaoLayerTechnicalException {
        long insertedAuthorId = 0;

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String generatedColumns[] = {COLUMN_AUTHOR_ID};
        try {
            connection = basicDataSource.getConnection();
            preparedStatement = connection.prepareStatement(
                    TransactionManager.getInstance().getProperty(TransactionManager.INSERT_AUTHOR_RECORD), generatedColumns);

            preparedStatement.setString(1, author.getAuthorName());
            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                insertedAuthorId = resultSet.getLong(1);
            }

        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        } finally {
            closeDataBaseResources(resultSet, preparedStatement, connection);
        }

        return insertedAuthorId;
    }

    /**
     * Override public method used to read record from the table Author.
     *
     * @param authorId unique identifier of instance which will be found
     * @return find Author instance
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public Author read(long authorId) throws DaoLayerTechnicalException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = basicDataSource.getConnection();
            preparedStatement = connection.prepareStatement(
                    TransactionManager.getInstance().getProperty(TransactionManager.READ_AUTHOR_RECORD_BY_ID));

            preparedStatement.setLong(1, authorId);

            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                author.setAuthorId(resultSet.getLong(1));
                author.setAuthorName(resultSet.getString(2));
            }

            return author;

        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        } finally {
            closeDataBaseResources(resultSet, preparedStatement, connection);
        }
    }

    /**
     * Override public method used to read all records from the table Author.
     *
     * @return list of find Author instances
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public List<Author> readAll() throws DaoLayerTechnicalException {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        List<Author> authors = null;

        try {
            connection = basicDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery(TransactionManager.getInstance().
                    getProperty(TransactionManager.SELECT_ALL_AUTHORS));

            authors = new ArrayList<Author>();
            while (resultSet.next()) {
                author = new Author();

                author.setAuthorId(resultSet.getLong(1));
                author.setAuthorName(resultSet.getString(2));

                authors.add(author);
            }
            return authors;

        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        } finally {
            closeDataBaseResources(resultSet, statement, connection);
        }
    }

    /**
     * Override public method used to update record in the table Author.
     *
     * @param author instance which will be update in the data base
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public void update(Author author) throws DaoLayerTechnicalException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = basicDataSource.getConnection();
            preparedStatement = connection.prepareStatement(
                    TransactionManager.getInstance().getProperty(TransactionManager.UPDATE_AUTHOR_RECORD));

            preparedStatement.setString(1, author.getAuthorName());
            preparedStatement.setLong(2, author.getAuthorId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        } finally {
            closeDataBaseResources(resultSet, preparedStatement, connection);
        }
    }

    /**
     * Override public method used to delete record from the table Author.
     *
     * @param authorId unique identifier of instance which will be delete
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public void delete(long authorId) throws DaoLayerTechnicalException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = basicDataSource.getConnection();
            preparedStatement = connection.prepareStatement(
                    TransactionManager.getInstance().getProperty(TransactionManager.DELETE_AUTHOR_RECORD));

            preparedStatement.setLong(1, authorId);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        } finally {
            closeDataBaseResources(resultSet, preparedStatement, connection);
        }
    }

    /**
     * Override public method used to bind Author instance with instance of News, and add
     * same record to the NewsAuthor table in data base.
     *
     * @param news   News instance for binding with Author instance
     * @param author Author instance for binding with News instance
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public void createAuthorForNews(News news, Author author) throws DaoLayerTechnicalException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = basicDataSource.getConnection();
            preparedStatement = connection.prepareStatement(
                    TransactionManager.getInstance().getProperty(TransactionManager.INSERT_NEWS_AUTHOR_RECORD));

            preparedStatement.setLong(1, news.getNewsId());
            preparedStatement.setLong(2, author.getAuthorId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        } finally {
            closeDataBaseResources(resultSet, preparedStatement, connection);
        }
    }

    /**
     * Method that execute operation of closing all resources, after use
     *
     * @param resultSet  ResultSet object to close after using
     * @param statement  Statement object to close after using
     * @param connection Connection object to close after using
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    private void closeDataBaseResources(ResultSet resultSet, Statement statement, Connection connection) throws DaoLayerTechnicalException {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        }
    }
}
