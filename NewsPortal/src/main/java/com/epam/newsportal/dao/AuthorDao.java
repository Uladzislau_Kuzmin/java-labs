package com.epam.newsportal.dao;

import com.epam.newsportal.dao.GenericDao;
import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.exception.DaoLayerTechnicalException;


/**
 * The second level of the hierarchy Data Access layer
 * interfaces. This interface <code>AuthorDao</code>
 * describes the behavior of a particular dao layer
 * which working with instance of <code>Author</code>.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.dao.GenericDao
 * @see com.epam.newsportal.domain.Author
 * @since 1.0
 */
public interface AuthorDao extends GenericDao<com.epam.newsportal.domain.Author> {
    /**
     * Public abstract method which one defining operation
     * binding the News instance with the Author instance
     * in the table NewsAuthor in data base.
     *
     * @param news   News instance for binding with Author instance
     * @param author Author instance for binding with News instance
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     * @see com.epam.newsportal.domain.News
     * @see com.epam.newsportal.domain.Author
     */
    public abstract void createAuthorForNews(News news, Author author) throws DaoLayerTechnicalException;
}
