package com.epam.newsportal.dao;

import com.epam.newsportal.domain.Comment;

/**
 * The second level of the hierarchy Data Access layer
 * interfaces. This interface <code>CommentDao</code>
 * describes the behavior of a particular dao layer
 * which working with instance of <code>Comment</code>.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.dao.GenericDao
 * @see com.epam.newsportal.domain.Comment
 * @since 1.0
 */
public interface CommentDao extends GenericDao<Comment> {

    //place for new methods

}
