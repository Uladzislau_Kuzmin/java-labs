package com.epam.newsportal.dao.implementation;

import com.epam.newsportal.dao.TagDao;
import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.domain.Tag;
import com.epam.newsportal.exception.DaoLayerTechnicalException;
import com.epam.newsportal.managers.LoggerMessageManager;
import com.epam.newsportal.managers.TransactionManager;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Public class <code>TagDaoImpl</code> is an element
 * of Data Access layer and working with Tag data base
 * instance. This is a  single ton realization of
 * <code>TagDao</code> interface and it performs all
 * operations described in this interface.
 * Realized pattern Singleton.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.dao.TagDao
 * @see com.epam.newsportal.domain.Tag
 * @since 1.0
 */
@Repository
public class TagDaoImpl implements TagDao {

    /**
     * Constant that content TAG_ID title from table TAG
     */
    private static final String COLUMN_TAG_ID = "TAG_ID";

    /**
     * object which used for connecting to the database
     */
    @Autowired
    private BasicDataSource basicDataSource;

    /**
     * object in which we saved received data
     */
    @Autowired
    private Tag tag;

    /**
     * Override public method used to add record to the table Tag.
     *
     * @param tag instance which will be add to the data base
     * @return unique identifier inserted instance
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public long create(Tag tag) throws DaoLayerTechnicalException {
        long insertedTagId = 0;

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String generatedColumns[] = {COLUMN_TAG_ID};
        try {
            connection = basicDataSource.getConnection();
            preparedStatement = connection.prepareStatement(
                    TransactionManager.getInstance().getProperty(TransactionManager.INSERT_TAG_RECORD), generatedColumns);

            preparedStatement.setString(1, tag.getTagName());

            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                insertedTagId = resultSet.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        } finally {
            closeDataBaseResources(resultSet, preparedStatement, connection);
        }
        return insertedTagId;
    }

    /**
     * Override public method used to read record from the table Tag.
     *
     * @param tagId unique identifier of instance which will be found
     * @return find Tag instance
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public Tag read(long tagId) throws DaoLayerTechnicalException {

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = basicDataSource.getConnection();
            preparedStatement = connection.prepareStatement(
                    TransactionManager.getInstance().getProperty(TransactionManager.READ_TAG_RECORD_BY_ID));

            preparedStatement.setLong(1, tagId);

            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                tag.setTagId(resultSet.getLong(1));
                tag.setTagName(resultSet.getString(2));
            }

            return tag;

        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION));
        } finally {
            closeDataBaseResources(resultSet, preparedStatement, connection);
        }
    }

    /**
     * Override public method used to read all records from the table Tag.
     *
     * @return list of find Tags instances
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public List<Tag> readAll() throws DaoLayerTechnicalException {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        List<Tag> tagList = null;

        try {
            connection = basicDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery(TransactionManager.getInstance().
                    getProperty(TransactionManager.SELECT_ALL_TAGS));

            tagList = new ArrayList<Tag>();
            while (resultSet.next()) {
                tag = new Tag();

                tag.setTagId(resultSet.getLong(1));
                tag.setTagName(resultSet.getString(2));

                tagList.add(tag);
            }

            return tagList;

        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        } finally {
            closeDataBaseResources(resultSet, statement, connection);
        }
    }

    /**
     * Override public method used to update record in the table Tag.
     *
     * @param tag instance which will be update in the data base
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public void update(Tag tag) throws DaoLayerTechnicalException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = basicDataSource.getConnection();
            preparedStatement = connection.prepareStatement(
                    TransactionManager.getInstance().getProperty(TransactionManager.UPDATE_TAG_RECORD));

            preparedStatement.setString(1, tag.getTagName());
            preparedStatement.setLong(2, tag.getTagId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        } finally {
            closeDataBaseResources(resultSet, preparedStatement, connection);
        }
    }

    /**
     * Override public method used to delete record from the table Tag.
     *
     * @param tagId unique identifier of instance which will be delete
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public void delete(long tagId) throws DaoLayerTechnicalException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = basicDataSource.getConnection();
            preparedStatement = connection.prepareStatement(
                    TransactionManager.getInstance().getProperty(TransactionManager.DELETE_TAG_RECORD));

            preparedStatement.setLong(1, tagId);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        } finally {
            closeDataBaseResources(resultSet, preparedStatement, connection);
        }
    }

    /**
     * Override public method used to search list of Tags record
     * from the table Tag by Author instance.
     *
     * @param author element of Author instance to search for Tag list
     * @return list of Tag which was find by the Author instance
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public List<Tag> searchTagByAuthor(Author author) throws DaoLayerTechnicalException {

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        List<Tag> tagList = null;

        try {
            connection = basicDataSource.getConnection();
            preparedStatement = connection.prepareStatement(
                    TransactionManager.getInstance().getProperty(TransactionManager.SELECT_TAG_RECORD_BY_AUTHOR_ID));

            preparedStatement.setLong(1, author.getAuthorId());

            resultSet = preparedStatement.executeQuery();

            tagList = new ArrayList<Tag>();
            while (resultSet.next()) {
                tag = new Tag();

                tag.setTagId(resultSet.getLong(1));
                tag.setTagName(resultSet.getString(2));

                tagList.add(tag);
            }

            return tagList;

        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        } finally {
            closeDataBaseResources(resultSet, preparedStatement, connection);
        }
    }

    /**
     * Override public method used to bind Tag instance with instance of News, and add
     * same record to the NewsTag table in data base.
     *
     * @param news News instance for binding with Tag instance
     * @param tag  Tag instance for binding with News instance
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public void createTagForNews(News news, Tag tag) throws DaoLayerTechnicalException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = basicDataSource.getConnection();
            preparedStatement = connection.prepareStatement(
                    TransactionManager.getInstance().getProperty(TransactionManager.INSERT_NEWS_TAG_RECORD));

            preparedStatement.setLong(1, news.getNewsId());
            preparedStatement.setLong(2, tag.getTagId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        } finally {
            closeDataBaseResources(resultSet, preparedStatement, connection);
        }
    }

    /**
     * Method that execute operation of closing all resources, after use
     *
     * @param resultSet  ResultSet object to close after using
     * @param statement  Statement object to close after using
     * @param connection Connection object to close after using
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    private void closeDataBaseResources(ResultSet resultSet, Statement statement, Connection connection) throws DaoLayerTechnicalException {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        }
    }

}
