package com.epam.newsportal.dao.implementation;

import com.epam.newsportal.dao.NewsDao;
import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.domain.Tag;
import com.epam.newsportal.exception.DaoLayerTechnicalException;
import com.epam.newsportal.managers.LoggerMessageManager;
import com.epam.newsportal.managers.TransactionManager;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Public class <code>NewsDaoImpl</code> is an element
 * of Data Access layer and working with News data base
 * instance. This is a  single ton realization of
 * <code>NewsDao</code> interface and it performs all
 * operations described in this interface.
 * Realized pattern Singleton.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.dao.NewsDao
 * @see com.epam.newsportal.domain.News
 * @since 1.0
 */
@Repository
public class NewsDaoImpl implements NewsDao {

    /**
     * Constant that content NEWS_ID title from table NEWS
     */
    private static final String COLUMN_NEWS_ID = "NEWS_ID";

    /**
     * object which used for connecting to the database
     */
    @Autowired
    private BasicDataSource basicDataSource;

    /**
     * object in which we saved received data
     */
    @Autowired
    private News news;

    /**
     * Override public method used to add record to the table News.
     *
     * @param news instance which will be add to the data base
     * @return unique identifier inserted instance
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public long create(News news) throws DaoLayerTechnicalException {
        long insertedNewsId = 0;

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String generatedColumns[] = {COLUMN_NEWS_ID};
        try {
            connection = basicDataSource.getConnection();
            preparedStatement = connection.prepareStatement(
                    TransactionManager.getInstance().getProperty(TransactionManager.INSERT_NEWS_RECORD), generatedColumns);

            preparedStatement.setString(1, news.getShortText());
            preparedStatement.setString(2, news.getFullText());
            preparedStatement.setString(3, news.getTitle());
            preparedStatement.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
            preparedStatement.setDate(5, new Date(news.getModificationDate().getTime()));

            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                insertedNewsId = resultSet.getLong(1);
            }

        } catch (SQLException e) {

            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        } finally {
            closeDataBaseResources(resultSet, preparedStatement, connection);
        }
        return insertedNewsId;

    }

    /**
     * Override public method used to read record from the table News.
     *
     * @param newsId unique identifier of instance which will be found
     * @return find News instance
     * @throws DaoLayerTechnicalException user-defined exception occurs when any sql error throw up
     * @see java.sql.SQLException
     */
    @Override
    public News read(long newsId) throws DaoLayerTechnicalException {

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = basicDataSource.getConnection();
            preparedStatement = connection.prepareStatement(
                    TransactionManager.getInstance().getProperty(TransactionManager.READ_NEWS_BY_ID));

            preparedStatement.setLong(1, newsId);

            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                news.setNewsId(resultSet.getLong(1));
                news.setShortText(resultSet.getString(2));
                news.setFullText(resultSet.getString(3));
                news.setTitle(resultSet.getString(4));
                news.setCreationDate(resultSet.getTimestamp(5));
                news.setModificationDate(resultSet.getDate(6));
            }

            return news;

        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        } finally {
            closeDataBaseResources(resultSet, preparedStatement, connection);
        }
    }

    /**
     * Override public method used to read all records from the table News.
     *
     * @return list of find News instances
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public List<News> readAll() throws DaoLayerTechnicalException {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        List<News> newsList = null;

        try {
            connection = basicDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery(TransactionManager.getInstance().
                    getProperty(TransactionManager.SELECT_ALL_NEWS));

            newsList = new ArrayList<News>();
            while (resultSet.next()) {
                news = new News();

                news.setNewsId(resultSet.getLong(1));
                news.setShortText(resultSet.getString(2));
                news.setFullText(resultSet.getString(3));
                news.setTitle(resultSet.getString(4));
                news.setCreationDate(resultSet.getTimestamp(5));
                news.setModificationDate(resultSet.getDate(6));

                newsList.add(news);
            }

            return newsList;

        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        } finally {
            closeDataBaseResources(resultSet, statement, connection);
        }
    }

    /**
     * Override public method used to update record in the table News.
     *
     * @param news instance which will be update in the data base
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public void update(News news) throws DaoLayerTechnicalException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = basicDataSource.getConnection();
            preparedStatement = connection.prepareStatement(
                    TransactionManager.getInstance().getProperty(TransactionManager.UPDATE_NEWS_RECORD));

            preparedStatement.setString(1, news.getShortText());
            preparedStatement.setString(2, news.getFullText());
            preparedStatement.setString(3, news.getTitle());
            preparedStatement.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
            preparedStatement.setDate(5, new Date(news.getModificationDate().getTime()));
            preparedStatement.setLong(6, news.getNewsId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);

        } finally {
            closeDataBaseResources(resultSet, preparedStatement, connection);
        }
    }

    /**
     * Override public method used to delete record from the table News.
     *
     * @param newsId unique identifier of instance which will be delete
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public void delete(long newsId) throws DaoLayerTechnicalException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = basicDataSource.getConnection();
            preparedStatement = connection.prepareStatement(
                    TransactionManager.getInstance().getProperty(TransactionManager.DELETE_NEWS_RECORD));

            preparedStatement.setLong(1, newsId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        } finally {
            closeDataBaseResources(resultSet, preparedStatement, connection);
        }
    }

    /**
     * Override public method used to get sorted, by comments count,
     * list of records from the table News.
     *
     * @return sorted list of News instance
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public List<News> sortedNewsByComments() throws DaoLayerTechnicalException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        List<News> newsList = null;

        try {
            connection = basicDataSource.getConnection();
            preparedStatement = connection.prepareStatement(TransactionManager.getInstance().
                    getProperty(TransactionManager.SELECT_MOST_COMMENTED_NEWS));

            resultSet = preparedStatement.executeQuery();

            newsList = new ArrayList<News>();
            while (resultSet.next()) {
                news = new News();

                news.setNewsId(resultSet.getLong(1));
                news.setShortText(resultSet.getString(2));
                news.setFullText(resultSet.getString(3));
                news.setTitle(resultSet.getString(4));
                news.setCreationDate(resultSet.getTimestamp(5));
                news.setModificationDate(resultSet.getDate(6));

                newsList.add(news);
            }

            return newsList;

        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        } finally {
            closeDataBaseResources(resultSet, preparedStatement, connection);
        }
    }

    /**
     * Override public method used to search list of News records
     * from the table News by Tag instance.
     *
     * @param tag element of Tag instance to search for news
     * @return list of News which was found by the Tag instance
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public List<News> searchNewsByTag(Tag tag) throws DaoLayerTechnicalException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        List<News> newsList = null;

        try {
            connection = basicDataSource.getConnection();
            preparedStatement = connection.prepareStatement(
                    TransactionManager.getInstance().getProperty(TransactionManager.SELECT_NEWS_BY_TAG_ID));

            preparedStatement.setLong(1, tag.getTagId());

            resultSet = preparedStatement.executeQuery();

            newsList = new ArrayList<News>();
            while (resultSet.next()) {
                news = new News();

                news.setNewsId(resultSet.getLong(1));
                news.setShortText(resultSet.getString(2));
                news.setFullText(resultSet.getString(3));
                news.setTitle(resultSet.getString(4));
                news.setCreationDate(resultSet.getTimestamp(5));
                news.setModificationDate(resultSet.getDate(6));

                newsList.add(news);
            }

            return newsList;

        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        } finally {
            closeDataBaseResources(resultSet, preparedStatement, connection);
        }
    }

    /**
     * Override public method used to search list of News records
     * from the table News by Author instance.
     *
     * @param author element of Author instance to search for News list
     * @return list of News which was find by the Author instance
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public List<News> searchNewsByAuthor(Author author) throws DaoLayerTechnicalException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        List<News> newsList = null;

        try {
            connection = basicDataSource.getConnection();
            preparedStatement = connection.prepareStatement(
                    TransactionManager.getInstance().getProperty(TransactionManager.SELECT_NEWS_RECORD_BY_AUTHOR_ID));

            preparedStatement.setLong(1, author.getAuthorId());

            resultSet = preparedStatement.executeQuery();

            newsList = new ArrayList<News>();
            while (resultSet.next()) {
                news = new News();

                news.setNewsId(resultSet.getLong(1));
                news.setShortText(resultSet.getString(2));
                news.setFullText(resultSet.getString(3));
                news.setTitle(resultSet.getString(4));
                news.setCreationDate(resultSet.getTimestamp(5));
                news.setModificationDate(resultSet.getDate(6));

                newsList.add(news);
            }

            return newsList;

        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        } finally {
            closeDataBaseResources(resultSet, preparedStatement, connection);
        }
    }

    /**
     * Method that execute operation of closing all resources, after use
     *
     * @param resultSet  ResultSet object to close after using
     * @param statement  Statement object to close after using
     * @param connection Connection object to close after using
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    private void closeDataBaseResources(ResultSet resultSet, Statement statement, Connection connection) throws DaoLayerTechnicalException {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        }
    }
}
