package com.epam.newsportal.dao.implementation;

import com.epam.newsportal.dao.CommentDao;
import com.epam.newsportal.domain.Comment;
import com.epam.newsportal.exception.DaoLayerTechnicalException;
import com.epam.newsportal.managers.LoggerMessageManager;
import com.epam.newsportal.managers.TransactionManager;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Public class <code>CommentDaoImpl</code> is an element
 * of Data Access layer and working with Comment data base
 * instance. This is a  single ton realization of
 * <code>CommentDao</code> interface and it performs all
 * operations described in this interface.
 * Realized pattern Singleton.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.dao.CommentDao
 * @see com.epam.newsportal.domain.Comment
 * @since 1.0
 */
@Repository
public class CommentDaoImpl implements CommentDao {

    /**
     * Constant that content COMMENT_ID title from table COMMENT
     */
    private static final String COLUMN_COMMENT_ID = "COMMENT_ID";

    /**
     * object which used for connecting to the database
     */
    @Autowired
    private BasicDataSource basicDataSource;

    /**
     * object in which we saved received data
     */
    @Autowired
    private Comment comment;

    /**
     * Override public method used to add record to the table Comment.
     *
     * @param comment instance which will be add to the data base
     * @return unique identifier inserted instance
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public long create(Comment comment) throws DaoLayerTechnicalException {
        long insertedCommentId = 0;

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String generatedColumns[] = {COLUMN_COMMENT_ID};
        try {
            connection = basicDataSource.getConnection();
            preparedStatement = connection.prepareStatement(
                    TransactionManager.getInstance().getProperty(TransactionManager.INSERT_COMMENTS_RECORD), generatedColumns);

            preparedStatement.setString(1, comment.getCommentText());
            preparedStatement.setTimestamp(2, new Timestamp(comment.getCreationDate().getTime()));
            preparedStatement.setLong(3, comment.getNewsId());

            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                insertedCommentId = resultSet.getLong(1);
            }

        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        } finally {
            closeDataBaseResources(resultSet, preparedStatement, connection);
        }

        return insertedCommentId;
    }

    /**
     * Override public method used to read record from the table Comment.
     *
     * @param commentId unique identifier of instance which will be found
     * @return find Comment instance
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public Comment read(long commentId) throws DaoLayerTechnicalException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = basicDataSource.getConnection();
            preparedStatement = connection.prepareStatement(
                    TransactionManager.getInstance().getProperty(TransactionManager.READ_COMMENT_RECORD_BY_ID));

            preparedStatement.setLong(1, commentId);

            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                comment.setCommentId(resultSet.getLong(1));
                comment.setCommentText(resultSet.getString(2));
                comment.setCreationDate(resultSet.getTimestamp(3));
                comment.setNewsId(resultSet.getLong(4));
            }

            return comment;

        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        } finally {
            closeDataBaseResources(resultSet, preparedStatement, connection);
        }
    }

    /**
     * Override public method used to read all records from the table Comment.
     *
     * @return list of find Comment instances
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public List<Comment> readAll() throws DaoLayerTechnicalException {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        List<Comment> commentList = null;

        try {
            connection = basicDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery(TransactionManager.getInstance().
                    getProperty(TransactionManager.SELECT_ALL_COMMENTS));

            commentList = new ArrayList<Comment>();
            while (resultSet.next()) {
                comment = new Comment();

                comment.setCommentId(resultSet.getLong(1));
                comment.setCommentText(resultSet.getString(2));
                comment.setCreationDate(resultSet.getTimestamp(3));
                comment.setNewsId(resultSet.getLong(4));

                commentList.add(comment);
            }

            return commentList;

        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        } finally {
            closeDataBaseResources(resultSet, statement, connection);
        }
    }

    /**
     * Override public method used to update record in the table Comment.
     *
     * @param comment instance which will be update in the data base
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public void update(Comment comment) throws DaoLayerTechnicalException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = basicDataSource.getConnection();
            preparedStatement = connection.prepareStatement(
                    TransactionManager.getInstance().getProperty(TransactionManager.UPDATE_COMMENT_RECORD));

            preparedStatement.setString(1, comment.getCommentText());
            preparedStatement.setTimestamp(2, new Timestamp(comment.getCreationDate().getTime()));
            preparedStatement.setLong(3, comment.getNewsId());
            preparedStatement.setLong(4, comment.getCommentId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        } finally {
            closeDataBaseResources(resultSet, preparedStatement, connection);
        }
    }

    /**
     * Override public method used to delete record from the table Comment.
     *
     * @param commentId unique identifier of instance which will be delete
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    @Override
    public void delete(long commentId) throws DaoLayerTechnicalException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = basicDataSource.getConnection();
            preparedStatement = connection.prepareStatement(
                    TransactionManager.getInstance().getProperty(TransactionManager.DELETE_COMMENT_RECORD));

            preparedStatement.setLong(1, commentId);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        } finally {
            closeDataBaseResources(resultSet, preparedStatement, connection);
        }
    }

    /**
     * Method that execute operation of closing all resources, after use
     *
     * @param resultSet  ResultSet object to close after using
     * @param statement  Statement object to close after using
     * @param connection Connection object to close after using
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    private void closeDataBaseResources(ResultSet resultSet, Statement statement, Connection connection) throws DaoLayerTechnicalException {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            throw new DaoLayerTechnicalException(LoggerMessageManager.getInstance()
                    .getProperty(LoggerMessageManager.DAO_TECHNICAL_SQL_EXCEPTION), e);
        }
    }
}