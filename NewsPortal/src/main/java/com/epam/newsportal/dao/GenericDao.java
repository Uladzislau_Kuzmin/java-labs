package com.epam.newsportal.dao;

import com.epam.newsportal.exception.DaoLayerTechnicalException;

import java.io.Serializable;
import java.util.List;

/**
 * Generic interface <code>GenericDao</code>
 * <br>
 * The top of the inheritance hierarchy of
 * interfaces Data Access layer contains a set
 * of four standard operations for working with
 * data base common to all DAO implementations.
 *
 * @param <Entity> the type of elements in this interface
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @since 1.0
 */
public interface GenericDao<Entity extends Serializable> {

    /**
     * Public abstract method which defining the operation
     * of adding record in a database.
     *
     * @param entity record to be added
     * @return inserted entityID
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    public abstract long create(Entity entity) throws DaoLayerTechnicalException;


    /**
     * Public abstract method which one defining the operation
     * of reading record in a database.
     *
     * @param entityID unique identifier of a record to read
     * @return entity record to be read
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    public abstract Entity read(long entityID) throws DaoLayerTechnicalException;

    /**
     * Public abstract method which one defining the operation
     * of reading all records in a database.
     *
     * @return list of entity records to read
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    public abstract List<Entity> readAll() throws DaoLayerTechnicalException;

    /**
     * Public abstract method which one defining the operation
     * of updating record in a database.
     *
     * @param entity record to updated
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    public abstract void update(Entity entity) throws DaoLayerTechnicalException;

    /**
     * Public abstract method which one defining the operation
     * of deleting record from a database.
     *
     * @param entityID unique identifier of a record to deleted
     * @throws DaoLayerTechnicalException user-defined exception occurs when
     *                                    when any <code>SQLException</code> throw up
     * @see java.sql.SQLException
     */
    public abstract void delete(long entityID) throws DaoLayerTechnicalException;

}
