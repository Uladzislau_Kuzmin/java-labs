package com.epam.newsportal.domain;

import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * Public class <code>Author</code> is one of Entities
 * classes. Its content is fully consistent with Table Author
 * in data base, which we use for. The main role is to store
 * associated with the table information(data).
 * Can be an element <code>HashTable</code>
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @since 1.0
 */
@Component
public class Author implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * unique identifier of instance
     */
    private Long authorId;

    /**
     * parameter describe author last Name
     */
    private String authorName;

    /**
     * Constructor without parameters for creating an object.
     */
    public Author() {
    }

    /**
     * Constructor with parameters for creating an object.
     *
     * @param authorID   author unique identifier to creating
     * @param authorName author Last Name to creating
     */
    public Author(Long authorID, String authorName) {
        this.authorName = authorName;
        this.authorId = authorID;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorID) {
        this.authorId = authorID;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (!(getClass() == obj.getClass())) return false;

        Author author = (Author) obj;
        if (authorId != null ? !authorId.equals(author.authorId) : author.authorId != null) return false;
        if (authorName != null ? !authorName.equals(author.authorName) : author.authorName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = authorId.hashCode();
        result = 31 * result + (authorName != null ? authorName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Author{" +
                "authorID=" + authorId +
                ", authorName='" + authorName + '\'' +
                '}';
    }
}
