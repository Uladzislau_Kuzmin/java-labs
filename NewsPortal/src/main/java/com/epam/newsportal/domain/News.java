package com.epam.newsportal.domain;

import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;

/**
 * Public class <code>News</code> is one of Entities
 * classes. Its content is fully consistent with Table Author
 * in data base, which we use for. The main role is to store
 * associated with the table information(data).
 * Can be an element <code>HashTable</code>
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @since 1.0
 */
@Component
public class News implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * unique identifier of instance
     */
    private Long newsId;

    /**
     * short news text, that describe contents
     */
    private String shortText;

    /**
     * full news text
     */
    private String fullText;

    /**
     * news title for reading
     */
    private String title;

    /**
     * date of news creating
     */
    private Date creationDate;

    /**
     * date of news modification
     */
    private Date modificationDate;

    /**
     * Constructor without parameters for creating an object.
     */
    public News() {
    }

    /**
     * @param newsId           unique identifier for creating
     * @param shortText        simple news text for creating
     * @param fullText         full news text for creating
     * @param title            news title for creating
     * @param creationDate     date for news creating
     * @param modificationDate date of news modification for creating
     */
    public News(Long newsId, String shortText, String fullText, String title,
                Date creationDate, Date modificationDate) {

        this.newsId = newsId;
        this.shortText = shortText;
        this.fullText = fullText;
        this.title = title;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (!(getClass() == obj.getClass())) return false;

        News news = (News) obj;
        if (newsId != null ? !newsId.equals(news.newsId) : news.newsId != null) return false;
        if (shortText != null ? !shortText.equals(news.shortText) : news.shortText != null) return false;
        if (fullText != null ? !fullText.equals(news.fullText) : news.fullText != null) return false;
        if (title != null ? !title.equals(news.title) : news.title != null) return false;
        if (creationDate != null ? !creationDate.equals(news.creationDate) : news.creationDate != null) return false;
        if (modificationDate != null ? !modificationDate.equals(news.modificationDate)
                : news.modificationDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = newsId.hashCode();
        result = 31 * result + (shortText != null ? shortText.hashCode() : 0);
        result = 31 * result + (fullText != null ? fullText.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (modificationDate != null ? modificationDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "News{" +
                "newsId=" + newsId +
                ", shortText='" + shortText + '\'' +
                ", fullText='" + fullText + '\'' +
                ", title='" + title + '\'' +
                ", creationDate=" + creationDate +
                ", modificationDate=" + modificationDate +
                '}';
    }
}
