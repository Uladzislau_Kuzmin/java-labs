package com.epam.newsportal.managers;

import java.util.ResourceBundle;

/**
 * <code>LoggerMessageManager</code>
 * <br>
 * Class LoggerMessageManager is one of several file managers
 * and its function is as follows associates information from
 * <file>logger_messages.properties</file> with constants which
 * describes in it.
 * <br>
 * Realized pattern Singleton.
 *
 * @see java.util.ResourceBundle
 */
public class LoggerMessageManager {

    /**
     * single instance of LoggerMessageManager
     */
    private static LoggerMessageManager instance;

    /**
     * ResourceBundle instance to bind with file, which content logger messages strings
     */
    private ResourceBundle resourceBundle;

    /**
     * main constants whose names coincide with the names of entries
     * that they can be called from logger_messages.properties file
     */
    private static final String BUNDLE_NAME = "logger_messages";

    public static final String NEWS_SUCCESSFULLY_ADDED = "NEWS_SUCCESSFULLY_ADDED";
    public static final String NEWS_SUCCESSFULLY_READ = "NEWS_SUCCESSFULLY_READ";
    public static final String NEWS_SUCCESSFULLY_UPDATE = "NEWS_SUCCESSFULLY_UPDATE";
    public static final String NEWS_SUCCESSFULLY_DELETE = "NEWS_SUCCESSFULLY_DELETE";
    public static final String NEWS_LIST_SUCCESSFULLY_READ = "NEWS_LIST_SUCCESSFULLY_READ";
    public static final String NEWS_LIST_BY_TAG_ID_SUCCESSFULLY_READ = "NEWS_LIST_BY_TAG_ID_SUCCESSFULLY_READ";
    public static final String MOST_COMMENTED_NEWS_SUCCESSFULLY_READ = "MOST_COMMENTED_NEWS_SUCCESSFULLY_READ";
    public static final String NEWS_SUCCESSFULLY_FOUND = "NEWS_SUCCESSFULLY_FOUND";

    public static final String COMMENT_SUCCESSFULLY_ADDED = "COMMENT_SUCCESSFULLY_ADDED";
    public static final String COMMENT_SUCCESSFULLY_READ = "COMMENT_SUCCESSFULLY_READ";
    public static final String COMMENT_LIST_SUCCESSFULLY_READ = "COMMENT_LIST_SUCCESSFULLY_READ";
    public static final String COMMENT_SUCCESSFULLY_UPDATE = "COMMENT_SUCCESSFULLY_UPDATE";
    public static final String COMMENT_SUCCESSFULLY_DELETE = "COMMENT_SUCCESSFULLY_DELETE";

    public static final String AUTHOR_SUCCESSFULLY_ADDED = "AUTHOR_SUCCESSFULLY_ADDED";
    public static final String AUTHOR_SUCCESSFULLY_READ = "AUTHOR_SUCCESSFULLY_READ";
    public static final String AUTHOR_LIST_SUCCESSFULLY_READ = "AUTHOR_LIST_SUCCESSFULLY_READ";
    public static final String AUTHOR_SUCCESSFULLY_UPDATE = "AUTHOR_SUCCESSFULLY_UPDATE";
    public static final String AUTHOR_SUCCESSFULLY_DELETE = "AUTHOR_SUCCESSFULLY_DELETE";

    public static final String TAG_SUCCESSFULLY_ADDED = "TAG_SUCCESSFULLY_ADDED";
    public static final String TAG_SUCCESSFULLY_READ = "TAG_SUCCESSFULLY_READ";
    public static final String TAG_LIST_SUCCESSFULLY_READ = "TAG_LIST_SUCCESSFULLY_READ";
    public static final String TAG_SUCCESSFULLY_UPDATE = "TAG_SUCCESSFULLY_UPDATE";
    public static final String TAG_SUCCESSFULLY_DELETE = "TAG_SUCCESSFULLY_DELETE";
    public static final String TAG_SUCCESSFULLY_FOUND = "TAG_SUCCESSFULLY_FOUND";

    public static final String DAO_TECHNICAL_SQL_EXCEPTION = "DAO_TECHNICAL_SQL_EXCEPTION";
    public static final String DAO_LOGIC_NOTHING_TO_RETURN_EXCEPTION = "DAO_LOGIC_NOTHING_TO_RETURN_EXCEPTION";
    public static final String EXCEPTION_MESSAGE = "EXCEPTION_MESSAGE";

    public static final String AUTHOR_IS_EMPTY = "AUTHOR_IS_EMPTY";
    public static final String COMMENT_IS_EMPTY = "COMMENT_IS_EMPTY";
    public static final String NEWS_IS_EMPTY = "NEWS_IS_EMPTY";
    public static final String TAG_IS_EMPTY = "TAG_IS_EMPTY";

    public static final String CAN_NOT_CREATE_RECORD = "CAN_NOT_CREATE_RECORD";
    public static final String CAN_NOT_UPDATE_RECORD = "CAN_NOT_UPDATE_RECORD";
    public static final String CAN_NOT_READ_RECORD = "CAN_NOT_READ_RECORD";
    public static final String CAN_NOT_READ_ALL_RECORDS = "CAN_NOT_READ_ALL_RECORDS";
    public static final String CAN_NOT_DELETE_RECORD = "CAN_NOT_DELETE_RECORD";
    public static final String CAN_NOT_FIND_TAG_BY_AUTHOR = "CAN_NOT_FIND_TAG_BY_AUTHOR";
    public static final String CAN_NOT_FIND_NEWS_BY_AUTHOR = "CAN_NOT_FIND_NEWS_BY_AUTHOR";
    public static final String CAN_NOT_FIND_NEWS_BY_TAG = "CAN_NOT_FIND_NEWS_BY_TAG";


    /**
     * Method that returns unique messagesManager instance
     * Standard realization of singleton pattern/
     *
     * @return single LoggerMessageManager instance
     */
    public static LoggerMessageManager getInstance() {
        if (instance == null) {
            instance = new LoggerMessageManager();
            instance.resourceBundle =
                    ResourceBundle.getBundle(BUNDLE_NAME);
        }
        return instance;
    }

    /**
     * This method enables us to obtain one of the records of the file
     * according to its signature transmitted parameter
     *
     * @param key String key for message from logger_messages.properties
     * @return String record from logger_messages.properties
     */
    public String getProperty(String key) {
        return (String) resourceBundle.getObject(key);
    }
}
