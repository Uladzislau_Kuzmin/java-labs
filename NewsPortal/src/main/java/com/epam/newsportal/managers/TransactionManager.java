package com.epam.newsportal.managers;


import com.epam.newsportal.dao.CommentDao;

import java.util.ResourceBundle;

/**
 * <code>TransactionManager</code>
 * <br>
 * Class TransactionManager is one of several file managers
 * and its function is as follows associates information from
 * <file>transaction.properties</file> with constants which
 * describes in it.
 * <br>
 * Realized pattern Singleton.
 *
 * @see java.util.ResourceBundle
 */
public class TransactionManager {
    CommentDao commentDao;

    /**
     * single instance of TransactionManager
     */
    private static TransactionManager instance;

    /**
     * ResourceBundle instance to bind with file which content transactions strings
     */
    private ResourceBundle resourceBundle;

    /**
     * Constants whose names coincide with the names of entries
     * that they can be called from transaction.properties file
     */
    private static final String BUNDLE_NAME = "transaction";

    public static final String INSERT_NEWS_RECORD = "INSERT_NEWS_RECORD";
    public static final String READ_NEWS_BY_ID = "READ_NEWS_RECORD_BY_ID";
    public static final String UPDATE_NEWS_RECORD = "UPDATE_NEWS_RECORD";
    public static final String DELETE_NEWS_RECORD = "DELETE_NEWS_RECORD";
    public static final String SELECT_ALL_NEWS = "SELECT_ALL_NEWS";
    public static final String SELECT_NEWS_BY_TAG_ID = "SELECT_NEWS_BY_TAG_ID";
    public static final String SELECT_MOST_COMMENTED_NEWS = "SELECT_MOST_COMMENTED_NEWS";
    public static final String SELECT_NEWS_RECORD_BY_AUTHOR_ID = "SELECT_NEWS_RECORD_BY_AUTHOR_ID";

    public static final String INSERT_COMMENTS_RECORD = "INSERT_COMMENTS_RECORD";
    public static final String READ_COMMENT_RECORD_BY_ID = "READ_COMMENT_RECORD_BY_ID";
    public static final String UPDATE_COMMENT_RECORD = "UPDATE_COMMENT_RECORD";
    public static final String DELETE_COMMENT_RECORD = "DELETE_COMMENT_RECORD";
    public static final String SELECT_COMMENT_RECORD_BY_AUTHOR_ID = "SELECT_COMMENT_RECORD_BY_AUTHOR_ID";
    public static final String SELECT_ALL_COMMENTS = "SELECT_ALL_COMMENTS";

    public static final String INSERT_AUTHOR_RECORD = "INSERT_AUTHOR_RECORD";
    public static final String READ_AUTHOR_RECORD_BY_ID = "READ_AUTHOR_RECORD_BY_ID";
    public static final String SELECT_ALL_AUTHORS = "SELECT_ALL_AUTHORS";
    public static final String UPDATE_AUTHOR_RECORD = "UPDATE_AUTHOR_RECORD";
    public static final String DELETE_AUTHOR_RECORD = "DELETE_AUTHOR_RECORD";

    public static final String INSERT_TAG_RECORD = "INSERT_TAG_RECORD";
    public static final String READ_TAG_RECORD_BY_ID = "READ_TAG_RECORD_BY_ID";
    public static final String SELECT_ALL_TAGS = "SELECT_ALL_TAGS";
    public static final String UPDATE_TAG_RECORD = "UPDATE_TAG_RECORD";
    public static final String DELETE_TAG_RECORD = "DELETE_TAG_RECORD";
    public static final String SELECT_TAG_RECORD_BY_AUTHOR_ID = "SELECT_TAG_RECORD_BY_AUTHOR_ID";

    public static final String INSERT_NEWS_TAG_RECORD = "INSERT_NEWS_TAG_RECORD";
    public static final String INSERT_NEWS_AUTHOR_RECORD = "INSERT_NEWS_AUTHOR_RECORD";


    /**
     * Method that returns unique TransactionManager instance
     * Standard realization of singleton pattern
     *
     * @return somgle TransactionManager instance
     */
    public static TransactionManager getInstance() {
        if (instance == null) {
            instance = new TransactionManager();
            instance.resourceBundle =
                    ResourceBundle.getBundle(BUNDLE_NAME);
        }
        return instance;
    }

    /**
     * This method enables us to obtain one of the records of the file
     * according to its signature transmitted parameter
     *
     * @param key String key for message from logger_messages.properties
     * @return String record from transaction.properties
     */
    public String getProperty(String key) {
        return (String) resourceBundle.getObject(key);
    }

}

