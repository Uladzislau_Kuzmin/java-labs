package com.epam.newsportal.exception;

/**
 * Public class <code>ServiceLayerLogicException</code> is a
 * user-defined exception subclass of <code>ServiceLayerException</code>
 * and used when information in Service layer methods is untrue, or
 * behavior of this methods differs from the present.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.exception.ApplicationException
 * @since 1.0
 */
public class ServiceLayerLogicException extends ServiceLayerException{

    /**
     * Constructs an {@code DaoLayerTechnicalException} with the
     * specified detail message and error object that trow up when
     * exception generate.
     *
     * @param message some string which describe exception cause
     * @param cause   exception object that throw up
     */
    public ServiceLayerLogicException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs an {@code DaoLayerTechnicalException} with the
     * specified detail message.
     *
     * @param message some string which describe exception cause
     */
    public ServiceLayerLogicException(String message) {
        super(message);
    }

    /**
     * Constructs an {@code DaoLayerTechnicalException} with the
     * specified object exception message.
     *
     * @param cause exception object that throw up
     */
    public ServiceLayerLogicException(Exception cause) {
        super(cause);
    }

    /**
     * Return the cause of exception situation
     *
     * @return exception object that throw up
     */
    @Override
    public Exception getHiddenException() {
        return super.getHiddenException();
    }
}
