package com.epam.newsportal.exception;

/**
 * Public class <code>ServiceLayerException</code>
 * user-define exception class and can be used for catching
 * anny subclasses exceptions on the height level of application.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.exception.ApplicationException
 * @since 1.0
 */
public class ServiceLayerException extends ApplicationException {

    /**
     * Constructs an {@code DaoLayerTechnicalException} with the
     * specified detail message and error object that trow up when
     * exception generate.
     *
     * @param message some string which describe exception cause
     * @param cause   exception object that throw up
     */
    public ServiceLayerException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs an {@code DaoLayerTechnicalException} with the
     * specified detail message.
     *
     * @param message some string which describe exception cause
     */
    public ServiceLayerException(String message) {
        super(message);
    }

    /**
     * Constructs an {@code DaoLayerTechnicalException} with the
     * specified object exception message.
     *
     * @param cause exception object that throw up
     */
    public ServiceLayerException(Exception cause) {
        super(cause);
    }

    /**
     * Return the cause of exception situation
     *
     * @return exception object that throw up
     */
    @Override
    public Exception getHiddenException() {
        return super.getHiddenException();
    }
}
