package com.epam.newsportal.exception;

/**
 * Public class <code>DaoLayerTechnicalException</code>
 * user-define exception class and can be used when any
 * public methods in the Data Access layer throw up SQL-errors.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.exception.ApplicationException
 * @since 1.0
 */
public class DaoLayerTechnicalException extends ApplicationException {

    /**
     * Constructs an {@code DaoLayerTechnicalException} with the
     * specified detail message and error object that trow up when
     * exception generate.
     *
     * @param message some string which describe exception cause
     * @param cause   exception object that throw up
     */
    public DaoLayerTechnicalException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs an {@code DaoLayerTechnicalException} with the
     * specified detail message.
     *
     * @param message some string which describe exception cause
     */
    public DaoLayerTechnicalException(String message) {
        super(message);
    }

    /**
     * Constructs an {@code DaoLayerTechnicalException} with the
     * specified object exception message.
     *
     * @param cause exception object that throw up
     */
    public DaoLayerTechnicalException(Exception cause) {
        super(cause);
    }

    /**
     * Return the cause of exception situation
     *
     * @return exception object that throw up
     */
    @Override
    public Exception getHiddenException() {
        return super.getHiddenException();
    }
}
