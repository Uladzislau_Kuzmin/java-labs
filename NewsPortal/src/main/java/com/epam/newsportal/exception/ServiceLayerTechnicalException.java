package com.epam.newsportal.exception;

/**
 * Public class <code>ServiceLayerTechnicalException</code> is a
 * subclass of <code>ServiceLayerException</code> can be used for catching
 * anny Data Access layer exceptions, and throw them up.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.exception.ApplicationException
 * @since 1.0
 */
public class ServiceLayerTechnicalException extends ServiceLayerException {

    /**
     * Constructs an {@code DaoLayerTechnicalException} with the
     * specified detail message and error object that trow up when
     * exception generate.
     *
     * @param message some string which describe exception cause
     * @param cause   exception object that throw up
     */
    public ServiceLayerTechnicalException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs an {@code DaoLayerTechnicalException} with the
     * specified detail message.
     *
     * @param message some string which describe exception cause
     */
    public ServiceLayerTechnicalException(String message) {
        super(message);
    }

    /**
     * Constructs an {@code DaoLayerTechnicalException} with the
     * specified object exception message.
     *
     * @param cause exception object that throw up
     */
    public ServiceLayerTechnicalException(Exception cause) {
        super(cause);
    }

    /**
     * Return the cause of exception situation
     *
     * @return exception object that throw up
     */
    @Override
    public Exception getHiddenException() {
        return super.getHiddenException();
    }
}
