package com.epam.newsportal.exception;

/**
 * Public class <code>ApplicationException</code> is top of hierarchy
 * user defined exceptions. Is a wrapper over all user exceptions
 * and, if necessary, may be intercept them.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see java.lang.Exception
 * @since 1.0
 */
public class ApplicationException extends Exception {

    /**
     * Private field that store exception cause
     */
    private Exception _hidden;

    /**
     * Constructs an {@code ApplicationException} with the specified detail message
     * and object that trow up when exception generate.
     *
     * @param message some string which describe exception cause
     * @param cause   exception object that throw up
     */
    public ApplicationException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs an {@code ApplicationException} with the specified detail message.
     *
     * @param message some string which describe exception cause
     */
    public ApplicationException(String message) {
        super(message);
    }

    /**
     * Constructs an {@code ApplicationException}
     * This constructor is useful for Application exceptions that are little more
     * than wrappers for other user-defines exceptions.
     *
     * @param cause exception object that throw up
     */
    public ApplicationException(Exception cause) {
        _hidden = cause;
    }

    /**
     * Return the cause of exception situation
     *
     * @return exception object that throw up
     */
    public Exception getHiddenException() {
        return _hidden;
    }
}
