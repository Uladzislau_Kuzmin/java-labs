package com.epam.newsportal.service;

import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.domain.Tag;
import com.epam.newsportal.exception.ServiceLayerLogicException;
import com.epam.newsportal.exception.ServiceLayerTechnicalException;

import java.util.List;

/**
 * The second level of the hierarchy Service layer interfaces.
 * This interface <code>NewsService</code> describes the
 * behavior of a particular service layer which working with
 * instance of <code>News</code> using Data Access layer
 * implementations of interface <code>NewsDao</code>.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.service.GenericService
 * @see com.epam.newsportal.dao.NewsDao
 * @see com.epam.newsportal.domain.News
 * @since 1.0
 */
public interface NewsService extends GenericService<News> {

    /**
     * Public abstract method which one defining sort operation
     * of  News table from database by count of comments, using
     * the sortedNewsByComments() method in NewsDao interface.
     *
     * @return sorted list of News instances
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     * @see com.epam.newsportal.exception.DaoLayerTechnicalException
     */
    public List<News> sortedNewsByComments() throws ServiceLayerTechnicalException;

    /**
     * Public abstract method which one defining search operation
     * from News table in data base by sent Tag instance,
     * using the searchNewsByTag() method in NewsDao interface.
     *
     * @param tag element of Tag instance to search for news
     * @return list of News which was found by the Tag instance
     * @throws ServiceLayerLogicException     user-defined exception occurs when submitted entity equals null
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     * @see com.epam.newsportal.exception.DaoLayerTechnicalException
     */
    public List<News> searchNewsByTag(Tag tag) throws ServiceLayerLogicException, ServiceLayerTechnicalException;

    /**
     * Public abstract method which one defining search operation
     * from News table in data base by sent Author instance,
     * using the searchNewsByAuthor() method in NewsDao interface.
     *
     * @param author element of Author instance to search for News list
     * @return list of News which was find by the Author instance
     * @throws ServiceLayerLogicException     user-defined exception occurs when submitted entity equals null
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     * @see com.epam.newsportal.exception.DaoLayerTechnicalException
     */
    public List<News> searchNewsByAuthor(Author author) throws ServiceLayerLogicException, ServiceLayerTechnicalException;
}
