package com.epam.newsportal.service;

import com.epam.newsportal.domain.Comment;

/**
 * The second level of the hierarchy Service layer interfaces.
 * This interface <code>CommentService</code> describes the
 * behavior of a particular service layer which working with
 * instance of <code>Comment</code> using Data Access layer
 * implementations of interface <code>CommentDao</code>.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.service.GenericService
 * @see com.epam.newsportal.dao.CommentDao
 * @see com.epam.newsportal.domain.Comment
 * @since 1.0
 */
public interface CommentService extends GenericService<Comment> {

    //place for new methods
}
