package com.epam.newsportal.service;

import com.epam.newsportal.exception.ServiceLayerLogicException;
import com.epam.newsportal.exception.ServiceLayerTechnicalException;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.List;

/**
 * Generic interface <code>GenericService</code>
 * <br>
 * The top of the inheritance hierarchy of interfaces
 * Service layer contains a set of four standard
 * operations for working with Data Access layer objects
 * common to all simple Service implementations.
 *
 * @param <Entity> the type of elements in this interface
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.dao.GenericDao
 * @since 1.0
 */
public interface GenericService<Entity extends Serializable> {

    /**
     * object to conduct logging
     */
    public static final Logger logger = Logger.getLogger(GenericService.class.getName());

    /**
     * Public abstract method which defining the operation
     * of adding record in a database, using, using the
     * create() method in GenericDao interface.
     *
     * @param entity record to be added
     * @return inserted entityID
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     * @throws ServiceLayerLogicException     user-defined exception occurs when submitted entity equals null
     * @see com.epam.newsportal.exception.DaoLayerTechnicalException
     */
    public abstract long create(Entity entity) throws ServiceLayerLogicException, ServiceLayerTechnicalException;

    /**
     * Public abstract method which one defining the operation
     * of reading record in a database, using the read() method
     * in GenericDao interface.
     *
     * @param entityID unique identifier of a record to read
     * @return entity record to be read
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     * @see com.epam.newsportal.exception.DaoLayerTechnicalException
     */
    public abstract Entity read(long entityID) throws ServiceLayerTechnicalException;

    /**
     * Public abstract method which one defining the operation
     * of reading all records in a database, using the readAll() method
     * of GenericDao interface.
     *
     * @return list of entity records to read
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     * @see com.epam.newsportal.exception.DaoLayerTechnicalException
     */
    public abstract List<Entity> readAll() throws ServiceLayerTechnicalException;

    /**
     * Public abstract method which one defining the operation
     * of updating record in a database, using the update() method
     * of GenericDao interface.
     *
     * @param entity record to updated
     * @throws ServiceLayerLogicException     user-defined exception occurs when submitted entity equals null
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     * @throws ServiceLayerLogicException     user-defined exception occurs when entity equals null
     * @see com.epam.newsportal.exception.DaoLayerTechnicalException
     */
    public abstract void update(Entity entity) throws ServiceLayerLogicException, ServiceLayerTechnicalException;

    /**
     * Public abstract method which one defining the operation
     * of deleting record from a database, using the delete() method
     * of GenericDao interface.
     *
     * @param entityID unique identifier of a record to deleted
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     * @see com.epam.newsportal.exception.DaoLayerTechnicalException
     */
    public abstract void delete(long entityID) throws ServiceLayerTechnicalException;

}
