package com.epam.newsportal.service.implementation;

import com.epam.newsportal.dao.NewsDao;
import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.domain.Tag;
import com.epam.newsportal.exception.DaoLayerTechnicalException;
import com.epam.newsportal.exception.ServiceLayerLogicException;
import com.epam.newsportal.exception.ServiceLayerTechnicalException;
import com.epam.newsportal.managers.LoggerMessageManager;
import com.epam.newsportal.service.NewsService;
import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Public class <code>NewsServiceImpl</code> is an element
 * of Service layer and working with News data base
 * instance. This is a realization of <code>NewsService</code>
 * interface and it performs all operations described in this
 * interface.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.service.NewsService
 * @see com.epam.newsportal.domain.News
 * @since 1.0
 */
@Service
public class NewsServiceImpl implements NewsService {

    /**
     * link to the interface part, providing access to basic operations
     */
    @Autowired
    private NewsDao newsDao;

    /**
     * Override public method used to add record to the table News,
     * which calls create() method from NewsDao interface.
     *
     * @param news instance which will be add to the data base
     * @return unique identifier inserted instance
     * @throws ServiceLayerTechnicalException user-defined exception occurs when
     *                                        when any <code>SQLException</code> throw up
     * @throws ServiceLayerLogicException     user-defined exception occurs when submitted entity equals null
     */
    @Override
    public long create(News news) throws ServiceLayerTechnicalException, ServiceLayerLogicException {
        long insertedNewsId = 0;

        if (news == null) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.NEWS_IS_EMPTY));
            throw new ServiceLayerLogicException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.NEWS_IS_EMPTY));
        }

        try {
            insertedNewsId = newsDao.create(news);
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.NEWS_SUCCESSFULLY_ADDED));

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_CREATE_RECORD));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_CREATE_RECORD));
        }
        return insertedNewsId;
    }

    /**
     * Override public method used to read record from the table News,
     * which calls read() method from NewsDao interface.
     *
     * @param newsId unique identifier of instance which will be found
     * @return find News instance
     * @throws ServiceLayerTechnicalException user-defined exception occurs when
     *                                        when any <code>SQLException</code> throw up
     */
    @Override
    public News read(long newsId) throws ServiceLayerTechnicalException {
        News news = null;

        try {
            news = newsDao.read(newsId);
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.NEWS_SUCCESSFULLY_READ));

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_READ_RECORD));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_READ_RECORD));
        }

        return news;
    }

    /**
     * Override public method used to read all records from the table News,
     * which calls readAll() method from NewsDao interface.
     *
     * @return list of find News instances
     * @throws ServiceLayerTechnicalException user-defined exception occurs when
     *                                        when any <code>SQLException</code> throw up
     */
    @Override
    public List<News> readAll() throws ServiceLayerTechnicalException {
        List<News> newsList = null;

        try {
            newsList = newsDao.readAll();
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.NEWS_LIST_SUCCESSFULLY_READ));

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_READ_ALL_RECORDS));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_READ_ALL_RECORDS));
        }

        return newsList;
    }

    /**
     * Override public method used to update record in the table News,
     * which calls update() method from NewsDao interface.
     *
     * @param news instance which will be update in the data base
     * @throws ServiceLayerTechnicalException user-defined exception occurs when
     *                                        when any <code>SQLException</code> throw up
     * @throws ServiceLayerLogicException     user-defined exception occurs when submitted entity equals null
     */
    @Override
    public void update(News news) throws ServiceLayerTechnicalException, ServiceLayerLogicException {
        if (news == null) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.NEWS_SUCCESSFULLY_READ));
            throw new ServiceLayerLogicException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.NEWS_IS_EMPTY));
        }

        try {
            newsDao.update(news);
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.NEWS_SUCCESSFULLY_UPDATE));

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_UPDATE_RECORD));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_UPDATE_RECORD));
        }
    }

    /**
     * Override public method used to delete record from the table News,
     * which calls delete() method from NewsDao interface.
     *
     * @param newsId unique identifier of instance which will be delete
     * @throws ServiceLayerTechnicalException user-defined exception occurs when
     *                                        when any <code>SQLException</code> throw up
     */
    @Override
    public void delete(long newsId) throws ServiceLayerTechnicalException {
        try {
            newsDao.delete(newsId);
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.NEWS_SUCCESSFULLY_DELETE));

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_DELETE_RECORD));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_DELETE_RECORD));
        }
    }

    /**
     * Override public method used to get sorted, by comments count,
     * list of records from the table News, which calls
     * sortedNewsByComments() method from NewsDao interface.
     *
     * @return sorted list of News instance
     * @throws ServiceLayerTechnicalException user-defined exception occurs when
     *                                        when any <code>SQLException</code> throw up
     */
    @Override
    public List<News> sortedNewsByComments() throws ServiceLayerTechnicalException {
        List<News> newsList = null;

        try {
            newsList = newsDao.sortedNewsByComments();
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.MOST_COMMENTED_NEWS_SUCCESSFULLY_READ));

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_READ_ALL_RECORDS));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_READ_ALL_RECORDS));
        }
        return newsList;
    }

    /**
     * Override public method used to search list of News records
     * from the table News by Tag instance,  which calls
     * searchNewsByTag() method from NewsDao interface.
     *
     * @param tag element of Tag instance to search for news
     * @return list of News which was found by the Tag instance
     * @throws ServiceLayerLogicException     user-defined exception occurs when submitted entity equals null
     * @throws ServiceLayerTechnicalException user-defined exception occurs when
     *                                        when any <code>SQLException</code> throw up
     */
    @Override
    public List<News> searchNewsByTag(Tag tag) throws ServiceLayerLogicException, ServiceLayerTechnicalException {
        List<News> newsList = null;

        if (tag == null) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.TAG_IS_EMPTY));
            throw new ServiceLayerLogicException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.TAG_IS_EMPTY));
        }

        try {
            newsList = newsDao.searchNewsByTag(tag);
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.NEWS_LIST_BY_TAG_ID_SUCCESSFULLY_READ) +
                    tag.getTagId() + "'");

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_FIND_NEWS_BY_TAG));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_FIND_NEWS_BY_TAG));
        }
        return newsList;
    }

    /**
     * Override public method used to search list of News records
     * from the table News by Author instance,  which calls
     * searchNewsByAuthor() method from NewsDao interface.
     *
     * @param author element of Author instance to search for News list
     * @return list of News which was find by the Author instance
     * @throws ServiceLayerLogicException     user-defined exception occurs when submitted entity equals null
     * @throws ServiceLayerTechnicalException user-defined exception occurs when
     *                                        when any <code>SQLException</code> throw up
     */
    @Override
    public List<News> searchNewsByAuthor(Author author) throws ServiceLayerLogicException, ServiceLayerTechnicalException {

        List<News> newsList = null;

        if (author == null) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.AUTHOR_IS_EMPTY));
            throw new ServiceLayerLogicException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.AUTHOR_IS_EMPTY));
        }

        try {
            newsList = newsDao.searchNewsByAuthor(author);
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.NEWS_SUCCESSFULLY_FOUND) +
                    author.getAuthorId() + "'");

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_FIND_NEWS_BY_AUTHOR));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_FIND_NEWS_BY_AUTHOR));
        }

        return newsList;
    }
}
