package com.epam.newsportal.service.implementation;

import com.epam.newsportal.dao.TagDao;
import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.domain.Tag;
import com.epam.newsportal.exception.DaoLayerTechnicalException;
import com.epam.newsportal.exception.ServiceLayerLogicException;
import com.epam.newsportal.exception.ServiceLayerTechnicalException;
import com.epam.newsportal.managers.LoggerMessageManager;
import com.epam.newsportal.service.TagService;
import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Public class <code>TagServiceImpl</code> is an element
 * of Service layer and working with Tag data base
 * instance. This is a realization of <code>TagService</code>
 * interface and it performs all operations described in this
 * interface.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.service.TagService
 * @see com.epam.newsportal.domain.Tag
 * @since 1.0
 */
@Service
public class TagServiceImpl implements TagService {

    /**
     * link to the interface part, providing access to basic operations
     */
    @Autowired
    private TagDao tagDao;

    /**
     * Override public method used to add record to the table Tag,
     * which calls create() method from TagDao interface.
     *
     * @param tag instance which will be add to the data base
     * @return unique identifier inserted instance
     * @throws ServiceLayerLogicException     user-defined exception occurs when submitted entity equals null
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     */
    @Override
    public long create(Tag tag) throws ServiceLayerLogicException, ServiceLayerTechnicalException {
        long insertedTagId = 0;

        if (tag == null) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.TAG_IS_EMPTY));
            throw new ServiceLayerLogicException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.TAG_IS_EMPTY));
        }

        try {
            insertedTagId = tagDao.create(tag);
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.TAG_SUCCESSFULLY_ADDED));

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_CREATE_RECORD));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_CREATE_RECORD));
        }
        return insertedTagId;
    }

    /**
     * Override public method used to read record from the table Tag,
     * which calls read() method from TagDao interface.
     *
     * @param tagId unique identifier of instance which will be found
     * @return find Tag instance
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     */
    @Override
    public Tag read(long tagId) throws ServiceLayerTechnicalException {
        Tag tag = null;

        try {
            tag = tagDao.read(tagId);
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.TAG_SUCCESSFULLY_READ));

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_READ_RECORD));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_READ_RECORD));
        }

        return tag;
    }

    /**
     * Override public method used to read all records from the table Tag,
     * which calls readAll() method from TagDao interface.
     *
     * @return list of find Tags instances
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     */
    @Override
    public List<Tag> readAll() throws ServiceLayerTechnicalException {

        List<Tag> tagList = null;
        try {
            tagList = tagDao.readAll();
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.TAG_LIST_SUCCESSFULLY_READ));

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_READ_ALL_RECORDS));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_READ_ALL_RECORDS));
        }

        return tagList;
    }

    /**
     * Override public method used to delete record from the table Tag,
     * which calls update() method from TagDao interface.
     *
     * @param tag instance which will be update in the data base
     * @throws ServiceLayerLogicException     user-defined exception occurs when submitted entity equals null
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     */
    @Override
    public void update(Tag tag) throws ServiceLayerLogicException, ServiceLayerTechnicalException {
        if (tag == null) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.TAG_IS_EMPTY));
            throw new ServiceLayerLogicException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.TAG_IS_EMPTY));
        }

        try {
            tagDao.update(tag);
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.TAG_SUCCESSFULLY_UPDATE));

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_UPDATE_RECORD));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_UPDATE_RECORD));
        }
    }

    /**
     * Override public method used to delete record from the table Tag,
     * which calls delete() method from TagDao interface.
     *
     * @param tagId unique identifier of instance which will be delete
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     */
    @Override
    public void delete(long tagId) throws ServiceLayerTechnicalException {
        try {
            tagDao.delete(tagId);
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.TAG_SUCCESSFULLY_DELETE));

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_DELETE_RECORD));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_DELETE_RECORD));
        }
    }

    /**
     * Override public method used to search list of Tags record
     * from the table Tag by Author instance. which calls
     * searchTagByAuthor() method from TagDao interface.
     *
     * @param author element of Author instance to search for Tag list
     * @return list of Tag which was find by the Author instance
     * @throws ServiceLayerLogicException     user-defined exception occurs when submitted entity equals null
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     */
    @Override
    public List<Tag> searchTagByAuthor(Author author) throws ServiceLayerLogicException, ServiceLayerTechnicalException {
        List<Tag> tagList = null;

        if (author == null) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.AUTHOR_IS_EMPTY));
            throw new ServiceLayerLogicException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.AUTHOR_IS_EMPTY));
        }

        try {
            tagList = tagDao.searchTagByAuthor(author);
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.TAG_SUCCESSFULLY_FOUND) + author.getAuthorId() + "'");

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_CREATE_RECORD));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_FIND_TAG_BY_AUTHOR));
        }

        return tagList;
    }

    /**
     * Override public method used to bind Tag instance with
     * instance of News, and add same record to the NewsTag
     * table in data base, which calls
     * createTagForNews() method from TagDao interface.
     *
     * @param news News instance for binding with Tag instance
     * @param tag  Tag instance for binding with News instance
     * @throws ServiceLayerLogicException     user-defined exception occurs when submitted entity equals null
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     */
    @Override
    public void createTagForNews(News news, Tag tag) throws ServiceLayerLogicException, ServiceLayerTechnicalException {
        if (news == null) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.NEWS_IS_EMPTY));
            throw new ServiceLayerLogicException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.NEWS_IS_EMPTY));

        } else if (tag == null) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.TAG_IS_EMPTY));
            throw new ServiceLayerLogicException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.TAG_IS_EMPTY));
        }

        try {
            tagDao.createTagForNews(news, tag);
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.TAG_SUCCESSFULLY_ADDED));

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_CREATE_RECORD));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_CREATE_RECORD));
        }
    }
}
