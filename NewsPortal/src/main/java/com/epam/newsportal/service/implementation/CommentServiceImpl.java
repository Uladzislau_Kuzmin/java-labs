package com.epam.newsportal.service.implementation;

import com.epam.newsportal.dao.CommentDao;
import com.epam.newsportal.domain.Comment;
import com.epam.newsportal.exception.DaoLayerTechnicalException;
import com.epam.newsportal.exception.ServiceLayerLogicException;
import com.epam.newsportal.exception.ServiceLayerTechnicalException;
import com.epam.newsportal.managers.LoggerMessageManager;
import com.epam.newsportal.service.CommentService;
import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Public class <code>CommentServiceImpl</code> is an element
 * of Service layer and working with Comment data base
 * instance. This is a realization of <code>CommentService</code>
 * interface and it performs all operations described in this
 * interface.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.service.CommentService
 * @see com.epam.newsportal.domain.Comment
 * @since 1.0
 */
@Service
public class CommentServiceImpl implements CommentService {

    /**
     * link to the interface part, providing access to basic operations
     */
    @Autowired
    private CommentDao commentDao;

    /**
     * Override public method used to add record to the table Comment,
     * which calls create() method from CommentDao interface.
     *
     * @param comment instance which will be add to the data base
     * @return unique identifier inserted instance
     * @throws ServiceLayerLogicException     user-defined exception occurs when submitted entity equals null
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     */
    @Override
    public long create(Comment comment) throws ServiceLayerLogicException, ServiceLayerTechnicalException {
        long insertedCommentId = 0;

        if (comment == null) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.COMMENT_IS_EMPTY));
            throw new ServiceLayerLogicException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.COMMENT_IS_EMPTY));
        }

        try {
            insertedCommentId = commentDao.create(comment);
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.COMMENT_SUCCESSFULLY_ADDED));

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_CREATE_RECORD));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_CREATE_RECORD));
        }
        return insertedCommentId;
    }

    /**
     * Override public method used to read record from the table Comment,
     * which calls read() method from CommentDao interface.
     *
     * @param commentId unique identifier of instance which will be found
     * @return find Comment instance
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     */
    @Override
    public Comment read(long commentId) throws ServiceLayerTechnicalException {
        Comment comment = null;

        try {
            comment = commentDao.read(commentId);
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.NEWS_SUCCESSFULLY_READ));

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_READ_RECORD));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_READ_RECORD));
        }

        return comment;
    }

    /**
     * Override public method used to read all records from the table Comment,
     * which calls readAll() method from CommentDao interface.
     *
     * @return list of find Comment instances
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     */
    @Override
    public List<Comment> readAll() throws ServiceLayerTechnicalException {
        List<Comment> commentList = null;

        try {
            commentList = commentDao.readAll();
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.COMMENT_LIST_SUCCESSFULLY_READ));

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_READ_ALL_RECORDS));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_READ_ALL_RECORDS));
        }

        return commentList;
    }

    /**
     * Override public method used to update record in the table Comment,
     * which calls update() method from CommentDao interface.
     *
     * @param comment instance which will be update in the data base
     * @throws ServiceLayerLogicException     user-defined exception occurs when submitted entity equals null
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     */
    @Override
    public void update(Comment comment) throws ServiceLayerLogicException, ServiceLayerTechnicalException {
        if (comment == null) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.COMMENT_IS_EMPTY));
            throw new ServiceLayerLogicException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.COMMENT_IS_EMPTY));
        }

        try {
            commentDao.update(comment);
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.COMMENT_SUCCESSFULLY_UPDATE));

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_UPDATE_RECORD));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_UPDATE_RECORD));
        }
    }

    /**
     * Override public method used to delete record from the table Comment,
     * which calls delete() method from CommentDao interface.
     *
     * @param commentId unique identifier of instance which will be delete
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     */
    @Override
    public void delete(long commentId) throws ServiceLayerTechnicalException {
        try {
            commentDao.delete(commentId);
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.COMMENT_SUCCESSFULLY_DELETE));

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_DELETE_RECORD));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_DELETE_RECORD));
        }
    }
}
