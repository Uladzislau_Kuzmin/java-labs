package com.epam.newsportal.service.implementation;

import com.epam.newsportal.dao.AuthorDao;
import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.exception.DaoLayerTechnicalException;
import com.epam.newsportal.exception.ServiceLayerLogicException;
import com.epam.newsportal.exception.ServiceLayerTechnicalException;
import com.epam.newsportal.managers.LoggerMessageManager;
import com.epam.newsportal.service.AuthorService;
import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Public class <code>AuthorServiceImpl</code> is an element
 * of Service layer and working with Author data base
 * instance. This is a realization of <code>AuthorService</code>
 * interface and it performs all operations described in this
 * interface.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.service.AuthorService
 * @see com.epam.newsportal.domain.Author
 * @since 1.0
 */
@Service
public class AuthorServiceImpl implements AuthorService {

    /**
     * link to the interface part, providing access to basic operations
     */
    @Autowired
    private AuthorDao authorDao;

    /**
     * Override public method used to add record to the table Author,
     * which calls create() method from AuthorDao interface.
     *
     * @param author instance which will be add to the data base
     * @return unique identifier inserted instance
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     * @throws ServiceLayerLogicException     user-defined exception occurs when submitted entity equals null
     */
    @Override
    public long create(Author author) throws ServiceLayerLogicException, ServiceLayerTechnicalException {
        long insertedAuthorId = 0;

        if (author == null) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.AUTHOR_IS_EMPTY));
            throw new ServiceLayerLogicException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.AUTHOR_IS_EMPTY));
        }

        try {
            insertedAuthorId = authorDao.create(author);
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.AUTHOR_SUCCESSFULLY_ADDED));

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_CREATE_RECORD));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_CREATE_RECORD));
        }
        return insertedAuthorId;
    }

    /**
     * Override public method used to read record from the table Author,
     * which calls read() method from AuthorDao interface.
     *
     * @param authorId unique identifier of instance which will be found
     * @return find Author instance
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     */
    @Override
    public Author read(long authorId) throws ServiceLayerTechnicalException {
        Author author = null;
        try {
            author = authorDao.read(authorId);
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.AUTHOR_SUCCESSFULLY_READ));

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_READ_RECORD));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_READ_RECORD));
        }

        return author;
    }

    /**
     * Override public method used to read all records from the table Author,
     * which calls readAll() method from AuthorDao interface.
     *
     * @return list of find Author instances
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     */
    @Override
    public List<Author> readAll() throws ServiceLayerTechnicalException {
        List<Author> authorList = null;

        try {
            authorList = authorDao.readAll();
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.AUTHOR_LIST_SUCCESSFULLY_READ));

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_READ_ALL_RECORDS));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_READ_ALL_RECORDS));
        }

        return authorList;
    }

    /**
     * Override public method used to update record in the table Author,
     * which calls update() method from AuthorDao interface.
     *
     * @param author instance which will be update in the data base
     * @throws ServiceLayerLogicException     user-defined exception occurs when submitted entity equals null
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     */
    @Override
    public void update(Author author) throws ServiceLayerLogicException, ServiceLayerTechnicalException {
        if (author == null) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.AUTHOR_IS_EMPTY));
            throw new ServiceLayerLogicException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.AUTHOR_IS_EMPTY));
        }

        try {
            authorDao.update(author);
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.AUTHOR_SUCCESSFULLY_UPDATE));

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_UPDATE_RECORD));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_UPDATE_RECORD));
        }
    }

    /**
     * Override public method used to delete record from the table Author,
     * which calls delete() method from AuthorDao interface.
     *
     * @param authorId unique identifier of instance which will be delete
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     */
    @Override
    public void delete(long authorId) throws ServiceLayerTechnicalException {

        try {
            authorDao.delete(authorId);
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.AUTHOR_SUCCESSFULLY_DELETE));

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_DELETE_RECORD));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_DELETE_RECORD));
        }
    }

    /**
     * Override public method used to bind Author instance with
     * instance of News, and add same record to the NewsAuthor
     * table in data base, calls createAuthorForNews() method from
     * AuthorDao interface.
     *
     * @param news   News instance for binding with Author instance
     * @param author Author instance for binding with News instance
     * @throws ServiceLayerLogicException     user-defined exception occurs when submitted entity equals null
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     */
    @Override
    public void createAuthorForNews(News news, Author author) throws ServiceLayerLogicException, ServiceLayerTechnicalException {
        if (news == null) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.NEWS_IS_EMPTY));
            throw new ServiceLayerLogicException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.NEWS_IS_EMPTY));

        } else if (author == null) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.AUTHOR_IS_EMPTY));
            throw new ServiceLayerLogicException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.AUTHOR_IS_EMPTY));
        }

        try {
            authorDao.createAuthorForNews(news, author);
            logger.log(Level.INFO, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.AUTHOR_SUCCESSFULLY_ADDED));

        } catch (DaoLayerTechnicalException e) {
            logger.log(Level.ERROR, LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_CREATE_RECORD));
            throw new ServiceLayerTechnicalException(LoggerMessageManager.getInstance().
                    getProperty(LoggerMessageManager.CAN_NOT_CREATE_RECORD));
        }
    }
}
