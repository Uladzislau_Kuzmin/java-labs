package com.epam.newsportal.service.implementation;

import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.domain.Tag;
import com.epam.newsportal.exception.ServiceLayerLogicException;
import com.epam.newsportal.exception.ServiceLayerTechnicalException;
import com.epam.newsportal.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Public class <code>NewsManagerServiceImpl<code/> is an
 * implementation of interface <code>NewsManagementService</code>
 * and overrides all methods describing transactional
 * operations for which, you must use a few simple Service
 * layer interfaces.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.service.NewsManagementService
 * @since 1.0
 */
@Component(value = "newsManagementService")
public class NewsManagerServiceImpl implements NewsManagementService {

    /**
     * author instance for using all service methods
     */
    @Autowired
    private AuthorService authorService;

    /**
     * news instance for using all service methods
     */
    @Autowired
    private NewsService newsService;

    /**
     * news instance for using all service methods
     */
    @Autowired
    private TagService tagService;

    /**
     * comment instance for using all service methods
     */
    @Autowired
    private CommentService commentService;


    /**
     * Override method that perform operation which
     * save news with list of binging tags and author in one step.
     * This is a transactional operation.
     *
     * @param news    News instance for saving in database
     * @param author  Author instance for saving in database
     * @param tagList list of Tag instances for saving in database
     * @throws ServiceLayerLogicException     user-defined exception occurs when submitted entity equals null
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     */
    @Override
    @Transactional(rollbackFor = {ServiceLayerLogicException.class, ServiceLayerTechnicalException.class})
    public void saveNews(News news, Author author, List<Tag> tagList) throws ServiceLayerLogicException, ServiceLayerTechnicalException {

        authorService.create(author);
        newsService.create(news);
        for (Tag tag : tagList) {
            tagService.create(tag);
        }

        authorService.createAuthorForNews(news, author);

        for (Tag tag : tagList) {
            tagService.createTagForNews(news, tag);
        }
    }
}
