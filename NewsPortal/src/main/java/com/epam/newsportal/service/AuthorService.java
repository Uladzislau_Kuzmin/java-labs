package com.epam.newsportal.service;


import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.exception.ServiceLayerLogicException;
import com.epam.newsportal.exception.ServiceLayerTechnicalException;

/**
 * The second level of the hierarchy Service layer interfaces.
 * This interface <code>AuthorService</code> describes the
 * behavior of a particular service layer which working with
 * instance of <code>Author</code> using Data Access layer
 * implementations of interface <code>AuthorDao</code>.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.service.GenericService
 * @see com.epam.newsportal.dao.AuthorDao
 * @see com.epam.newsportal.domain.Author
 * @since 1.0
 */
public interface AuthorService extends GenericService<Author> {

    /**
     * Public abstract method which one defining operation
     * binding the News instance with the Author instance
     * in the table NewsAuthor in data base, using the
     * createAuthorForNews() method in AuthorDao interface.
     *
     * @param news   News instance for binding with Author instance
     * @param author Author instance for binding with News instance
     * @throws ServiceLayerLogicException     user-defined exception occurs when submitted entity equals null
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     * @see com.epam.newsportal.exception.DaoLayerTechnicalException
     */
    public abstract void createAuthorForNews(News news, Author author) throws ServiceLayerLogicException, ServiceLayerTechnicalException;
}
