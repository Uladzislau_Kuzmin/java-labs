package com.epam.newsportal.service;

import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.domain.Tag;
import com.epam.newsportal.exception.ServiceLayerLogicException;
import com.epam.newsportal.exception.ServiceLayerTechnicalException;

import java.util.List;

/**
 * The second level of the hierarchy Service layer interfaces.
 * This interface <code>TagService</code> describes the
 * behavior of a particular service layer which working with
 * instance of <code>Tag</code> using Data Access layer
 * implementations of interface <code>TagDao</code>.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsportal.service.GenericService
 * @see com.epam.newsportal.dao.TagDao
 * @see com.epam.newsportal.domain.Tag
 * @since 1.0
 */
public interface TagService extends GenericService<Tag> {

    /**
     * Public abstract method which one defining search operation
     * from Tag table in data base by sent Author instance, using
     * the searchTagByAuthor() method in TagDao interface.
     *
     * @param author element of Author instance to search for Tag list
     * @return list of Tag which was find by the Author instance
     * @throws ServiceLayerLogicException     user-defined exception occurs when submitted entity equals null
     * @throws ServiceLayerTechnicalException @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     * @see com.epam.newsportal.exception.DaoLayerTechnicalException
     */
    public List<Tag> searchTagByAuthor(Author author) throws ServiceLayerLogicException, ServiceLayerTechnicalException;

    /**
     * Public abstract method which one defining operation
     * binding the News instance with the Tag instance in
     * the table NewsTag in data base, using the
     * createTagForNews() method in TagDao interface.
     *
     * @param news News instance for binding with Tag instance
     * @param tag  Tag instance for binding with News instance
     * @throws ServiceLayerLogicException     user-defined exception occurs when submitted entity equals null
     * @throws ServiceLayerTechnicalException @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     * @see com.epam.newsportal.exception.DaoLayerTechnicalException
     */
    public abstract void createTagForNews(News news, Tag tag) throws ServiceLayerLogicException, ServiceLayerTechnicalException;
}
