package com.epam.newsportal.service;


import com.epam.newsportal.domain.Author;
import com.epam.newsportal.domain.News;
import com.epam.newsportal.domain.Tag;
import com.epam.newsportal.exception.ServiceLayerLogicException;
import com.epam.newsportal.exception.ServiceLayerTechnicalException;

import java.util.List;

/**
 * General interface <code>NewsManagementService</code> describing
 * transactional operations for which, you must use a few simple
 * Service layer interfaces.
 *
 *
 * The main interface in all Service layer interfaces.
 * This interface <code>NewsManagementService</code> describing
 * transactional operations for which, you must use a few simple
 * Service layer interfaces implementations.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @since 1.0
 */
public interface NewsManagementService {

    /**
     * Public abstract method that perform operation which
     * save news with list of binging tags and author in one step.
     * This is a transactional operation.
     *
     * @param news    News instance for saving in database
     * @param author  Author instance for saving in database
     * @param tagList list of Tag instances for saving in database
     * @throws ServiceLayerLogicException     user-defined exception occurs when submitted entity equals null
     * @throws ServiceLayerTechnicalException user-defined exception occurs
     *                                        when any <code>DaoLayerTechnicalException</code> throw up
     */
    public abstract void saveNews(News news, Author author, List<Tag> tagList) throws ServiceLayerLogicException, ServiceLayerTechnicalException;
}
