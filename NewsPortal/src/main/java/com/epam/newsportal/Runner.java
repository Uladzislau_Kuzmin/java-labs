package com.epam.newsportal;


import com.epam.newsportal.domain.Author;
import com.epam.newsportal.exception.ServiceLayerLogicException;
import com.epam.newsportal.exception.ServiceLayerTechnicalException;
import com.epam.newsportal.service.NewsManagementService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Runner {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring.config.xml");
        NewsManagementService newsManagementService = (NewsManagementService)
                applicationContext.getBean("newsManagementService");

        Author author = new Author();
        author.setAuthorName("MyLove");
        try {
            newsManagementService.saveNews(null, author, null);
        } catch (ServiceLayerLogicException e) {
            e.printStackTrace();
        } catch (ServiceLayerTechnicalException e) {
            e.printStackTrace();
        }
    }
}
