--------------------------------------------------------
--  DDL for deleting Tables if exists
--------------------------------------------------------
BEGIN
  EXECUTE IMMEDIATE 'DROP TABLE COMMENTS';
  EXCEPTION WHEN OTHERS THEN IF SQLCODE != -942
THEN RAISE; END IF;
END;

BEGIN
  EXECUTE IMMEDIATE 'DROP TABLE NEWS_AUTHOR';
  EXCEPTION WHEN OTHERS THEN IF SQLCODE != -942
THEN RAISE; END IF;
END;

BEGIN
  EXECUTE IMMEDIATE 'DROP TABLE NEWS_TAG';
  EXCEPTION WHEN OTHERS THEN IF SQLCODE != -942
THEN RAISE; END IF;
END;

BEGIN
  EXECUTE IMMEDIATE 'DROP TABLE TAG';
  EXCEPTION WHEN OTHERS THEN IF SQLCODE != -942
THEN RAISE; END IF;
END;

BEGIN
  EXECUTE IMMEDIATE 'DROP TABLE AUTHOR';
  EXCEPTION WHEN OTHERS THEN IF SQLCODE != -942
THEN RAISE; END IF;
END;

BEGIN
  EXECUTE IMMEDIATE 'DROP TABLE NEWS';
  EXCEPTION WHEN OTHERS THEN IF SQLCODE != -942
THEN RAISE; END IF;
END;

BEGIN EXECUTE IMMEDIATE 'DROP SEQUENCE ONE_PLUS';
  EXCEPTION WHEN OTHERS THEN IF SQLCODE != -2289
THEN RAISE; END IF;
END;

--------------------------------------------------------
--  DDL for Table NEWS
--------------------------------------------------------

CREATE TABLE "HONEY"."NEWS"
(
  "NEWS_ID"           NUMBER(20, 0),
  "SHORT_TEXT"        NVARCHAR2(100)  NOT NULL,
  "FULL_TEXT"         NVARCHAR2(2000) NOT NULL,
  "TITLE"             NVARCHAR2(30)   NOT NULL,
  "CREATION_DATE"     TIMESTAMP(6)        NOT NULL,
  "MODIFICATION_DATE" DATE                NOT NULL,
  CONSTRAINT NEWS_PK PRIMARY KEY ("NEWS_ID")

);
--------------------------------------------------------
--  DDL for Table AUTHOR
--------------------------------------------------------

CREATE TABLE "HONEY"."AUTHOR"
(
  "AUTHOR_ID" NUMBER(20, 0),
  "NAME"      NVARCHAR2(30) NOT NULL,
  CONSTRAINT AUTHOR_PK PRIMARY KEY ("AUTHOR_ID")
);
--------------------------------------------------------
--  DDL for Table NEWS_AUTHOR
--------------------------------------------------------

CREATE TABLE "HONEY"."NEWS_AUTHOR"
(
  "NEWS_ID"   NUMBER(20, 0) NOT NULL,
  "AUTHOR_ID" NUMBER(20, 0) NOT NULL
);
--------------------------------------------------------
--  DDL for Table COMMENTS
--------------------------------------------------------

CREATE TABLE "HONEY"."COMMENTS"
(
  "COMMENT_ID"    NUMBER(20, 0),
  "COMMENT_TEXT"  NVARCHAR2(100) NOT NULL,
  "CREATION_DATE" TIMESTAMP(6)       NOT NULL,
  "NEWS_ID"       NUMBER(20, 0)      NOT NULL,
  CONSTRAINT COMMENTS_PK PRIMARY KEY ("COMMENT_ID")
);
--------------------------------------------------------
--  DDL for Table TAG
--------------------------------------------------------

CREATE TABLE "HONEY"."TAG"
(
  "TAG_ID"   NUMBER(20, 0),
  "TAG_NAME" NVARCHAR2(30) NOT NULL,
  CONSTRAINT TAG_PK PRIMARY KEY ("TAG_ID")
);
--------------------------------------------------------
--  DDL for Table NEWS_TAG
--------------------------------------------------------

CREATE TABLE "HONEY"."NEWS_TAG"
(
  "NEWS_ID" NUMBER(20, 0),
  "TAG_ID"  NUMBER(20, 0)
);

--------------------------------------------------------
--  Ref Constraints for Table NEWS_AUTHOR
--------------------------------------------------------

ALTER TABLE "HONEY"."NEWS_AUTHOR" ADD CONSTRAINT "NEWS_AUTHOR_AUTHOR" FOREIGN KEY ("AUTHOR_ID")
REFERENCES "HONEY"."AUTHOR" ("AUTHOR_ID") ON DELETE CASCADE;
ALTER TABLE "HONEY"."NEWS_AUTHOR" ADD CONSTRAINT "NEWS_AUTHOR_NEWS" FOREIGN KEY ("NEWS_ID")
REFERENCES "HONEY"."NEWS" ("NEWS_ID") ON DELETE CASCADE;
--------------------------------------------------------
--  Ref Constraints for Table COMMENTS
--------------------------------------------------------

ALTER TABLE "HONEY"."COMMENTS" ADD CONSTRAINT "COMMENTS_NEWS" FOREIGN KEY ("NEWS_ID")
REFERENCES "HONEY"."NEWS" ("NEWS_ID") ON DELETE CASCADE;
--------------------------------------------------------
--  Ref Constraints for Table NEWS_TAG
--------------------------------------------------------

ALTER TABLE "HONEY"."NEWS_TAG" ADD CONSTRAINT "NEWS_TAG_NEWS" FOREIGN KEY ("NEWS_ID")
REFERENCES "HONEY"."NEWS" ("NEWS_ID") ON DELETE CASCADE;
ALTER TABLE "HONEY"."NEWS_TAG" ADD CONSTRAINT "NEWS_TAG_TAG" FOREIGN KEY ("TAG_ID")
REFERENCES "HONEY"."TAG" ("TAG_ID") ON DELETE CASCADE;

--------------------------------------------------------

--  SEQUENCE for Data Base
--------------------------------------------------------
CREATE SEQUENCE one_plus
START WITH 1000
INCREMENT BY 1
NOCACHE
NOCYCLE;