package com.epam.newsmanagement.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
public class HomeController {
	
	
	@RequestMapping({"/", "home"})
	public String getHome(Model model){
		model.addAttribute("message", "I am client application!");
		return "home";
	}
}
