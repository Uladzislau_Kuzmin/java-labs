package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteriaVO;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.DaoException;

/**
 * The second level of the hierarchy Data Access layer interfaces. This
 * interface <code>NewsDao</code> describes the behavior of a particular dao
 * layer which working with instance of <code>News</code>.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsmanagement.dao.GenericDao
 * @see com.epam.newsmanagement.domain.News
 * @since 1.0
 */
public interface NewsDao extends GenericDao<News> {

	/**
	 * Sort News instances from table News from database by count of comments.
	 *
	 * @return sorted list of News instance
	 * @throws DaoLayerTechnicalException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 * @see com.epam.newsmanagement.domain.News
	 */
	public List<News> sortNewsByComments() throws DaoException;

	/**
	 * Searching News instance from table News in data base by sent Tag
	 * instance.
	 *
	 * @param tag
	 *            element of Tag instance to search for news
	 * @return list of News which was found by the Tag instance
	 * @throws DaoLayerTechnicalException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 * @see com.epam.newsmanagement.domain.Tag
	 */
	public List<News> searchNewsByTag(Tag tag) throws DaoException;

	/**
	 * Searching News instance from table News in data base by sent Author
	 * instance.
	 *
	 * @param author
	 *            element of Author instance to search for News list
	 * @return list of News which was find by the Author instance
	 * @throws DaoLayerTechnicalException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 * @see com.epam.newsmanagement.domain.Author
	 */
	public List<News> searchNewsByAuthor(Author author) throws DaoException;

	/**
	 * Binding News instance with the Tag instance in the table NewsTag in data
	 * base.
	 *
	 * @param news
	 *            News instance for binding with Tag instance
	 * @param tag
	 *            Tag instance for binding with News instance
	 * @throws DaoLayerTechnicalException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 * @see com.epam.newsportal.domain.Tag
	 * @see com.epam.newsportal.domain.News
	 */
	public void bindNewsTag(News news, Tag tag) throws DaoException;

	/**
	 * Bindings News instance with the Author instance in the table NewsAuthor
	 * in data base.
	 *
	 * @param news
	 *            News instance for binding with Author instance
	 * @param author
	 *            Author instance for binding with News instance
	 * @throws DaoLayerTechnicalException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 * @see com.epam.newsmanagement.domain.News
	 * @see com.epam.newsmanagement.domain.Author
	 */
	public void bindNewsAuthor(News news, Author author) throws DaoException;

	/**
	 * Bindings News instance with the list of Tag instances in the table
	 * NewsTag in data base.
	 *
	 * @param news
	 *            News instance for binding with Author instance
	 * @param author
	 *            Author instance for binding with News instance
	 * @throws DaoException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 * @see com.epam.newsmanagement.domain.News
	 * @see com.epam.newsmanagement.domain.Author
	 */
	public void bindNewsTagList(News news, List<Tag> tagList)
			throws DaoException;

	public List<News> getLimitNews(int pageNumber, int newsOnPage)
			throws DaoException;

	public List<News> getNewsBySearchCriteria(SearchCriteriaVO searchCriteriaVO)
			throws DaoException;

	public void deleteNewsList(List<Long> newsIdList) throws DaoException;

	public List<News> getFilterLimitNews(int lowBorder, int heightBorder,
			SearchCriteriaVO searchCriteriaVO) throws DaoException;

	public List<News> getLimitNewsByAuthor(int lowBorder, int heightBorder,
			SearchCriteriaVO searchCriteriaVO) throws DaoException;

	public Long readNextNews(Long currentNewsId) throws DaoException;

	public Long readPrevNews(Long currentNewsId) throws DaoException;

	public Long readNextFilerNews(Long currentNewsId,
			SearchCriteriaVO searchCriteriaVO) throws DaoException;

	public Long readPrevFilterNews(Long currentNewsId,
			SearchCriteriaVO searchCriteriaVO) throws DaoException;

}
