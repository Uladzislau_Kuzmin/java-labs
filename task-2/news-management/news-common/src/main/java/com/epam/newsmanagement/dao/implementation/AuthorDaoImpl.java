package com.epam.newsmanagement.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.utils.DataBaseUtil;

/**
 * Public class <code>AuthorDaoImpl</code> is an element of Data Access layer
 * and working with Author data base instance. This is a single ton realization
 * of <code>AuthorDao</code> interface and it performs all operations described
 * in this interface. Realized pattern Singleton.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsmanagement.dao.CommentDao
 * @see com.epam.newsmanagement.domain.Author
 * @since 1.0
 */
@Repository
public class AuthorDaoImpl implements AuthorDao {

	/**
	 * SQL query for create author
	 */
	private static final String CREATE_QUERY = "INSERT INTO AUTHOR (AUTHOR_ID, NAME) VALUES (AUTHOR_ID_SEQ.NEXTVAL, ?)";

	/**
	 * SQL query for read author by id
	 */
	private static final String READ_QUERY = "SELECT AUTHOR_ID, NAME FROM AUTHOR WHERE AUTHOR_ID = ?";

	/**
	 * SQL query for readAll author records
	 */
	private static final String READ_ALL_QUERY = "SELECT AUTHOR_ID, NAME FROM AUTHOR ORDER BY AUTHOR_ID";

	/**
	 * SQL query for update author
	 */
	private static final String UPDATE_QUERY = "UPDATE AUTHOR SET NAME = ? WHERE AUTHOR_ID = ?";

	/**
	 * SQL query for delete author
	 */
	private static final String DELETE_QUERY = "DELETE FROM AUTHOR WHERE AUTHOR_ID = ?";

	private static final String SEARCH_BY_NEWS_QUERY = "SELECT DISTINCT AUTHOR.AUTHOR_ID, AUTHOR.NAME FROM AUTHOR "
			+ "INNER JOIN NEWS_AUTHOR on AUTHOR.AUTHOR_ID = NEWS_AUTHOR.AUTHOR_ID WHERE NEWS_AUTHOR.NEWS_ID = ? "
			+ "ORDER BY AUTHOR_ID";
	/**
	 * object which used for connecting to the database
	 */
	@Autowired
	private DataSource dataSource;

	/**
	 * Override public method used to add record to the table Author.
	 *
	 * @param author
	 *            instance which will be add to the data base
	 * @return unique identifier inserted instance
	 * @throws DaoException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */

	public Long create(Author author) throws DaoException {
		String columnAuthorId = "AUTHOR_ID";
		Long authorId = null;

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		String generatedColumns[] = { columnAuthorId };
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(CREATE_QUERY,
					generatedColumns);
			preparedStatement.setString(1, author.getAuthorName());
			preparedStatement.executeUpdate();

			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				authorId = resultSet.getLong(1);
			}

		} catch (SQLException e) {
			throw new DaoException("SQL error in create 'author' operation.", e);
		} finally {
			DataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}

		return authorId;
	}

	/**
	 * Override public method used to read record from the table Author.
	 *
	 * @param authorId
	 *            unique identifier of instance which will be found
	 * @return find Author instance
	 * @throws DaoException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */

	public Author read(Long authorId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		Author author = null;

		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(READ_QUERY);
			preparedStatement.setLong(1, authorId);

			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				author = new Author();
				author.setAuthorId(resultSet.getLong(1));
				author.setAuthorName(resultSet.getString(2));
			}

			return author;

		} catch (SQLException e) {
			throw new DaoException("SQL error in read 'author' operation.", e);
		} finally {
			DataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	/**
	 * Override public method used to read all records from the table Author.
	 *
	 * @return list of find Author instances
	 * @throws DaoException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */

	public List<Author> readAll() throws DaoException {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;

		List<Author> authors = null;
		Author author = null;

		try {
			connection = dataSource.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(READ_ALL_QUERY);

			authors = new ArrayList<Author>();
			while (resultSet.next()) {
				author = new Author();

				author.setAuthorId(resultSet.getLong(1));
				author.setAuthorName(resultSet.getString(2));

				authors.add(author);
			}
			return authors;

		} catch (SQLException e) {
			throw new DaoException("SQL error in readAll 'authors' operation.",
					e);
		} finally {
			DataBaseUtil.closeResources(resultSet, statement, connection);
		}
	}

	/**
	 * Override public method used to update record in the table Author.
	 *
	 * @param author
	 *            instance which will be update in the data base
	 * @throws DaoException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */

	public void update(Author author) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(UPDATE_QUERY);
			preparedStatement.setString(1, author.getAuthorName());
			preparedStatement.setLong(2, author.getAuthorId());

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException("SQL error in update 'author' operation", e);
		} finally {
			DataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	/**
	 * Override public method used to delete record from the table Author.
	 *
	 * @param authorId
	 *            unique identifier of instance which will be delete
	 * @throws DaoException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public void delete(Long authorId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(DELETE_QUERY);

			preparedStatement.setLong(1, authorId);

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException("SQL error in delete 'author'operation.", e);
		} finally {
			DataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}
	
	/**
	 * Override method used to search Author record from the table Author by
	 * News instance.
	 *
	 * @param news
	 *            element of News instance to search for Tag list
	 * @return list of Tag which was find by the Author instance
	 * @throws DaoException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public Author searchAuthorByNews(News news) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataSource.getConnection();
			preparedStatement = connection
					.prepareStatement(SEARCH_BY_NEWS_QUERY);

			preparedStatement.setLong(1, news.getNewsId());

			resultSet = preparedStatement.executeQuery();

			Author author = new Author();
			while (resultSet.next()) {

				author.setAuthorId(resultSet.getLong(1));
				author.setAuthorName(resultSet.getString(2));
			}

			return author;

		} catch (SQLException e) {
			throw new DaoException(
					"SQL error in search 'author' by news operation.", e);
		} finally {
			DataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}
}