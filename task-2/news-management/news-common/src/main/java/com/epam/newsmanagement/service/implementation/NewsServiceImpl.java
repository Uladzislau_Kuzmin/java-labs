package com.epam.newsmanagement.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteriaVO;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;

/**
 * Public class <code>NewsServiceImpl</code> is an element of Service layer and
 * working with News data base instance. This is a realization of
 * <code>NewsService</code> interface and it performs all operations described
 * in this interface.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsmanagement.service.NewsService
 * @see com.epam.newsmanagement.domain.News
 * @since 1.0
 */
@Service
public class NewsServiceImpl implements NewsService {

	/**
	 * link to the interface part, providing access to basic operations
	 */
	@Autowired
	private NewsDao newsDao;

	/**
	 * Override public method used to add record to the table News, which calls
	 * create() method from NewsDao interface.
	 *
	 * @param news
	 *            instance which will be add to the data base
	 * @return unique identifier inserted instance
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 */

	public Long create(News news) throws ServiceException {
		Long newsId = null;

		if (news == null) {
			throw new ServiceException("News is empty.");
		}

		try {
			newsId = newsDao.create(news);

		} catch (DaoException e) {
			throw new ServiceException("Cannot create news.", e);
		}
		return newsId;
	}

	/**
	 * Override public method used to read record from the table News, which
	 * calls read() method from NewsDao interface.
	 *
	 * @param newsId
	 *            unique identifier of instance which will be found
	 * @return find News instance
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 */

	public News read(Long newsId) throws ServiceException {
		News news = null;

		try {
			news = newsDao.read(newsId);

		} catch (DaoException e) {
			throw new ServiceException("Cannot read news.", e);
		}

		return news;
	}

	/**
	 * Override public method used to read all records from the table News,
	 * which calls readAll() method from NewsDao interface.
	 *
	 * @return list of find News instances
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 */

	public List<News> readAll() throws ServiceException {
		List<News> newsList = null;

		try {
			newsList = newsDao.readAll();

		} catch (DaoException e) {
			throw new ServiceException("Cannot read all news.", e);
		}

		return newsList;
	}

	/**
	 * Override public method used to update record in the table News, which
	 * calls update() method from NewsDao interface.
	 *
	 * @param news
	 *            instance which will be update in the data base
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 */

	public void update(News news) throws ServiceException {
		if (news == null) {
			throw new ServiceException("News is epmty.");
		}

		try {
			newsDao.update(news);

		} catch (DaoException e) {
			throw new ServiceException("Cannot update news.", e);
		}
	}

	/**
	 * Override public method used to delete record from the table News, which
	 * calls delete() method from NewsDao interface.
	 *
	 * @param newsId
	 *            unique identifier of instance which will be delete
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 */

	public void delete(Long newsId) throws ServiceException {
		try {
			newsDao.delete(newsId);

		} catch (DaoException e) {
			throw new ServiceException("Cannot delete news.", e);
		}
	}

	/**
	 * Override public method used to get sorted, by comments count, list of
	 * records from the table News, which calls sortedNewsByComments() method
	 * from NewsDao interface.
	 *
	 * @return sorted list of News instance
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 */

	public List<News> sortNewsByComments() throws ServiceException {
		List<News> newsList = null;

		try {
			newsList = newsDao.sortNewsByComments();

		} catch (DaoException e) {
			throw new ServiceException("Cannot sort news by comments count.", e);
		}
		return newsList;
	}

	/**
	 * Override public method used to search list of News records from the table
	 * News by Tag instance, which calls searchNewsByTag() method from NewsDao
	 * interface.
	 *
	 * @param tag
	 *            element of Tag instance to search for news
	 * @return list of News which was found by the Tag instance
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 */

	public List<News> searchNewsByTag(Tag tag) throws ServiceException {
		List<News> newsList = null;

		if (tag == null) {
			throw new ServiceException("Tag is empty.");
		}

		try {
			newsList = newsDao.searchNewsByTag(tag);

		} catch (DaoException e) {
			throw new ServiceException("Cannot search news by tag.", e);
		}
		return newsList;
	}

	/**
	 * Override public method used to search list of News records from the table
	 * News by Author instance, which calls searchNewsByAuthor() method from
	 * NewsDao interface.
	 *
	 * @param author
	 *            element of Author instance to search for News list
	 * @return list of News which was find by the Author instance
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 */

	public List<News> searchNewsByAuthor(Author author) throws ServiceException {

		List<News> newsList = null;

		if (author == null) {
			throw new ServiceException("Author is empty.");
		}

		try {
			newsList = newsDao.searchNewsByAuthor(author);

		} catch (DaoException e) {
			throw new ServiceException("Cannot search news by author.", e);
		}

		return newsList;
	}

	/**
	 * Override public method used to bind Author instance with instance of
	 * News, and add same record to the NewsAuthor table in data base, calls
	 * createAuthorForNews() method from AuthorDao interface.
	 *
	 * @param news
	 *            News instance for binding with Author instance
	 * @param author
	 *            Author instance for binding with News instance
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */

	public void bindNewsAuthor(News news, Author author)
			throws ServiceException {
		if (news == null) {
			throw new ServiceException("News is empty.");

		} else if (author == null) {
			throw new ServiceException("Author is empty.");
		}

		try {
			newsDao.bindNewsAuthor(news, author);

		} catch (DaoException e) {
			throw new ServiceException("Cannot bind news with author.", e);
		}
	}

	/**
	 * Override public method used to bind Tag instance with instance of News,
	 * and add same record to the NewsTag table in data base, which calls
	 * createTagForNews() method from TagDao interface.
	 *
	 * @param news
	 *            News instance for binding with Tag instance
	 * @param tag
	 *            Tag instance for binding with News instance
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */

	public void bindNewsTag(News news, Tag tag) throws ServiceException {
		if (news == null) {
			throw new ServiceException("News is empty.");

		} else if (tag == null) {
			throw new ServiceException("Tag is empty.");
		}

		try {
			newsDao.bindNewsTag(news, tag);

		} catch (DaoException e) {
			throw new ServiceException("Cannot bind news with tag.", e);
		}
	}

	public void bindNewsTagList(News news, List<Tag> tag)
			throws ServiceException {
		if (news == null) {
			throw new ServiceException("News is empty.");
		} else if (tag == null) {
			throw new ServiceException("Tag list is empty.");
		}

		try {
			newsDao.bindNewsTagList(news, tag);
		} catch (DaoException e) {
			throw new ServiceException("Cannot bind news with list of tags.", e);
		}
	}

	public List<News> getLimitNews(int pageNumber, int newsOnPageCount)
			throws ServiceException {
		List<News> newsList = null;
		int lowBorder = (pageNumber - 1) * newsOnPageCount;
		int heightBorder = pageNumber * newsOnPageCount;

		try {
			newsList = newsDao.getLimitNews(lowBorder, heightBorder);
		} catch (DaoException e) {
			throw new ServiceException("Cannot read news by page number.", e);
		}
		return newsList;
	}

	public void deleteNewsList(List<Long> newsIdList) throws ServiceException {

		try {
			newsDao.deleteNewsList(newsIdList);
		} catch (DaoException e) {
			throw new ServiceException("Can not delete lsit of news.", e);
		}
	}

	public List<News> getNewsBySearchCriteria(SearchCriteriaVO searchCriteriaVO)
			throws ServiceException {
		List<News> newsList = null;

		if (searchCriteriaVO == null) {
			throw new ServiceException("SearchCriteriaVO is empty.");
		}

		try {
			newsList = newsDao.getNewsBySearchCriteria(searchCriteriaVO);
		} catch (DaoException e) {
			throw new ServiceException(
					"Cannot getting news by search criteria.", e);
		}

		return newsList;
	}

	public List<News> getFilterLimitNews(int pageNumber, int newsOnPageCount,
			SearchCriteriaVO searchCriteriaVO) throws ServiceException {
		List<News> newsList = null;

		int lowBorder = (pageNumber - 1) * newsOnPageCount;
		int heightBorder = pageNumber * newsOnPageCount;

		try {
			newsList = newsDao.getFilterLimitNews(lowBorder, heightBorder,
					searchCriteriaVO);
		} catch (DaoException e) {
			throw new ServiceException(
					"Cannot read filter news by page number.", e);
		}
		return newsList;
	}

	public List<News> getLimitNewsByAuthor(int pageNumber, int newsOnPageCount,
			SearchCriteriaVO searchCriteriaVO) throws ServiceException {
		List<News> newsList = null;
		int lowBorder = (pageNumber - 1) * newsOnPageCount;
		int heightBorder = pageNumber * newsOnPageCount;

		try {
			newsList = newsDao.getLimitNewsByAuthor(lowBorder, heightBorder,
					searchCriteriaVO);
		} catch (DaoException e) {
			throw new ServiceException("Cannot read news by page number.", e);
		}
		return newsList;
	}

	public Long readNextNews(Long currentNewsId) throws ServiceException {
		Long nextNewsId = null;

		if (currentNewsId == null) {
			throw new ServiceException("Current news id is empty.");
		}

		try {
			nextNewsId = newsDao.readNextNews(currentNewsId);

		} catch (DaoException e) {
			throw new ServiceException("Cannot read next news.", e);
		}

		return nextNewsId;
	}

	public Long readPrevNews(Long currentNewsId) throws ServiceException {
		Long prevNewsId = null;

		if (currentNewsId == null) {
			throw new ServiceException("Current news id is empty.");
		}

		try {
			prevNewsId = newsDao.readPrevNews(currentNewsId);

		} catch (DaoException e) {
			throw new ServiceException("Cannot read previous news.", e);
		}

		return prevNewsId;
	}

	public Long readNextFilerNews(Long currentNewsId,
			SearchCriteriaVO searchCriteriaVO) throws ServiceException {
		Long nextNewsId = null;

		if (currentNewsId == null) {
			throw new ServiceException("Current news id is empty.");
		} else if (searchCriteriaVO == null) {
			throw new ServiceException("Filter is empty");
		}

		try {
			nextNewsId = newsDao.readNextFilerNews(currentNewsId,
					searchCriteriaVO);
		} catch (DaoException e) {
			throw new ServiceException("Can not read next filter news.");
		}

		return nextNewsId;
	}

	public Long readPrevFilterNews(Long currentNewsId,
			SearchCriteriaVO searchCriteriaVO) throws ServiceException {
		Long nextNewsId = null;

		if (currentNewsId == null) {
			throw new ServiceException("Current news id is empty.");
		} else if (searchCriteriaVO == null) {
			throw new ServiceException("Filter is empty");
		}

		try {
			nextNewsId = newsDao.readPrevFilterNews(currentNewsId,
					searchCriteriaVO);
		} catch (DaoException e) {
			throw new ServiceException("Can not read next filter news.");
		}

		return nextNewsId;
	}

}