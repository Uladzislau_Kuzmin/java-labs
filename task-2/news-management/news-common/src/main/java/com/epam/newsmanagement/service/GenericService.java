package com.epam.newsmanagement.service;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;

import com.epam.newsmanagement.exception.ServiceException;

/**
 * Generic interface <code>GenericService</code> <br>
 * The top of the inheritance hierarchy of interfaces Service layer contains a
 * set of four standard operations for working with Data Access layer objects
 * common to all simple Service implementations.
 *
 * @param <Entity>
 *            the type of elements in this interface
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsmanagement.dao.GenericDao
 * @since 1.0
 */
public interface GenericService<Entity extends Serializable> {

	/**
	 * object to conduct logging
	 */
	public static final Logger logger = Logger.getLogger(GenericService.class
			.getName());

	/**
	 * Adding record in a database, using, using the create() method in
	 * GenericDao interface.
	 *
	 * @param entity
	 *            record to be added
	 * @return inserted entityID
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public Long create(Entity entity) throws ServiceException;

	/**
	 * Reading record in a database, using the read() method in GenericDao
	 * interface.
	 *
	 * @param entityID
	 *            unique identifier of a record to read
	 * @return entity record to be read
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public Entity read(Long entityID) throws ServiceException;

	/**
	 * Reading all records in a database, using the readAll() method of
	 * GenericDao interface.
	 *
	 * @return list of entity records to read
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public List<Entity> readAll() throws ServiceException;

	/**
	 * Updating record in a database, using the update() method of GenericDao
	 * interface.
	 *
	 * @param entity
	 *            record to updated
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public void update(Entity entity) throws ServiceException;

	/**
	 * Deleting record from a database, using the delete() method of GenericDao
	 * interface.
	 *
	 * @param entityID
	 *            unique identifier of a record to deleted
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public void delete(Long entityID) throws ServiceException;

}