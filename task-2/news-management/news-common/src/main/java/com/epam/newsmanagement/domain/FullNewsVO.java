package com.epam.newsmanagement.domain;

import java.util.List;

public class FullNewsVO {

	/**
	 * Current news instance
	 */
	private News news;

	/**
	 * Author instance for current news
	 */
	private Author author;

	/**
	 * List of tag instances for curent news
	 */
	private List<Tag> tagList;
	
	/**
	 * List of comment instances for curent news
	 */
	private List<Comment> commentList;

	public FullNewsVO() {
	}

	public FullNewsVO(News news, Author author, List<Tag> tagList, List<Comment> commentList) {
		this.news = news;
		this.author = author;
		this.tagList = tagList;
		this.commentList = commentList;
	}

	public News getNews() {
		return news;
	}

	public Author getAuthor() {
		return author;
	}

	public List<Tag> getTagList() {
		return tagList;
	}
	
	public List<Comment> getCommentList() {
		return commentList;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public void setTagList(List<Tag> tagList) {
		this.tagList = tagList;
	}
	
	public void setCommentList(List<Comment> commentList) {
		this.commentList = commentList;
	}

}
