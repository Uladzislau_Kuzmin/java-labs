package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * The second level of the hierarchy Service layer interfaces. This interface
 * <code>CommentService</code> describes the behavior of a particular service
 * layer which working with instance of <code>Comment</code> using Data Access
 * layer implementations of interface <code>CommentDao</code>.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsmanagement.service.GenericService
 * @see com.epam.newsmanagement.dao.CommentDao
 * @see com.epam.newsmanagement.domain.Comment
 * @since 1.0
 */
public interface CommentService extends GenericService<Comment> {
	
	/**
	 * Defining search operation from Comment table in data base by sent News
	 * instance, using the searchCommentsByNews() method in CommentDao interface.
	 *
	 * @param news
	 *            element of News instance to search for Comment list
	 * @return list of Comment which was find by the News instance
	 * @@throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public List<Comment> searchCommentsByNews(News news) throws ServiceException;
}