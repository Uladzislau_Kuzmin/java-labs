package com.epam.newsmanagement.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.epam.newsmanagement.exception.DaoException;

public class DataBaseUtil {

	/**
	 * Method that execute operation of closing all resources, after using.
	 *
	 * @param resultSet
	 *            ResultSet object to close after using
	 * @param statement
	 *            Statement object to close after using
	 * @param connection
	 *            Connection object to close after using
	 * @throws DaoLayerTechnicalException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public static void closeResources(ResultSet resultSet, Statement statement,
			Connection connection) throws DaoException {
		try {
			if (resultSet != null) {
				resultSet.close();
			}
			if (statement != null) {
				statement.close();
			}
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error in close resources operation.", e);
		}
	}
}
