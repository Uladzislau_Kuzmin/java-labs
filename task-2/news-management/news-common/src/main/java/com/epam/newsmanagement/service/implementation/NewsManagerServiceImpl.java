package com.epam.newsmanagement.service.implementation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.FullNewsVO;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteriaVO;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;

/**
 * Public class <code>NewsManagerServiceImpl<code/> is an
 * implementation of interface <code>NewsManagementService</code> and overrides
 * all methods describing transactional operations for which, you must use a few
 * simple Service layer interfaces.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsmanagement.service.NewsManagementService
 * @since 1.0
 */
@Service
public class NewsManagerServiceImpl implements NewsManagementService {

	/**
	 * author instance for using all service methods
	 */
	@Autowired
	private AuthorService authorService;

	/**
	 * news instance for using all service methods
	 */
	@Autowired
	private NewsService newsService;

	/**
	 * news instance for using all service methods
	 */
	@Autowired
	private TagService tagService;

	/**
	 * comment instance for using all service methods
	 */
	@Autowired
	private CommentService commentService;

	/**
	 * Binding news with author and list of tags in data base.
	 */
	@Transactional
	public void saveNewsVO(FullNewsVO fullNewsVO) throws ServiceException {

		Long newsId = newsService.create(fullNewsVO.getNews());
		fullNewsVO.getNews().setNewsId(newsId);

		newsService
				.bindNewsAuthor(fullNewsVO.getNews(), fullNewsVO.getAuthor());
		newsService.bindNewsTagList(fullNewsVO.getNews(),
				fullNewsVO.getTagList());
	}

	@Transactional
	public List<FullNewsVO> getLimitNewsVO(int pageNumber, int newsOnPageCount)
			throws ServiceException {
		List<News> newsList = null;
		List<FullNewsVO> fullNewsList = null;

		newsList = newsService.getLimitNews(pageNumber, newsOnPageCount);
		fullNewsList = setNewsVOList(newsList);

		return fullNewsList;
	}

	@Transactional
	public FullNewsVO getCurrentNewsVO(Long newsId) throws ServiceException {
		FullNewsVO fullNewsVO = null;
		News news = null;

		news = newsService.read(newsId);
		fullNewsVO = setNewsVODataByNews(news);
		return fullNewsVO;
	}

	private List<FullNewsVO> setNewsVOList(List<News> newsList)
			throws ServiceException {

		FullNewsVO fullNewsVO = null;
		List<FullNewsVO> fullNewsList = new ArrayList<FullNewsVO>();

		for (News news : newsList) {
			fullNewsVO = setNewsVODataByNews(news);
			fullNewsList.add(fullNewsVO);
		}

		return fullNewsList;
	}

	private FullNewsVO setNewsVODataByNews(News news) throws ServiceException {
		Author author = null;
		List<Tag> tagList = null;
		List<Comment> commentList = null;
		FullNewsVO fullNewsVO = null;

		author = authorService.searchAuthorByNews(news);
		tagList = tagService.searchTagsByNews(news);
		commentList = commentService.searchCommentsByNews(news);

		fullNewsVO = new FullNewsVO(news, author, tagList, commentList);

		return fullNewsVO;
	}

	/*
	 * @Transactional public List<FullNewsVO> getFilterLimitNewsVO(int
	 * pageNumber, int newsOnPageCount, Long authorId, List<Long> tagsIdList)
	 * throws ServiceException {
	 * 
	 * List<News> newsList = newsService.getFilterLimitNews(pageNumber,
	 * newsOnPageCount, authorId, tagsIdList);
	 * 
	 * List<FullNewsVO> fullNewsList = setNewsVOList(newsList); return
	 * fullNewsList; }
	 */

	public Long createAuthor(Author author) throws ServiceException {
		return authorService.create(author);
	}

	public Long createNews(News news) throws ServiceException {
		return newsService.create(news);
	}

	public Long createComment(Comment comment) throws ServiceException {
		return commentService.create(comment);
	}

	public Long createTag(Tag tag) throws ServiceException {
		return tagService.create(tag);
	}

	public Author readAuthor(Long authorId) throws ServiceException {
		return authorService.read(authorId);
	}

	public News readNews(Long newsId) throws ServiceException {
		return newsService.read(newsId);
	}

	public Comment readComment(Long commentId) throws ServiceException {
		return commentService.read(commentId);
	}

	public Tag readTag(Long tagId) throws ServiceException {
		return tagService.read(tagId);
	}

	public List<Author> readAllAuthors() throws ServiceException {
		return authorService.readAll();
	}

	public List<News> readAllNews() throws ServiceException {
		return newsService.readAll();
	}

	public List<Comment> readAllComments() throws ServiceException {
		return commentService.readAll();
	}

	public List<Tag> readAllTags() throws ServiceException {
		return tagService.readAll();
	}

	public void updateAuthor(Author author) throws ServiceException {
		authorService.update(author);

	}

	public void updateNews(News news) throws ServiceException {
		newsService.update(news);

	}

	public void updateComment(Comment comment) throws ServiceException {
		commentService.update(comment);

	}

	public void updateTag(Tag tag) throws ServiceException {
		tagService.update(tag);

	}

	public void deleteAuthor(Long authorId) throws ServiceException {
		authorService.delete(authorId);

	}

	public void deleteNews(Long newsId) throws ServiceException {
		newsService.delete(newsId);

	}

	public void deleteComment(Long commentId) throws ServiceException {
		commentService.delete(commentId);

	}

	public void deleteTag(Long tagId) throws ServiceException {
		tagService.delete(tagId);

	}

	public Author searchAuthorByNews(News news) throws ServiceException {
		return authorService.searchAuthorByNews(news);
	}

	public List<Comment> searchCommentsByNews(News news)
			throws ServiceException {
		return commentService.searchCommentsByNews(news);
	}

	public List<News> sortNewsByComments() throws ServiceException {
		return newsService.sortNewsByComments();
	}

	public List<News> searchNewsByTag(Tag tag) throws ServiceException {
		return newsService.searchNewsByTag(tag);
	}

	public List<News> searchNewsByAuthor(Author author) throws ServiceException {
		return newsService.searchNewsByAuthor(author);
	}

	public void bindNewsTag(News news, Tag tag) throws ServiceException {
		newsService.bindNewsTag(news, tag);

	}

	public void bindNewsAuthor(News news, Author author)
			throws ServiceException {
		newsService.bindNewsAuthor(news, author);

	}

	public void bindNewsTagList(News news, List<Tag> tagList)
			throws ServiceException {
		newsService.bindNewsTagList(news, tagList);

	}

	public void deleteNewsList(List<Long> newsIdList) throws ServiceException {
		newsService.deleteNewsList(newsIdList);

	}

	public Long readNextNews(Long currentNewsId) throws ServiceException {
		return newsService.readNextNews(currentNewsId);
	}

	public Long readPrevNews(Long currentNewsId) throws ServiceException {
		return newsService.readPrevNews(currentNewsId);
	}

	public Long readNextFilerNews(Long currentNewsId,
			SearchCriteriaVO searchCriteriaVO) throws ServiceException {
		return newsService.readNextFilerNews(currentNewsId, searchCriteriaVO);
	}

	public Long readPrevFilterNews(Long currentNewsId,
			SearchCriteriaVO searchCriteriaVO) throws ServiceException {
		return newsService.readPrevFilterNews(currentNewsId, searchCriteriaVO);
	}

	public List<Tag> searchTagsByAuthor(Author author) throws ServiceException {
		return tagService.searchTagsByAuthor(author);
	}

	public List<Tag> searchTagsByNews(News news) throws ServiceException {
		return tagService.searchTagsByNews(news);
	}

	public List<FullNewsVO> getFilterLimitNews(int pageNumber,
			int newsOnPageCount, SearchCriteriaVO searchCriteriaVO)
			throws ServiceException {
		List<News> newsList = null;
		List<FullNewsVO> fList = null;

		Author author = new Author();
		author.setAuthorId(searchCriteriaVO.getAuthorId());

		if (searchCriteriaVO.getTagsIdList() == null) {
			newsList = newsService.getLimitNewsByAuthor(pageNumber,
					newsOnPageCount, searchCriteriaVO);
		} else {
			newsList = newsService.getFilterLimitNews(pageNumber,
					newsOnPageCount, searchCriteriaVO);
		}

		fList = setNewsVOList(newsList);
		return fList;
	}

	public List<News> readAllNewsByFilter(SearchCriteriaVO searchCriteriaVO)
			throws ServiceException {

		List<News> newsList = null;

		Author author = new Author();
		author.setAuthorId(searchCriteriaVO.getAuthorId());

		if (searchCriteriaVO.getTagsIdList() == null) {
			newsList = newsService.searchNewsByAuthor(author);
		} else {
			newsList = newsService.getNewsBySearchCriteria(searchCriteriaVO);
		}
		return newsList;
	}

	public List<News> getLimitNewsByAuthor(int pageNumber, int newsOnPageCount,
			SearchCriteriaVO searchCriteriaVO) throws ServiceException {
		return newsService.getLimitNewsByAuthor(pageNumber, newsOnPageCount,
				searchCriteriaVO);
	}
}