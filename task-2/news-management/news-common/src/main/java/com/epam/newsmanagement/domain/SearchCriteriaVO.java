package com.epam.newsmanagement.domain;

import java.util.List;

public class SearchCriteriaVO {

	private Long authorId;
	private List<Long> tagsIdList;

	public SearchCriteriaVO() {
	}

	public SearchCriteriaVO(Long authorId, List<Long> tagsIdList) {
		this.authorId = authorId;
		this.tagsIdList = tagsIdList;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public List<Long> getTagsIdList() {
		return tagsIdList;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public void setTagsIdList(List<Long> tagsIdList) {
		this.tagsIdList = tagsIdList;
	}
	
}
