package com.epam.newsmanagement.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;

/**
 * Public class <code>CommentServiceImpl</code> is an element of Service layer
 * and working with Comment data base instance. This is a realization of
 * <code>CommentService</code> interface and it performs all operations
 * described in this interface.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsmanagement.service.CommentService
 * @see com.epam.newsmanagement.domain.Comment
 * @since 1.0
 */
@Service
public class CommentServiceImpl implements CommentService {

	/**
	 * link to the interface part, providing access to basic operations
	 */
	@Autowired
	private CommentDao commentDao;

	/**
	 * Override method used to add record to the table Comment, which calls
	 * create() method from CommentDao interface.
	 *
	 * @param comment
	 *            instance which will be add to the data base
	 * @return unique identifier inserted instance
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */

	public Long create(Comment comment) throws ServiceException {
		Long commentId = null;

		if (comment == null) {
			throw new ServiceException("Comment is empty.");
		}

		try {
			commentId = commentDao.create(comment);

		} catch (DaoException e) {
			throw new ServiceException("Cannot create comment.", e);
		}
		return commentId;
	}

	/**
	 * Override method used to read record from the table Comment, which calls
	 * read() method from CommentDao interface.
	 *
	 * @param commentId
	 *            unique identifier of instance which will be found
	 * @return find Comment instance
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */

	public Comment read(Long commentId) throws ServiceException {
		Comment comment = null;

		try {
			comment = commentDao.read(commentId);

		} catch (DaoException e) {
			throw new ServiceException("Cannot read comment.", e);
		}

		return comment;
	}

	/**
	 * Override method used to read all records from the table Comment, which
	 * calls readAll() method from CommentDao interface.
	 *
	 * @return list of find Comment instances
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */

	public List<Comment> readAll() throws ServiceException {
		List<Comment> commentList = null;

		try {
			commentList = commentDao.readAll();

		} catch (DaoException e) {
			throw new ServiceException("Cannot read all comments.", e);
		}

		return commentList;
	}

	/**
	 * Override method used to update record in the table Comment, which calls
	 * update() method from CommentDao interface.
	 *
	 * @param comment
	 *            instance which will be update in the data base
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */

	public void update(Comment comment) throws ServiceException {
		if (comment == null) {
			throw new ServiceException("Comment is empty.");
		}

		try {
			commentDao.update(comment);

		} catch (DaoException e) {
			throw new ServiceException("Cannot update comment.", e);
		}
	}

	/**
	 * Override method used to delete record from the table Comment, which calls
	 * delete() method from CommentDao interface.
	 *
	 * @param commentId
	 *            unique identifier of instance which will be delete
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */

	public void delete(Long commentId) throws ServiceException {
		try {
			commentDao.delete(commentId);

		} catch (DaoException e) {
			throw new ServiceException("Cannot delete comment.", e);
		}
	}
	
	
	public List<Comment> searchCommentsByNews(News news)
			throws ServiceException {
		List<Comment> commentList = null;
		
		if(news == null) {
			throw new ServiceException("News is empty.");
		}
		
		try {
			commentList = commentDao.searchCommentsByNews(news);
		} catch (DaoException e) {
			throw new ServiceException("Cannot find comment.", e);
		}
		
		return commentList;
	}
}