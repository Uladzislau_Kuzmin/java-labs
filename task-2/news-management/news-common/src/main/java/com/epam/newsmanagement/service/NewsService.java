package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteriaVO;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * The second level of the hierarchy Service layer interfaces. This interface
 * <code>NewsService</code> describes the behavior of a particular service layer
 * which working with instance of <code>News</code> using Data Access layer
 * implementations of interface <code>NewsDao</code>.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsmanagement.service.GenericService
 * @see com.epam.newsmanagement.dao.NewsDao
 * @see com.epam.newsmanagement.domain.News
 * @since 1.0
 */
public interface NewsService extends GenericService<News> {

	/**
	 * Sort news instances of News table from database by count of comments,
	 * using the sortedNewsByComments() method in NewsDao interface.
	 *
	 * @return sorted list of News instances
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public List<News> sortNewsByComments() throws ServiceException;

	/**
	 * Search list News instances from News table in data base by sent Tag
	 * instance, using the searchNewsByTag() method in NewsDao interface.
	 *
	 * @param tag
	 *            element of Tag instance to search for news
	 * @return list of News which was found by the Tag instance
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public List<News> searchNewsByTag(Tag tag) throws ServiceException;

	/**
	 * Search list of News instances from News table in data base by sent Author
	 * instance, using the searchNewsByAuthor() method in NewsDao interface.
	 *
	 * @param author
	 *            element of Author instance to search for News list
	 * @return list of News which was find by the Author instance
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public List<News> searchNewsByAuthor(Author author) throws ServiceException;

	/**
	 * Binding the News instance with the Author instance in the table
	 * NewsAuthor in data base, using the bindingNewsAuthor() method in NewsDao
	 * interface.
	 *
	 * @param news
	 *            News instance for binding with Author instance
	 * @param author
	 *            Author instance for binding with News instance
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public void bindNewsAuthor(News news, Author author)
			throws ServiceException;

	/**
	 * Binding the News instance with the Tag instance in the table NewsTag in
	 * data base, using the bindingNewsTag() method in NewsDao interface.
	 *
	 * @param news
	 *            News instance for binding with Tag instance
	 * @param tag
	 *            Tag instance for binding with News instance
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public void bindNewsTag(News news, Tag tag) throws ServiceException;

	/**
	 * Binding the News instance with the list of Tag instances in the table
	 * NewsTag in data base, using the createTagForNews() method in NewsDao
	 * interface.
	 *
	 * @param news
	 *            News instance for binding with Tag instance
	 * @param tag
	 *            Tag instance for binding with News instance
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public void bindNewsTagList(News news, List<Tag> tagList)
			throws ServiceException;

	public List<News> getLimitNews(int pageNumber, int newsOnPageCount)
			throws ServiceException;

	public List<News> getNewsBySearchCriteria(SearchCriteriaVO searchCriteriaVO)
			throws ServiceException;

	public List<News> getFilterLimitNews(int pageNumber, int newsOnPageCount,
			SearchCriteriaVO searchCriteriaVO) throws ServiceException;

	public void deleteNewsList(List<Long> newsIdList) throws ServiceException;

	public List<News> getLimitNewsByAuthor(int pageNumber, int newsOnPageCount,
			SearchCriteriaVO searchCriteriaVO) throws ServiceException;

	public Long readNextNews(Long currentNewsId) throws ServiceException;

	public Long readPrevNews(Long currentNewsId) throws ServiceException;

	public Long readNextFilerNews(Long currentNewsId,
			SearchCriteriaVO searchCriteriaVO) throws ServiceException;

	public Long readPrevFilterNews(Long currentNewsId,
			SearchCriteriaVO searchCriteriaVO) throws ServiceException;

}