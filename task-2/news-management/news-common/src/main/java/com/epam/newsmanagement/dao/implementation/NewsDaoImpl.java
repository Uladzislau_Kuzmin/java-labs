package com.epam.newsmanagement.dao.implementation;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteriaVO;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.utils.DataBaseUtil;

/**
 * Public class <code>NewsDaoImpl</code> is an element of Data Access layer and
 * working with News data base instance. This is a single ton realization of
 * <code>NewsDao</code> interface and it performs all operations described in
 * this interface. Realized pattern Singleton.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsmanagement.dao.NewsDao
 * @see com.epam.newsmanagement.domain.News
 * @since 1.0
 */
@Repository
public class NewsDaoImpl implements NewsDao {

	/**
	 * SQL query for create news
	 */
	private static final String CREATE_QUERY = "INSERT INTO NEWS (NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE) "
			+ "VALUES (NEWS_ID_SEQ.NEXTVAL, ?, ?, ?, ?, ?)";

	/**
	 * SQL query for read news by id
	 */
	private static final String READ_QUERY = "SELECT NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE FROM NEWS "
			+ "WHERE NEWS_ID = ?";

	/**
	 * SQL query for readAll news records
	 */
	private static final String READ_ALL_QUERY = "SELECT NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE FROM NEWS "
			+ "ORDER BY NEWS_ID";

	/**
	 * SQL query for update news
	 */
	private static final String UPDATE_QUERY = "UPDATE NEWS SET SHORT_TEXT = ?, FULL_TEXT = ?, TITLE = ?, CREATION_DATE = ?, MODIFICATION_DATE = ? "
			+ "WHERE NEWS_ID = ?";

	/**
	 * SQL query for delete news
	 */
	private static final String DELETE_QUERY = "DELETE FROM NEWS WHERE NEWS_ID = ?";

	/**
	 * SQL query for search news by author
	 */
	private static final String SEARCH_BY_AUTHOR_QUERY = "SELECT DISTINCT N.NEWS_ID, N.SHORT_TEXT, N.FULL_TEXT, N.TITLE, N.CREATION_DATE, N.MODIFICATION_DATE, C.CNT FROM NEWS N LEFT JOIN (SELECT NEWS_ID, COUNT(*) CNT FROM COMMENTS GROUP BY NEWS_ID) C ON N.NEWS_ID = C.NEWS_ID INNER JOIN NEWS_AUTHOR on N.NEWS_ID = NEWS_AUTHOR.NEWS_ID  where news_author.author_id = ? ORDER BY C.CNT DESC NULLS LAST";

	/**
	 * SQL query for search news by tag
	 */
	private static final String SEARCH_BY_TAG_QUERY = "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, "
			+ "NEWS.MODIFICATION_DATE FROM NEWS INNER JOIN NEWS_TAG ON NEWS.NEWS_ID = NEWS_TAG.NEWS_ID WHERE NEWS_TAG.TAG_ID = ?";

	/**
	 * SQL query for sort news by comments count
	 */
	private static final String SORT_BY_COMMENTS_COUNT_QUERY = "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, "
			+ "NEWS.MODIFICATION_DATE FROM NEWS FULL OUTER JOIN COMMENTS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID GROUP BY (NEWS.NEWS_ID, NEWS.SHORT_TEXT, "
			+ "NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE) ORDER BY COUNT(COMMENTS.COMMENT_ID)DESC";

	/**
	 * SQL query for bind news with author
	 */
	private static final String BIND_NEWS_AUTHOR_QUERY = "INSERT INTO NEWS_AUTHOR (NEWS_AUTHOR.NEWS_ID, NEWS_AUTHOR.AUTHOR_ID) VALUES(?, ?)";

	/**
	 * SQL query for bind news with tag
	 */
	private static final String BIND_NEWS_TAG_QUERY = "INSERT INTO NEWS_TAG (NEWS_TAG.NEWS_ID, NEWS_TAG.TAG_ID) VALUES(?, ?)";

	/**
	 * SQL query for read limited news count, sorted by comments
	 */
	private static final String READ_NEWS_BY_PAGE_QUERY = "WITH NUMBERED_QUERY AS ( SELECT Q.*, ROWNUM AS IDX FROM (SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS LEFT JOIN COMMENTS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID GROUP BY (NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE) ORDER BY COUNT(COMMENTS.COMMENT_ID) DESC, NEWS.NEWS_ID NULLS LAST)Q) SELECT * FROM NUMBERED_QUERY Q WHERE IDX > ? AND IDX <= ?";

	private static final String READ_NEXT_NEWS_QUERY = "WITH NUMBERED_QUERY AS ( SELECT Q.*, ROWNUM AS IDX FROM (SELECT NEWS.NEWS_ID FROM NEWS LEFT JOIN COMMENTS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID GROUP BY (NEWS.NEWS_ID) ORDER BY COUNT(COMMENTS.COMMENT_ID) DESC, NEWS.NEWS_ID NULLS LAST)Q) SELECT * FROM NUMBERED_QUERY Q, (SELECT MIN(IDX) AS IDX FROM NUMBERED_QUERY WHERE NEWS_ID = ?)NI WHERE Q.IDX = NI.IDX + 1";

	private static final String READ_PREV_NEWS_QUERY = "WITH NUMBERED_QUERY AS ( SELECT Q.*, ROWNUM AS IDX FROM (SELECT NEWS.NEWS_ID FROM NEWS LEFT JOIN COMMENTS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID GROUP BY (NEWS.NEWS_ID) ORDER BY COUNT(COMMENTS.COMMENT_ID) DESC, NEWS.NEWS_ID NULLS LAST)Q) SELECT * FROM NUMBERED_QUERY Q, (SELECT MIN(IDX) AS IDX FROM NUMBERED_QUERY WHERE NEWS_ID = ?)NI WHERE Q.IDX = NI.IDX - 1";

	private static final String READ_LIMIT_NEWS_BY_AUTHOR_QUERY = "WITH NUMBERED_QUERY AS (SELECT Q.*, ROWNUM AS IDX FROM (SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE, COUNT(COMMENTS.COMMENT_ID) FROM NEWS LEFT JOIN COMMENTS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID INNER JOIN NEWS_AUTHOR ON NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID WHERE NEWS_AUTHOR.AUTHOR_ID= ? GROUP BY (NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE) ORDER BY COUNT(COMMENTS.COMMENT_ID) DESC, NEWS.NEWS_ID NULLS LAST)Q) SELECT * FROM NUMBERED_QUERY Q WHERE IDX > ? AND IDX <= ?";

	/**
	 * object which used for connecting to the database
	 */
	@Autowired
	private DataSource dataSource;

	/**
	 * Override method used to add record to the table News.
	 *
	 * @param news
	 *            instance which will be add to the data base
	 * @return unique identifier inserted instance
	 * @throws DaoException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */

	public Long create(News news) throws DaoException {
		String columnNewsId = "NEWS_ID";
		Long newsId = null;

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		String generatedColumns[] = { columnNewsId };
		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(CREATE_QUERY,
					generatedColumns);

			preparedStatement.setString(1, news.getShortText());
			preparedStatement.setString(2, news.getFullText());
			preparedStatement.setString(3, news.getTitle());
			preparedStatement.setTimestamp(4, new Timestamp(news
					.getCreationDate().getTime()));
			preparedStatement.setDate(5, new Date(news.getModificationDate()
					.getTime()));

			preparedStatement.executeUpdate();

			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				newsId = resultSet.getLong(1);
			}

		} catch (SQLException e) {
			throw new DaoException("SQL error in create 'news' operation.", e);
		} finally {
			DataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
		return newsId;

	}

	/**
	 * Override method used to read record from the table News.
	 *
	 * @param newsId
	 *            unique identifier of instance which will be found
	 * @return find News instance
	 * @throws DaoException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */

	public News read(Long newsId) throws DaoException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		News news = null;

		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(READ_QUERY);

			preparedStatement.setLong(1, newsId);

			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				news = new News();
				news.setNewsId(resultSet.getLong(1));
				news.setShortText(resultSet.getString(2));
				news.setFullText(resultSet.getString(3));
				news.setTitle(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(5));
				news.setModificationDate(resultSet.getDate(6));
			}

			return news;

		} catch (SQLException e) {
			throw new DaoException("SQL error in read 'news' operation.", e);
		} finally {
			DataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	/**
	 * Override method used to read all records from the table News.
	 *
	 * @return list of find News instances
	 * @throws DaoException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */

	public List<News> readAll() throws DaoException {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;

		List<News> newsList = null;
		News news = null;

		try {
			connection = dataSource.getConnection();
			statement = connection.createStatement();

			resultSet = statement.executeQuery(READ_ALL_QUERY);

			newsList = new ArrayList<News>();
			while (resultSet.next()) {
				news = new News();

				news.setNewsId(resultSet.getLong(1));
				news.setShortText(resultSet.getString(2));
				news.setFullText(resultSet.getString(3));
				news.setTitle(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(5));
				news.setModificationDate(resultSet.getDate(6));

				newsList.add(news);
			}

			return newsList;

		} catch (SQLException e) {
			throw new DaoException("SQL error in readAll 'news' operation.", e);
		} finally {
			DataBaseUtil.closeResources(resultSet, statement, connection);
		}
	}

	/**
	 * Override method used to update record in the table News.
	 *
	 * @param news
	 *            instance which will be update in the data base
	 * @throws DaoException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */

	public void update(News news) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(UPDATE_QUERY);

			preparedStatement.setString(1, news.getShortText());
			preparedStatement.setString(2, news.getFullText());
			preparedStatement.setString(3, news.getTitle());
			preparedStatement.setTimestamp(4, new Timestamp(news
					.getCreationDate().getTime()));
			preparedStatement.setDate(5, new Date(news.getModificationDate()
					.getTime()));
			preparedStatement.setLong(6, news.getNewsId());

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException("SQL error in update 'news' operation.", e);
		} finally {
			DataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	/**
	 * Override method used to delete record from the table News.
	 *
	 * @param newsId
	 *            unique identifier of instance which will be delete
	 * @throws DaoException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */

	public void delete(Long newsId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(DELETE_QUERY);

			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException("SQL error in delete 'news' operation.", e);
		} finally {
			DataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	/**
	 * Override method used to get sorted, by comments count, list of records
	 * from the table News.
	 *
	 * @return sorted list of News instance
	 * @throws DaoException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */

	public List<News> sortNewsByComments() throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		List<News> newsList = null;
		News news = null;

		try {
			connection = dataSource.getConnection();
			preparedStatement = connection
					.prepareStatement(SORT_BY_COMMENTS_COUNT_QUERY);

			resultSet = preparedStatement.executeQuery();

			newsList = new ArrayList<News>();
			while (resultSet.next()) {
				news = new News();

				news.setNewsId(resultSet.getLong(1));
				news.setShortText(resultSet.getString(2));
				news.setFullText(resultSet.getString(3));
				news.setTitle(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(5));
				news.setModificationDate(resultSet.getDate(6));

				newsList.add(news);
			}

			return newsList;

		} catch (SQLException e) {
			throw new DaoException("SQL error in sort 'news' operation.", e);
		} finally {
			DataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	/**
	 * Override method used to search list of News records from the table News
	 * by Tag instance.
	 *
	 * @param tag
	 *            element of Tag instance to search for news
	 * @return list of News which was found by the Tag instance
	 * @throws DaoException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */

	public List<News> searchNewsByTag(Tag tag) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		List<News> newsList = null;
		News news = null;

		try {
			connection = dataSource.getConnection();
			preparedStatement = connection
					.prepareStatement(SEARCH_BY_TAG_QUERY);

			preparedStatement.setLong(1, tag.getTagId());

			resultSet = preparedStatement.executeQuery();

			newsList = new ArrayList<News>();
			while (resultSet.next()) {
				news = new News();

				news.setNewsId(resultSet.getLong(1));
				news.setShortText(resultSet.getString(2));
				news.setFullText(resultSet.getString(3));
				news.setTitle(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(5));
				news.setModificationDate(resultSet.getDate(6));

				newsList.add(news);
			}

			return newsList;

		} catch (SQLException e) {
			throw new DaoException(
					"SQL error in search 'news' by tag operation.", e);
		} finally {
			DataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	/**
	 * Override method used to search list of News records from the table News
	 * by Author instance.
	 *
	 * @param author
	 *            element of Author instance to search for News list
	 * @return list of News which was find by the Author instance
	 * @throws DaoException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */

	public List<News> searchNewsByAuthor(Author author) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		List<News> newsList = null;
		News news = null;

		try {
			connection = dataSource.getConnection();
			preparedStatement = connection
					.prepareStatement(SEARCH_BY_AUTHOR_QUERY);

			preparedStatement.setLong(1, author.getAuthorId());

			resultSet = preparedStatement.executeQuery();

			newsList = new ArrayList<News>();
			while (resultSet.next()) {
				news = new News();

				news.setNewsId(resultSet.getLong(1));
				news.setShortText(resultSet.getString(2));
				news.setFullText(resultSet.getString(3));
				news.setTitle(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(5));
				news.setModificationDate(resultSet.getDate(6));

				newsList.add(news);
			}

			return newsList;

		} catch (SQLException e) {
			throw new DaoException(
					"SQL error in search 'news' by author operation.", e);
		} finally {
			DataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	/**
	 * Override method used to bind Author instance with instance of News, and
	 * add same record to the NewsAuthor table in data base.
	 *
	 * @param news
	 *            News instance for binding with Author instance
	 * @param author
	 *            Author instance for binding with News instance
	 * @throws DaoException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */

	public void bindNewsAuthor(News news, Author author) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataSource.getConnection();
			preparedStatement = connection
					.prepareStatement(BIND_NEWS_AUTHOR_QUERY);

			preparedStatement.setLong(1, news.getNewsId());
			preparedStatement.setLong(2, author.getAuthorId());

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException(
					"SQL error in binding 'news' with author operation.", e);
		} finally {
			DataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	/**
	 * Override method used to bind Tag instance with instance of News, and add
	 * same record to the NewsTag table in data base.
	 *
	 * @param news
	 *            News instance for binding with Tag instance
	 * @param tag
	 *            Tag instance for binding with News instance
	 * @throws DaoException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public void bindNewsTag(News news, Tag tag) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataSource.getConnection();
			preparedStatement = connection
					.prepareStatement(BIND_NEWS_TAG_QUERY);

			preparedStatement.setLong(1, news.getNewsId());
			preparedStatement.setLong(2, tag.getTagId());

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException(
					"SQL error in binding 'news' with tag operation.", e);
		} finally {
			DataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	/**
	 * Override method used to bind list of Tag instances with instance of News,
	 * and add same record to the NewsTag table in data base.
	 *
	 * @param news
	 *            News instance for binding with Tag instance
	 * @param tagList
	 *            List of tag instance for binding with News instance
	 * @throws DaoException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 */
	public void bindNewsTagList(News news, List<Tag> tagsList)
			throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			connection = dataSource.getConnection();
			for (Tag tag : tagsList) {
				preparedStatement = connection
						.prepareStatement(BIND_NEWS_TAG_QUERY);
				preparedStatement.setLong(1, news.getNewsId());
				preparedStatement.setLong(2, tag.getTagId());
				preparedStatement.executeUpdate();
			}
		} catch (SQLException e) {
			throw new DaoException(
					"SQL error in binding 'news' with list tag operation.", e);
		} finally {
			DataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}

	}

	public List<News> getLimitNews(int lowBorder, int heightBorder)
			throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		List<News> newsList = new ArrayList<News>();

		try {
			connection = dataSource.getConnection();
			preparedStatement = connection
					.prepareStatement(READ_NEWS_BY_PAGE_QUERY);
			preparedStatement.setLong(1, lowBorder);
			preparedStatement.setLong(2, heightBorder);
			resultSet = preparedStatement.executeQuery();

			News news = null;
			while (resultSet.next()) {
				news = new News();

				news.setNewsId(resultSet.getLong(1));
				news.setShortText(resultSet.getString(2));
				news.setFullText(resultSet.getString(3));
				news.setTitle(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(5));
				news.setModificationDate(resultSet.getDate(6));

				newsList.add(news);
			}
			return newsList;

		} catch (SQLException e) {
			throw new DaoException(
					"SQL error in binding 'news' with list tag operation.", e);
		} finally {
			DataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	public void deleteNewsList(List<Long> newsIdList) throws DaoException {
		int startPosition = 1;
		String deleteNewsListQuey = concatDeleteNewsListQuery(newsIdList.size());

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Long newsId = null;

		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(deleteNewsListQuey);
			for (int i = 0; i < newsIdList.size(); i++) {
				newsId = newsIdList.get(i);
				preparedStatement.setLong(startPosition + i, newsId);
			}
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException(
					"SQL error in delete list of 'news' operation.", e);
		} finally {
			DataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}

	}

	private String concatDeleteNewsListQuery(int newsListSize) {
		StringBuilder deleteNewsListQuey = new StringBuilder(
				"DELETE FROM NEWS WHERE NEWS_ID IN (");

		for (int i = 0; i < newsListSize; i++) {
			if (i != newsListSize - 1) {
				deleteNewsListQuey.append("?,");
			} else {
				deleteNewsListQuey.append("?)");
			}
		}
		return deleteNewsListQuey.toString();
	}

	public List<News> getNewsBySearchCriteria(SearchCriteriaVO searchCriteriaVO)
			throws DaoException {

		int startSetPosition = 2;
		int tagListSize = searchCriteriaVO.getTagsIdList().size();
		String searchNewsQuery = concatSeacrhNewsQuery(tagListSize);

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		List<News> newsList = new ArrayList<News>();

		try {
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(searchNewsQuery);
			preparedStatement.setLong(1, searchCriteriaVO.getAuthorId());
			for (int i = 0; i < tagListSize; i++) {
				preparedStatement.setLong(i + startSetPosition,
						searchCriteriaVO.getTagsIdList().get(i));
			}

			resultSet = preparedStatement.executeQuery();

			News news = null;
			while (resultSet.next()) {
				news = new News();

				news.setNewsId(resultSet.getLong(1));
				news.setShortText(resultSet.getString(2));
				news.setFullText(resultSet.getString(3));
				news.setTitle(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(5));
				news.setModificationDate(resultSet.getDate(6));

				newsList.add(news);
			}
			return newsList;

		} catch (SQLException e) {
			throw new DaoException(
					"SQL error in getting 'news' by searchCriteria operation.",
					e);
		} finally {
			DataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	private String concatSeacrhNewsQuery(int tagListSize) {
		StringBuilder searchNewsQuery = new StringBuilder(
				"SELECT DISTINCT N.NEWS_ID, N.SHORT_TEXT, N.FULL_TEXT, N.TITLE, N.CREATION_DATE, N.MODIFICATION_DATE, C.CNT FROM NEWS N LEFT JOIN (SELECT NEWS_ID, COUNT(*) CNT FROM COMMENTS GROUP BY NEWS_ID) C ON N.NEWS_ID = C.NEWS_ID INNER JOIN NEWS_AUTHOR on N.NEWS_ID = NEWS_AUTHOR.NEWS_ID  INNER JOIN NEWS_TAG on N.NEWS_ID = NEWS_TAG.NEWS_ID  where news_author.author_id = ? and news_tag.TAG_ID in (");

		for (int i = 0; i < tagListSize; i++) {
			if (i != tagListSize - 1) {
				searchNewsQuery.append("?,");
			} else {
				searchNewsQuery.append("?");
			}
		}
		searchNewsQuery.append(")ORDER BY C.CNT DESC NULLS LAST");
		return searchNewsQuery.toString();
	}

	public Long readNextNews(Long currentNewsId) throws DaoException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		Long nextNewsId = null;

		try {
			connection = dataSource.getConnection();
			preparedStatement = connection
					.prepareStatement(READ_NEXT_NEWS_QUERY);
			preparedStatement.setLong(1, currentNewsId);

			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				nextNewsId = resultSet.getLong(1);
			}

			return nextNewsId;

		} catch (SQLException e) {
			throw new DaoException("SQL error in getting 'next' news.", e);
		} finally {
			DataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	public Long readPrevNews(Long currentNewsId) throws DaoException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		Long prevNewsId = null;

		try {
			connection = dataSource.getConnection();
			preparedStatement = connection
					.prepareStatement(READ_PREV_NEWS_QUERY);
			preparedStatement.setLong(1, currentNewsId);

			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				prevNewsId = resultSet.getLong(1);
			}

			return prevNewsId;

		} catch (SQLException e) {
			throw new DaoException("SQL error in getting 'previoues' news.", e);
		} finally {
			DataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	public List<News> getFilterLimitNews(int lowBorder, int heightBorder,
			SearchCriteriaVO searchCriteriaVO) throws DaoException {
		List<Long> tagList = searchCriteriaVO.getTagsIdList();

		final String getFilterLimitNewsQuery = concatFilterLimitNewsQuery(tagList);

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Long tagId = null;
		int tagIdStartPosition = 2;

		List<News> newsList = new ArrayList<News>();

		try {
			connection = dataSource.getConnection();
			preparedStatement = connection
					.prepareStatement(getFilterLimitNewsQuery);
			preparedStatement.setLong(1, searchCriteriaVO.getAuthorId());
			for (int i = 0; i < tagList.size(); i++) {
				tagId = tagList.get(i);
				preparedStatement.setLong(i + tagIdStartPosition, tagId);
			}
			preparedStatement.setLong(tagList.size() + 2, lowBorder);
			preparedStatement.setLong(tagList.size() + 3, heightBorder);
			resultSet = preparedStatement.executeQuery();

			News news = null;
			while (resultSet.next()) {
				news = new News();

				news.setNewsId(resultSet.getLong(1));
				news.setShortText(resultSet.getString(2));
				news.setFullText(resultSet.getString(3));
				news.setTitle(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(5));
				news.setModificationDate(resultSet.getDate(6));

				newsList.add(news);
			}

			return newsList;

		} catch (SQLException e) {
			throw new DaoException(
					"SQL error in binding 'news' with list tag operation.", e);
		} finally {
			DataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	private String concatFilterLimitNewsQuery(List<Long> tagsIdList) {
		StringBuilder readFilterLimitNewsQuery = new StringBuilder(
				"WITH NUMBERED_QUERY AS (SELECT Q.*, ROWNUM AS IDX FROM (SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE, COUNT(COMMENTS.COMMENT_ID) FROM NEWS LEFT JOIN COMMENTS ON NEWS.NEWS_ID = COMMENTS.NEWS_ID INNER JOIN NEWS_AUTHOR ON NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID INNER JOIN NEWS_TAG ON NEWS.NEWS_ID = NEWS_TAG.NEWS_ID WHERE NEWS_AUTHOR.AUTHOR_ID= ? AND NEWS_TAG.TAG_ID IN(");
		for (int i = 0; i < tagsIdList.size(); i++) {
			if (i != tagsIdList.size() - 1) {
				readFilterLimitNewsQuery.append("?,");
			} else {
				readFilterLimitNewsQuery.append("?");
			}
		}

		readFilterLimitNewsQuery
				.append(") GROUP BY (NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE) ORDER BY COUNT(COMMENTS.COMMENT_ID) DESC, NEWS.NEWS_ID NULLS LAST)Q) SELECT * FROM NUMBERED_QUERY Q WHERE IDX > ? AND IDX <= ?");

		return readFilterLimitNewsQuery.toString();
	}

	public List<News> getLimitNewsByAuthor(int lowBorder, int heightBorder,
			SearchCriteriaVO searchCriteriaVO) throws DaoException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		List<News> newsList = new ArrayList<News>();

		try {
			connection = dataSource.getConnection();
			preparedStatement = connection
					.prepareStatement(READ_LIMIT_NEWS_BY_AUTHOR_QUERY);
			preparedStatement.setLong(1, searchCriteriaVO.getAuthorId());
			preparedStatement.setLong(2, lowBorder);
			preparedStatement.setLong(3, heightBorder);
			resultSet = preparedStatement.executeQuery();

			News news = null;
			while (resultSet.next()) {
				news = new News();

				news.setNewsId(resultSet.getLong(1));
				news.setShortText(resultSet.getString(2));
				news.setFullText(resultSet.getString(3));
				news.setTitle(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(5));
				news.setModificationDate(resultSet.getDate(6));

				newsList.add(news);
			}

			return newsList;

		} catch (SQLException e) {
			throw new DaoException(
					"SQL error in getting list of 'news' by author on current page.",
					e);
		} finally {
			DataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	public Long readNextFilerNews(Long currentNewsId,
			SearchCriteriaVO searchCriteriaVO) throws DaoException {
		List<Long> tagIdList = searchCriteriaVO.getTagsIdList();

		final String readNextFilterNewsQuery = concatNextFilterNewsQuery(tagIdList);

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Long tagId = null;
		int tagIdStartPosition = 2;

		Long nextNewsId = null;

		try {
			connection = dataSource.getConnection();
			preparedStatement = connection
					.prepareStatement(readNextFilterNewsQuery);
			preparedStatement.setLong(1, searchCriteriaVO.getAuthorId());
			for (int i = 0; i < tagIdList.size(); i++) {
				tagId = tagIdList.get(i);
				preparedStatement.setLong(i + tagIdStartPosition, tagId);
			}
			preparedStatement.setLong(tagIdList.size() + 2, currentNewsId);
			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {

				nextNewsId = resultSet.getLong(1);

			}

			return nextNewsId;

		} catch (SQLException e) {
			throw new DaoException("SQL error in reading next filter news.", e);
		} finally {
			DataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	private String concatNextFilterNewsQuery(List<Long> tagIdList) {
		StringBuilder readNextFilterNewsQuery = new StringBuilder(
				"WITH NUMBERED_QUERY AS ( SELECT Q.*, ROWNUM AS IDX FROM (SELECT NEWS.NEWS_ID FROM NEWS LEFT JOIN COMMENTS ON  NEWS.NEWS_ID = COMMENTS.NEWS_ID INNER JOIN NEWS_AUTHOR ON NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID INNER JOIN NEWS_TAG ON NEWS.NEWS_ID = NEWS_TAG.NEWS_ID WHERE NEWS_AUTHOR.AUTHOR_ID = ? AND NEWS_TAG.TAG_ID IN (");

		for (int i = 0; i < tagIdList.size(); i++) {
			if (i != tagIdList.size() - 1) {
				readNextFilterNewsQuery.append("?,");
			} else {
				readNextFilterNewsQuery.append("?");
			}
		}

		readNextFilterNewsQuery
				.append(")GROUP BY (NEWS.NEWS_ID) ORDER BY COUNT(COMMENTS.COMMENT_ID) DESC, NEWS.NEWS_ID NULLS LAST)Q) SELECT * FROM NUMBERED_QUERY Q, (SELECT MIN(IDX) AS IDX FROM NUMBERED_QUERY WHERE NEWS_ID = ?)NI WHERE Q.IDX = NI.IDX + 1");
		return readNextFilterNewsQuery.toString();
	}

	public Long readPrevFilterNews(Long currentNewsId,
			SearchCriteriaVO searchCriteriaVO) throws DaoException {
		List<Long> tagIdList = searchCriteriaVO.getTagsIdList();

		final String readNextFilterNewsQuery = concatPrevFilterNewsQuery(tagIdList);

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Long tagId = null;
		int tagIdStartPosition = 2;

		Long nextNewsId = null;

		try {
			connection = dataSource.getConnection();
			preparedStatement = connection
					.prepareStatement(readNextFilterNewsQuery);
			preparedStatement.setLong(1, searchCriteriaVO.getAuthorId());
			for (int i = 0; i < tagIdList.size(); i++) {
				tagId = tagIdList.get(i);
				preparedStatement.setLong(i + tagIdStartPosition, tagId);
			}
			preparedStatement.setLong(tagIdList.size() + 2, currentNewsId);
			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {

				nextNewsId = resultSet.getLong(1);

			}

			return nextNewsId;

		} catch (SQLException e) {
			throw new DaoException(
					"SQL error in reading previous filter news.", e);
		} finally {
			DataBaseUtil.closeResources(resultSet, preparedStatement,
					connection);
		}
	}

	private String concatPrevFilterNewsQuery(List<Long> tagIdList) {
		StringBuilder readNextFilterNewsQuery = new StringBuilder(
				"WITH NUMBERED_QUERY AS ( SELECT Q.*, ROWNUM AS IDX FROM (SELECT NEWS.NEWS_ID FROM NEWS LEFT JOIN COMMENTS ON  NEWS.NEWS_ID = COMMENTS.NEWS_ID INNER JOIN NEWS_AUTHOR ON NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID INNER JOIN NEWS_TAG ON NEWS.NEWS_ID = NEWS_TAG.NEWS_ID WHERE NEWS_AUTHOR.AUTHOR_ID = ? AND NEWS_TAG.TAG_ID IN (");

		for (int i = 0; i < tagIdList.size(); i++) {
			if (i != tagIdList.size() - 1) {
				readNextFilterNewsQuery.append("?,");
			} else {
				readNextFilterNewsQuery.append("?");
			}
		}

		readNextFilterNewsQuery
				.append(")GROUP BY (NEWS.NEWS_ID) ORDER BY COUNT(COMMENTS.COMMENT_ID) DESC, NEWS.NEWS_ID NULLS LAST)Q) SELECT * FROM NUMBERED_QUERY Q, (SELECT MIN(IDX) AS IDX FROM NUMBERED_QUERY WHERE NEWS_ID = ?)NI WHERE Q.IDX = NI.IDX - 1");
		return readNextFilterNewsQuery.toString();
	}
}