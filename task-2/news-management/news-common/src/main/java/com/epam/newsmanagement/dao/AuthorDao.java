package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DaoException;

/**
 * The second level of the hierarchy Data Access layer interfaces. This
 * interface <code>AuthorDao</code> describes the behavior of a particular dao
 * layer which working with instance of <code>Author</code>.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsmanagement.dao.GenericDao
 * @see com.epam.newsmanagement.domain.Author
 * @since 1.0
 */
public interface AuthorDao extends GenericDao<Author> {

	/**
	 * Search Author instance from Author table in data base by sent News
	 * instance.
	 *
	 * @param author
	 *            element of Author instance to search for Tag list
	 * @return list of Tag which was find by the Author instance
	 * @throws DaoException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see com.epam.newsportal.domain.Tag
	 */
	public Author searchAuthorByNews(News news) throws DaoException;
}