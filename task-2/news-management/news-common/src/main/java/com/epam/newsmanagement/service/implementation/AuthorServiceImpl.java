package com.epam.newsmanagement.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;

/**
 * Public class <code>AuthorServiceImpl</code> is an element of Service layer
 * and working with Author data base instance. This is a realization of
 * <code>AuthorService</code> interface and it performs all operations described
 * in this interface.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsmanagement.service.AuthorService
 * @see com.epam.newsmanagement.domain.Author
 * @since 1.0
 */
@Service
public class AuthorServiceImpl implements AuthorService {

	/**
	 * link to the interface part, providing access to basic operations
	 */
	@Autowired
	private AuthorDao authorDao;

	/**
	 * Override method used to add record to the table Author, which calls
	 * create() method from AuthorDao interface.
	 *
	 * @param author
	 *            instance which will be add to the data base
	 * @return unique identifier inserted instance
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 */

	public Long create(Author author) throws ServiceException {
		Long authorId = null;

		if (author == null) {
			throw new ServiceException("Author is empty.");
		}

		try {
			authorId = authorDao.create(author);

		} catch (DaoException e) {
			throw new ServiceException("Cannot create author.", e);
		}
		return authorId;
	}

	/**
	 * Override method used to read record from the table Author, which calls
	 * read() method from AuthorDao interface.
	 *
	 * @param authorId
	 *            unique identifier of instance which will be found
	 * @return find Author instance
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */

	public Author read(Long authorId) throws ServiceException {
		Author author = null;
		try {
			author = authorDao.read(authorId);

		} catch (DaoException e) {
			throw new ServiceException("Cannot read author.", e);
		}

		return author;
	}

	/**
	 * Override method used to read all records from the table Author, which
	 * calls readAll() method from AuthorDao interface.
	 *
	 * @return list of find Author instances
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */

	public List<Author> readAll() throws ServiceException {
		List<Author> authorList = null;

		try {
			authorList = authorDao.readAll();

		} catch (DaoException e) {
			throw new ServiceException("Cannot read all authors.", e);
		}

		return authorList;
	}

	/**
	 * Override method used to update record in the table Author, which calls
	 * update() method from AuthorDao interface.
	 *
	 * @param author
	 *            instance which will be update in the data base
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */

	public void update(Author author) throws ServiceException {
		if (author == null) {
			throw new ServiceException("Author is empty.");
		}

		try {
			authorDao.update(author);

		} catch (DaoException e) {
			throw new ServiceException("Cannot update author.", e);
		}
	}

	/**
	 * Override method used to delete record from the table Author, which calls
	 * delete() method from AuthorDao interface.
	 *
	 * @param authorId
	 *            unique identifier of instance which will be delete
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */

	public void delete(Long authorId) throws ServiceException {

		try {
			authorDao.delete(authorId);

		} catch (DaoException e) {
			throw new ServiceException("Cannot delete author.", e);
		}
	}

	public Author searchAuthorByNews(News news) throws ServiceException {
		Author author = null;

		if (news == null) {
			throw new ServiceException("News is empty.");
		}

		try {
			author = authorDao.searchAuthorByNews(news);

		} catch (DaoException e) {
			throw new ServiceException("Cannot search author by news.", e);
		}

		return author;
	}
}