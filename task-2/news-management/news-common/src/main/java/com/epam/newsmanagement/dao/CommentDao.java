package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.exception.DaoException;

/**
 * The second level of the hierarchy Data Access layer interfaces. This
 * interface <code>CommentDao</code> describes the behavior of a particular dao
 * layer which working with instance of <code>Comment</code>.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsmanagement.dao.GenericDao
 * @see com.epam.newsmanagement.domain.Comment
 * @since 1.0
 */
public interface CommentDao extends GenericDao<Comment> {

	/**
	 * Search list of Comments instance from Comment table in data base by sent
	 * News instance.
	 *
	 * @param author
	 *            element of Author instance to search for Tag list
	 * @return list of Tag which was find by the Author instance
	 * @throws DaoException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 * @see com.epam.newsportal.domain.Tag
	 */
	public List<Comment> searchCommentsByNews(News news) throws DaoException;

}