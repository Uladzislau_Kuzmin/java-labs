package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * The second level of the hierarchy Service layer interfaces. This interface
 * <code>TagService</code> describes the behavior of a particular service layer
 * which working with instance of <code>Tag</code> using Data Access layer
 * implementations of interface <code>TagDao</code>.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsmanagement.service.GenericService
 * @see com.epam.newsmanagement.dao.TagDao
 * @see com.epam.newsmanagement.domain.Tag
 * @since 1.0
 */
public interface TagService extends GenericService<Tag> {

	/**
	 * Defining search operation from Tag table in data base by sent Author
	 * instance, using the searchTagByAuthor() method in TagDao interface.
	 *
	 * @param author
	 *            element of Author instance to search for Tag list
	 * @return list of Tag which was find by the Author instance
	 * @@throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public List<Tag> searchTagsByAuthor(Author author) throws ServiceException;
	
	/**
	 * Defining search operation from Tag table in data base by sent News
	 * instance, using the searchTagByAuthor() method in TagDao interface.
	 *
	 * @param author
	 *            element of Author instance to search for Tag list
	 * @return list of Tag which was find by the Author instance
	 * @@throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>DaoException</code> throw up
	 * @see com.epam.newsmanagement.exception.DaoException
	 */
	public List<Tag> searchTagsByNews(News news) throws ServiceException;

}