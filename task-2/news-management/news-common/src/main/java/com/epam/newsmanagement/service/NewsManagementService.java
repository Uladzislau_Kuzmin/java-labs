package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.FullNewsVO;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteriaVO;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * General interface <code>NewsManagementService</code> describing transactional
 * operations for which, you must use a few simple Service layer interfaces.
 *
 *
 * The main interface in all Service layer interfaces. This interface
 * <code>NewsManagementService</code> describing transactional operations for
 * which, you must use a few simple Service layer interfaces implementations.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @since 1.0
 */
public interface NewsManagementService {

	public Long createAuthor(Author author) throws ServiceException;

	public Long createNews(News news) throws ServiceException;

	public Long createComment(Comment comment) throws ServiceException;

	public Long createTag(Tag tag) throws ServiceException;

	public Author readAuthor(Long authorId) throws ServiceException;

	public News readNews(Long newsId) throws ServiceException;

	public Comment readComment(Long commentId) throws ServiceException;

	public Tag readTag(Long tagId) throws ServiceException;

	public List<Author> readAllAuthors() throws ServiceException;

	public List<News> readAllNews() throws ServiceException;

	public List<Comment> readAllComments() throws ServiceException;

	public List<Tag> readAllTags() throws ServiceException;

	public void updateAuthor(Author author) throws ServiceException;

	public void updateNews(News news) throws ServiceException;

	public void updateComment(Comment comment) throws ServiceException;

	public void updateTag(Tag tag) throws ServiceException;

	public void deleteAuthor(Long authorId) throws ServiceException;

	public void deleteNews(Long newsId) throws ServiceException;

	public void deleteComment(Long commentId) throws ServiceException;

	public void deleteTag(Long tagId) throws ServiceException;

	public Author searchAuthorByNews(News news) throws ServiceException;

	public List<Comment> searchCommentsByNews(News news)
			throws ServiceException;

	public List<News> sortNewsByComments() throws ServiceException;

	public List<News> searchNewsByTag(Tag tag) throws ServiceException;

	public List<News> searchNewsByAuthor(Author author) throws ServiceException;

	public void bindNewsTag(News news, Tag tag) throws ServiceException;

	public void bindNewsAuthor(News news, Author author)
			throws ServiceException;

	public void bindNewsTagList(News news, List<Tag> tagList)
			throws ServiceException;

	public void deleteNewsList(List<Long> newsIdList) throws ServiceException;

	public List<Tag> searchTagsByAuthor(Author author) throws ServiceException;

	public List<Tag> searchTagsByNews(News news) throws ServiceException;

	public List<News> readAllNewsByFilter(SearchCriteriaVO searchCriteriaVO)
			throws ServiceException;

	public List<News> getLimitNewsByAuthor(int pageNumber, int newsOnPageCount,
			SearchCriteriaVO searchCriteriaVO) throws ServiceException;

	public Long readNextNews(Long currentNewsId) throws ServiceException;

	public Long readPrevNews(Long currentNewsId) throws ServiceException;

	public Long readNextFilerNews(Long currentNewsId,
			SearchCriteriaVO searchCriteriaVO) throws ServiceException;

	public Long readPrevFilterNews(Long currentNewsId,
			SearchCriteriaVO searchCriteriaVO) throws ServiceException;

	/**
	 * Save news with list of binding tags and author in one step. This is a
	 * transactional operation.
	 *
	 * @param news
	 *            News instance for saving in database
	 * @param author
	 *            Author instance for saving in database
	 * @param tagList
	 *            list of Tag instances for saving in database
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>ServiceException</code> throw up
	 * @see com.epam.newsmanagement.exception.ServiceException
	 */
	public void saveNewsVO(FullNewsVO fullNewsVO) throws ServiceException;

	/**
	 * Get list of FullNewsVO instance as one transactional operation.
	 *
	 * @throws ServiceException
	 *             user-defined exception occurs when any
	 *             <code>ServiceException</code> throw up
	 * @see com.epam.newsmanagement.exception.ServiceException
	 */

	public FullNewsVO getCurrentNewsVO(Long newsId) throws ServiceException;

	public List<FullNewsVO> getLimitNewsVO(int pageNumber, int newsOnPageCount)
			throws ServiceException;

	public List<FullNewsVO> getFilterLimitNews(int pageNumber,
			int newsOnPageCount, SearchCriteriaVO searchCriteriaVO)
			throws ServiceException;

}