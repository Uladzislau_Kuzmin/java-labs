package com.epam.newsmanagement.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;

/**
 * Public class <code>TagServiceImpl</code> is an element of Service layer and
 * working with Tag data base instance. This is a realization of
 * <code>TagService</code> interface and it performs all operations described in
 * this interface.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsmanagement.service.TagService
 * @see com.epam.newsmanagement.domain.Tag
 * @since 1.0
 */
@Service
public class TagServiceImpl implements TagService {

	/**
	 * link to the interface part, providing access to basic operations
	 */
	@Autowired
	private TagDao tagDao;

	/**
	 * Override method used to add record to the table Tag, which calls create()
	 * method from TagDao interface.
	 *
	 * @param tag
	 *            instance which will be add to the data base
	 * @return unique identifier inserted instance
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */

	public Long create(Tag tag) throws ServiceException {
		Long tagId = null;

		if (tag == null) {
			throw new ServiceException("Tag is empty.");
		}

		try {
			tagId = tagDao.create(tag);

		} catch (DaoException e) {
			throw new ServiceException("Cannot create tag.", e);
		}
		return tagId;
	}

	/**
	 * Override method used to read record from the table Tag, which calls
	 * read() method from TagDao interface.
	 *
	 * @param tagId
	 *            unique identifier of instance which will be found
	 * @return find Tag instance
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */

	public Tag read(Long tagId) throws ServiceException {
		Tag tag = null;

		try {
			tag = tagDao.read(tagId);

		} catch (DaoException e) {
			throw new ServiceException("Cannot read tag.", e);
		}

		return tag;
	}

	/**
	 * Override method used to read all records from the table Tag, which calls
	 * readAll() method from TagDao interface.
	 *
	 * @return list of find Tags instances
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */

	public List<Tag> readAll() throws ServiceException {

		List<Tag> tagList = null;
		try {
			tagList = tagDao.readAll();

		} catch (DaoException e) {
			throw new ServiceException("Cannot read all tags.", e);
		}

		return tagList;
	}

	/**
	 * Override method used to delete record from the table Tag, which calls
	 * update() method from TagDao interface.
	 *
	 * @param tag
	 *            instance which will be update in the data base
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */

	public void update(Tag tag) throws ServiceException {
		if (tag == null) {
			throw new ServiceException("Tag is empty.");
		}

		try {
			tagDao.update(tag);

		} catch (DaoException e) {
			throw new ServiceException("Cannot update tag.", e);
		}
	}

	/**
	 * Override method used to delete record from the table Tag, which calls
	 * delete() method from TagDao interface.
	 *
	 * @param tagId
	 *            unique identifier of instance which will be delete
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */

	public void delete(Long tagId) throws ServiceException {
		try {
			tagDao.delete(tagId);

		} catch (DaoException e) {
			throw new ServiceException("Cannot delete tag.", e);
		}
	}

	/**
	 * Override method used to search list of Tags record from the table Tag by
	 * Author instance. which calls searchTagsByAuthor() method from TagDao
	 * interface.
	 *
	 * @param author
	 *            element of Author instance to search for Tag list
	 * @return list of Tag which was find by the Author instance
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */

	public List<Tag> searchTagsByAuthor(Author author) throws ServiceException {
		List<Tag> tagList = null;

		if (author == null) {
			throw new ServiceException("Author is empty.");
		}

		try {
			tagList = tagDao.searchTagsByAuthor(author);

		} catch (DaoException e) {
			throw new ServiceException("Cannot search tag by author.", e);
		}

		return tagList;
	}
	
	/**
	 * Override method used to search list of Tags record from the table Tag by
	 * News instance. which calls searchTagssByNews() method from TagDao
	 * interface.
	 *
	 * @param author
	 *            element of Author instance to search for Tag list
	 * @return list of Tag which was find by the Author instance
	 * @throws ServiceLayerLogicException
	 *             user-defined exception occurs when submitted entity equals
	 *             null
	 * @throws ServiceLayerTechnicalException
	 *             user-defined exception occurs when any
	 *             <code>DaoLayerTechnicalException</code> throw up
	 */

	public List<Tag> searchTagsByNews(News news) throws ServiceException {
		List<Tag> tagList = null;

		if (news == null) {
			throw new ServiceException("News is empty.");
		}

		try {
			tagList = tagDao.searchTagsByNews(news);

		} catch (DaoException e) {
			throw new ServiceException("Cannot search tag by news.", e);
		}

		return tagList;
	}
	
}