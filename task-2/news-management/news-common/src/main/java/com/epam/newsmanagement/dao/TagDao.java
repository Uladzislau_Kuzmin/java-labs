package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.DaoException;

/**
 * The second level of the hierarchy Data Access layer interfaces. This
 * interface <code>TagDao</code> describes the behavior of a particular dao
 * layer which working with instance of <code>Tag</code>.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsmanagement.dao.GenericDao
 * @see com.epam.newsmanagement.domain.Tag
 * @since 1.0
 */
public interface TagDao extends GenericDao<Tag> {

	/**
	 * Search Tag instance from Tag table in data base by sent Author instance.
	 *
	 * @param author
	 *            element of Author instance to search for Tag list
	 * @return list of Tag which was find by the Author instance
	 * @throws DaoLayerTechnicalException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 * @see com.epam.newsportal.domain.Tag
	 */
	public List<Tag> searchTagsByAuthor(Author author) throws DaoException;

	/**
	 * Search Tag instance from Tag table in data base by sent News instance.
	 *
	 * @param author
	 *            element of Author instance to search for Tag list
	 * @return list of Tag which was find by the Author instance
	 * @throws DaoException
	 *             user-defined exception occurs when when any
	 *             <code>SQLException</code> throw up
	 * @see java.sql.SQLException
	 * @see com.epam.newsportal.domain.Tag
	 */
	public List<Tag> searchTagsByNews(News news) throws DaoException;
}