package com.epam.newsmanagement.dao.implementation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * Public class <code>TagDaoImplTest</code> content list of public Data Base
 * Unit tests, which check right work of Data Access layer class
 * <code>TagDao</code> methods.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsmanagement.dao.TagDao
 * @see com.epam.newsmanagement.domain.Tag
 * @since 1.0
 */

@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@ContextConfiguration(locations = { "/spring/spring.test.config.xml" })
@DatabaseSetup(value = "data.before.operations.xml")
public class TagDaoImplTest {

	/**
	 * reference on NewsDao class, to use it all public methods
	 */
	@Autowired
	private TagDao tagDao;

	/**
	 * Data Base Unit test, that checks work create() method in the TagDaoImpl
	 * class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void create() throws Exception {
		Long tagId = null;
		String tagName = "CustomerTag";

		Tag tag = new Tag();
		tag.setTagName(tagName);
		tagId = tagDao.create(tag);

		tag = tagDao.read(tagId);
		assertEquals(tag.getTagName(), tagName);
	}

	/**
	 * Data Base Unit test, that checks work read() method in the TagDaoImpl
	 * class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void read() throws Exception {
		Long tagId = 1L;
		String tagName = "FirstTag";

		Tag tag = tagDao.read(tagId);
		assertEquals(tag.getTagName(), tagName);
	}

	/**
	 * Data Base Unit test, that checks work readAll() method in the TagDaoImpl
	 * class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAll() throws Exception {
		String tagName = "SecondTag";
		List<Tag> tagList = tagDao.readAll();

		Tag tag = tagList.get(1);
		assertEquals(tag.getTagName(), tagName);
	}

	/**
	 * Data Base Unit test, that checks work update() method in the TagDaoImpl
	 * class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void update() throws Exception {
		Long tagId = 1L;
		String tagName = "SuperNewTag";

		Tag tag = new Tag();
		tag.setTagId(tagId);
		tag.setTagName(tagName);
		tagDao.update(tag);

		tag = tagDao.read(tagId);
		assertEquals(tag.getTagName(), tagName);
	}

	/**
	 * Data Base Unit test, that checks work delete() method in the TagDaoImpl
	 * class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void delete() throws Exception {
		Long tagId = 1L;

		Tag tag = new Tag();
		tag = tagDao.read(tagId);
		assertNotNull(tag);

		tagDao.delete(tagId);
		tag = tagDao.read(tagId);
		assertNull(tag);
	}

	/**
	 * Data Base Unit test, that checks work searchTagByAuthor() method in the
	 * TagDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchTagsByAuthor() throws Exception {
		Long authorId = 1L;
		String tagName = "FirstTag";

		Author author = new Author();
		author.setAuthorId(authorId);

		List<Tag> tagList = tagDao.searchTagsByAuthor(author);

		Tag tag = tagList.get(0);
		assertEquals(tag.getTagName(), tagName);
	}
}