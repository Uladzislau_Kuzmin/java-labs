package com.epam.newsmanagement.service.implementation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.domain.Comment;

/**
 * Public class <code>CommentServiceImplTest</code> content list of public
 * Mockito Unit tests, which check right work of Service layer class
 * <code>CommentServiceImpl</code> methods.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsmanagement.service.CommentService
 * @see com.epam.newsmanagement.service.implementation.CommentServiceImpl
 * @since 1.0
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration("/spring.config.xml")
public class CommentServiceImplTest {

	/**
	 * mock object, which will be inject into CommentServiceImpl class
	 */
	@Mock
	private CommentDao commentDao;

	/**
	 * service layer object, which we will testing
	 */
	@Autowired
	@InjectMocks
	private CommentServiceImpl commentService;

	/**
	 * Public Mockito Unit test, that verify work create() method in the
	 * CommentServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
    public void create() throws Exception {
    	Long commentId = null;
    	
        Comment comment = new Comment();
        when(commentDao.create(comment)).thenReturn(Long.valueOf(1L));
        
        commentId = commentService.create(comment);
        verify(commentDao, times(1)).create(comment);
        assertEquals(commentId,Long.valueOf(1L));
    }

	/**
	 * Public Mockito Unit test, that verify work read() method in the
	 * CommentServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void read() throws Exception {
		Long commentId = 1L;
		String commentText = "FourthText";
		Date creationDate = Timestamp.valueOf("2015-03-30 03:45:24");
		Long newsId = 2L;

		Comment comment = new Comment();
		comment.setCommentId(commentId);
		comment.setCommentText(commentText);
		comment.setCreationDate(creationDate);
		comment.setNewsId(newsId);
		when(commentDao.read(commentId)).thenReturn(comment);

		comment = commentDao.read(commentId);
		verify(commentDao, times(1)).read(commentId);
		assertEquals(comment.getCommentId(), commentId);
		assertEquals(comment.getCommentText(), commentText);
		assertEquals(comment.getCreationDate(), creationDate);
		assertEquals(comment.getNewsId(), newsId);
	}

	/**
	 * Public Mockito Unit test, that verify work readAll() method in the
	 * CommentServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAll() throws Exception {
		List<Comment> authorList;
		when(commentDao.readAll()).thenReturn(new ArrayList<Comment>());

		authorList = commentService.readAll();
		verify(commentDao, times(1)).readAll();
		assertNotNull(authorList);
	}

	/**
	 * Public Mockito Unit test, that verify work update() method in the
	 * CommentServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void update() throws Exception {

		Comment comment = new Comment();
		comment.setCommentText("I am super heavy comment text");

		commentService.update(comment);
		verify(commentDao, times(1)).update(comment);
	}

	/**
	 * Public Mockito Unit test, that verify work delete() method in the
	 * CommentServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void delete() throws Exception {
		Long commentId = 1L;

		commentService.delete(commentId);
		verify(commentDao, times(1)).delete(commentId);
	}
}