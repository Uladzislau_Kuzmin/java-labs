package com.epam.newsmanagement.service.implementation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;

/**
 * Public class <code>NewsServiceImplTest</code> content list of public Mockito
 * Unit tests, which check right work of Service layer class
 * <code>NewsManagerServiceImpl</code> methods.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsmanagement.service.NewsService
 * @see com.epam.newsmanagement.service.implementation.NewsManagerServiceImpl
 * @since 1.0
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration("/spring.config.xml")
public class NewsServiceImplTest {

	/**
	 * mock object, which will be inject into NewsServiceImpl class
	 */
	@Mock
	private NewsDao newsDao;

	/**
	 * service layer object, which we will testing
	 */
	@Autowired
	@InjectMocks
	private NewsServiceImpl newsService;

	/**
	 * Public Mockito Unit test, that verify work create() method in the
	 * NewsServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void create() throws Exception {
		Long newsId = null;

		News news = new News();
		when(newsDao.create(news)).thenReturn(Long.valueOf(1L));

		newsId = newsService.create(news);
		verify(newsDao, times(1)).create(news);
		assertEquals(newsId, Long.valueOf(1L));
	}

	/**
	 * Public Mockito Unit test, that verify work read() method in the
	 * NewsServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void read() throws Exception {
		Long newsId = 1L;
		String shortText = "NEW_SH1";
		String fullText = "NEW_FL1";
		String title = "NEW_T1";
		java.util.Date creationDate = Timestamp.valueOf("2015-03-29 03:45:24");
		java.util.Date modificationDate = Date.valueOf("2015-03-29");

		News news = new News();
		news.setNewsId(newsId);
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setTitle(title);
		news.setCreationDate(creationDate);
		news.setModificationDate(modificationDate);

		when(newsDao.read(newsId)).thenReturn(news);

		news = newsService.read(newsId);
		verify(newsDao, times(1)).read(newsId);
		assertEquals(news.getNewsId(), newsId);
		assertEquals(news.getShortText(), shortText);
		assertEquals(news.getFullText(), fullText);
		assertEquals(news.getTitle(), title);
		assertEquals(news.getCreationDate(), creationDate);
		assertEquals(news.getModificationDate(), modificationDate);

	}

	/**
	 * Public Mockito Unit test, that verify work readAll() method in the
	 * NewsServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAll() throws Exception {
		List<News> tagList;
		when(newsDao.readAll()).thenReturn(new ArrayList<News>());

		tagList = newsService.readAll();
		verify(newsDao, times(1)).readAll();
		assertNotNull(tagList);
	}

	/**
	 * Public Mockito Unit test, that verify work update() method in the
	 * NewsServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void update() throws Exception {
		News news = new News();

		newsService.update(news);
		verify(newsDao, times(1)).update(news);
	}

	/**
	 * Public Mockito Unit test, that verify work delete() method in the
	 * NewsServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void delete() throws Exception {
		Long newsId = 1L;
		
		newsService.delete(newsId);
		verify(newsDao, times(1)).delete(newsId);
	}

	/**
	 * Public Mockito Unit test, that verify work searchNewsByAuthor() method in
	 * the NewsServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchNewsByAuthor() throws Exception {
		List<News> newsList = null;

		Author author = new Author();
		
		when(newsDao.searchNewsByAuthor(author)).thenReturn(
				new ArrayList<News>());

		newsList = newsService.searchNewsByAuthor(author);
		verify(newsDao, times(1)).searchNewsByAuthor(author);
		assertNotNull(newsList);
	}

	/**
	 * Public Mockito Unit test, that verify work searchNewsByTag() method in
	 * the NewsServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchNewsByTag() throws Exception {
		List<News> newsList = null;

		Tag tag = new Tag();
		when(newsDao.searchNewsByTag(tag)).thenReturn(new ArrayList<News>());

		newsList = newsService.searchNewsByTag(tag);
		verify(newsDao, times(1)).searchNewsByTag(tag);
		assertNotNull(newsList);
	}

	/**
	 * Public Mockito Unit test, that verify work sortedNewsByComments() method
	 * in the NewsServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void sortNewsByComments() throws Exception {
		List<News> newsList = null;

		when(newsDao.sortNewsByComments()).thenReturn(new ArrayList<News>());

		newsList = newsService.sortNewsByComments();
		verify(newsDao, times(1)).sortNewsByComments();
		assertNotNull(newsList);
	}

	/**
	 * Public Mockito Unit test, that verify work createAuthorForNews() method
	 * in the AuthorServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void bindingNewsAuthor() throws Exception {

		News news = new News();
		Author author = new Author();
		
		newsService.bindNewsAuthor(news, author);
		verify(newsDao, times(1)).bindNewsAuthor(news, author);
	}

	/**
	 * Public Mockito Unit test, that verify work createTagForNews() method in
	 * the TagServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void bindingNewsTag() throws Exception {
		News news = new News();
		Tag tag = new Tag();

		newsService.bindNewsTag(news, tag);
		verify(newsDao, times(1)).bindNewsTag(news, tag);
	}
}