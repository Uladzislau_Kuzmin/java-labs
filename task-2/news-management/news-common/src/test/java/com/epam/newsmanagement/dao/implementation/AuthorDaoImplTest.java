package com.epam.newsmanagement.dao.implementation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.domain.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * Class <code>AuthorDaoImplTest</code> content list of public Data Base
 * Unit tests, which check right work of Data Access layer class
 * <codes>AuthorDao</code> methods.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsmanagement.dao.AuthorDao
 * @see com.epam.newsmanagement.domain.Author
 * @since 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@ContextConfiguration(locations = { "/spring/spring.test.config.xml" })
@DatabaseSetup(value = "data.before.operations.xml")
public class AuthorDaoImplTest {

	/**
	 * reference on AuthorDao class, to use it all public methods
	 */
	@Autowired
	private AuthorDao authorDao;	

	/**
	 * Data Base Unit test, that verify work create() method in the
	 * AuthorDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void create() throws Exception {
		Long authorId = null;
		String authorName = "Taylor";

		Author author = new Author();
		author.setAuthorName(authorName);

		authorId = authorDao.create(author);
		author = authorDao.read(authorId);
		
		assertEquals(author.getAuthorName(), authorName);
	}

	/**
	 * Data Base Unit test, that verify work read() method in the
	 * AuthorDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void read() throws Exception {
		Long authorId = 1L;
		String authorName = "Bob";

		Author author = new Author();
		author = authorDao.read(authorId);

		assertEquals(author.getAuthorName(), authorName);
	}

	/**
	 * Data Base Unit test, that verify work readAll() method in the
	 * AuthorDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAll() throws Exception {
		Long authorId = 3L;
		String authorName = "Robby";
		int expectedSize = 4;
		
		List<Author> authorList = authorDao.readAll();
		
		Author author = authorList.get(2);
		
		assertEquals(authorList.size(), expectedSize);
		assertEquals(author.getAuthorId(), authorId);
		assertEquals(author.getAuthorName(), authorName);
	}

	/**
	 * Data Base Unit test, that verify work update() method in the
	 * AuthorDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void update() throws Exception {
		Long authorId = 1L;
		String authorName = "SuperNewBob";
		
		Author author = new Author();
		author.setAuthorId(authorId);
		author.setAuthorName(authorName);

		authorDao.update(author);
		
		author = authorDao.read(authorId);
		assertEquals(author.getAuthorId(), authorId);
		assertEquals(author.getAuthorName(), authorName);
	}

	/**
	 * Data Base Unit test, that verify work delete() method in the
	 * AuthorDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void delete() throws Exception {
		Long authorId = 1L;
		
		Author author = new Author();
		author = authorDao.read(authorId);
		assertNotNull(author);
		
		authorDao.delete(authorId);
		author = authorDao.read(authorId);
		assertNull(author);
	}

}