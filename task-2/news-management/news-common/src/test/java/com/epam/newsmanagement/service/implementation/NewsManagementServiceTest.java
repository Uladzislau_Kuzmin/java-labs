package com.epam.newsmanagement.service.implementation;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.FullNewsVO;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;

/**
 * Public class <code>NewsManagementServiceTest</code> content list of public
 * Mockito Unit tests, which check right work of Service layer class
 * <code>NewsManagerServiceImpl</code> methods.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsmanagement.service.NewsManagementService
 * @see com.epam.newsmanagement.service.implementation.NewsManagerServiceImpl
 * @since 1.0
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration("/spring.config.xml")
public class NewsManagementServiceTest {

	/**
	 * authorService object, which will be inject into CommentServiceImpl class
	 */
	@Mock
	private AuthorService authorService;

	/**
	 * newsService object, which will be inject into CommentServiceImpl class
	 */
	@Mock
	private NewsService newsService;

	/**
	 * commentService object, which will be inject into CommentServiceImpl class
	 */
	@Mock
	private CommentService commentService;

	/**
	 * tagService object, which will be inject into CommentServiceImpl class
	 */
	@Mock
	private TagService tagService;

	/**
	 * service manager object, which we will testing
	 */
	@InjectMocks
	@Autowired
	private NewsManagerServiceImpl newsManagerService;

	/**
	 * Public Mockito Unit test, that verify work saveNews() method in the
	 * NewsManagerServiceImpl class. This method consists of calls few simple
	 * service methods.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void saveNews() throws Exception {

		News news = new News();
		Author author = new Author();
		List<Tag> tagList = new ArrayList<Tag>();
		for (int i = 0; i < 3; i++) {
			tagList.add(new Tag());
		}

		FullNewsVO fullNewsVO = new FullNewsVO();
		fullNewsVO.setNews(news);
		fullNewsVO.setAuthor(author);
		fullNewsVO.setTagList(tagList);

		newsManagerService.saveNewsVO(fullNewsVO);

		verify(newsService, times(1)).bindNewsAuthor(news, author);
		verify(newsService, times(1)).bindNewsTagList(news, tagList);
	}
}