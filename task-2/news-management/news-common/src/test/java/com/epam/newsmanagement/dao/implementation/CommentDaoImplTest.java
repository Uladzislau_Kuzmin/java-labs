package com.epam.newsmanagement.dao.implementation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.domain.Comment;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * Class <code>CommentDaoImplTest</code> content list of public Data Base Unit
 * tests, which check right work of Data Access layer class
 * <code>CommentDao</code> methods.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsmanagement.dao.CommentDao
 * @see com.epam.newsmanagement.domain.Comment
 * @since 1.0
 */

@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@ContextConfiguration(locations = { "/spring/spring.test.config.xml" })
@DatabaseSetup(value = "data.before.operations.xml")
public class CommentDaoImplTest {

	/**
	 * reference on CommentDao class, to use it all public methods
	 */
	@Autowired
	private CommentDao commentDao;

	/**
	 * Data Base Unit test, that verify work create() method in the
	 * CommentDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void create() throws Exception {
		Long commentId = null;
		String commentText = "FourthText";
		Date creationDate = Timestamp.valueOf("2015-03-30 03:45:24");
		Long newsId = 2L;

		Comment comment = new Comment();
		comment.setCommentText(commentText);
		comment.setCreationDate(creationDate);
		comment.setNewsId(newsId);
		commentId = commentDao.create(comment);

		comment = commentDao.read(commentId);
		assertEquals(comment.getCommentId(), commentId);
		assertEquals(comment.getCommentText(), commentText);
		assertEquals(comment.getCreationDate(), creationDate);
		assertEquals(comment.getNewsId(), newsId);
	}

	/**
	 * Data Base Unit test, that verify work read() method in the CommentDaoImpl
	 * class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void read() throws Exception {
		Long commentId = 1L;
		String commentText = "FirstText";
		Date creationDate = Timestamp.valueOf("2015-03-30 03:45:24");
		Long newsId = 2L;

		Comment comment = commentDao.read(commentId);

		assertEquals(comment.getCommentId(), commentId);
		assertEquals(comment.getCommentText(), commentText);
		assertEquals(comment.getCreationDate(), creationDate);
		assertEquals(comment.getNewsId(), newsId);
	}

	/**
	 * Data Base Unit test, that verify work readAll() method in the
	 * CommentDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAll() throws Exception {
		Long commentId = 2L;
		String commentText = "SecondText";
		Date creationDate = Timestamp.valueOf("2015-03-30 03:45:24");
		Long newsId = 2L;
		int expectedSize = 3;

		List<Comment> commentList = commentDao.readAll();

		Comment comment = commentList.get(1);

		assertEquals(commentList.size(), expectedSize);
		assertEquals(comment.getCommentId(), commentId);
		assertEquals(comment.getCommentText(), commentText);
		assertEquals(comment.getCreationDate(), creationDate);
		assertEquals(comment.getNewsId(), newsId);
	}

	/**
	 * Data Base Unit test, that verify work update() method in the
	 * CommentDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void update() throws Exception {
		Long commentId = 1L;
		String commentText = "SuperNewText";
		Date creationDate = Timestamp.valueOf("2015-03-29 03:45:24");
		Long newsId = 2L;

		Comment comment = new Comment();
		comment.setCommentId(commentId);
		comment.setCommentText(commentText);
		comment.setCreationDate(creationDate);
		comment.setNewsId(newsId);

		commentDao.update(comment);

		comment = commentDao.read(commentId);
		assertEquals(comment.getCommentId(), commentId);
		assertEquals(comment.getCommentText(), commentText);
		assertEquals(comment.getCreationDate(), creationDate);
		assertEquals(comment.getNewsId(), newsId);
	}

	/**
	 * Public Data Base Unit test, that verify work delete() method in the
	 * CommentDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void delete() throws Exception {
		Long commentId = 1L;

		Comment comment = new Comment();
		comment = commentDao.read(commentId);
		assertNotNull(comment);

		commentDao.delete(commentId);
		comment = commentDao.read(commentId);
		assertNull(comment);
	}
}