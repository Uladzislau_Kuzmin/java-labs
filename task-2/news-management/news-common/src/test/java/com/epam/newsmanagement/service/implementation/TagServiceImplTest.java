package com.epam.newsmanagement.service.implementation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;

/**
 * Public class <code>TagServiceImplTest</code> content list of public Mockito
 * Unit tests, which check right work of Service layer class
 * <code>TagServiceImpl</code> methods.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsmanagement.service.TagService
 * @see com.epam.newsmanagement.service.implementation.TagServiceImpl
 * @since 1.0
 */
@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTest {

	/**
	 * mock object, which will be inject into TagServiceImpl class
	 */
	@Mock
	private TagDao tagDao;

	/**
	 * service layer object, which we will testing
	 */
	@Autowired
	@InjectMocks
	private TagServiceImpl tagService;

	/**
	 * Public Mockito Unit test, that verify work create() method in the
	 * TagServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void create() throws Exception {
		Long tagId = null;

		Tag tag = new Tag();
		when(tagDao.create(tag)).thenReturn(Long.valueOf(1L));

		tagId = tagService.create(tag);
		verify(tagDao, times(1)).create(tag);
		assertEquals(tagId, Long.valueOf(1L));
	}

	/**
	 * Public Mockito Unit test, that verify work read() method in the
	 * TagServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void read() throws Exception {
		Long tagId = 1L;
		String tagName = "TagName";

		Tag tag = new Tag();
		tag.setTagId(tagId);
		tag.setTagName(tagName);
		when(tagDao.read(tagId)).thenReturn(tag);

		tag = tagService.read(tagId);
		verify(tagDao, times(1)).read(tagId);
		assertEquals(tag.getTagId(), tagId);
		assertEquals(tag.getTagName(), tagName);

	}

	/**
	 * Public Mockito Unit test, that verify work readAll() method in the
	 * TagServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAll() throws Exception {
		List<Tag> tagList;
		when(tagDao.readAll()).thenReturn(new ArrayList<Tag>());

		tagList = tagService.readAll();
		verify(tagDao, times(1)).readAll();
		assertNotNull(tagList);
	}

	/**
	 * Public Mockito Unit test, that verify work update() method in the
	 * TagServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void update() throws Exception {
		Tag tag = new Tag();

		tagService.update(tag);
		verify(tagDao, times(1)).update(tag);
	}

	/**
	 * Public Mockito Unit test, that verify work delete() method in the
	 * TagServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void delete() throws Exception {
		Long tagId = 1L;

		tagService.delete(tagId);
		verify(tagDao, times(1)).delete(tagId);
	}

	/**
	 * Public Mockito Unit test, that verify work searchTagByAuthor() method in
	 * the TagServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchTagsByAuthor() throws Exception {
		List<Tag> tagList = null;

		Author author = new Author();
		author.setAuthorName("Stalin!");

		when(tagDao.searchTagsByAuthor(author)).thenReturn(new ArrayList<Tag>());

		tagList = tagService.searchTagsByAuthor(author);
		verify(tagDao, times(1)).searchTagsByAuthor(author);
		assertNotNull(tagList);
	}
	
	
	/**
	 * Public Mockito Unit test, that verify work searchTagByAuthor() method in
	 * the TagServiceImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchTagsByNews() throws Exception {
		List<Tag> tagList = null;

		News news = new News();

		when(tagDao.searchTagsByNews(news)).thenReturn(new ArrayList<Tag>());

		tagList = tagService.searchTagsByNews(news);
		verify(tagDao, times(1)).searchTagsByNews(news);
		assertNotNull(tagList);
	}

}