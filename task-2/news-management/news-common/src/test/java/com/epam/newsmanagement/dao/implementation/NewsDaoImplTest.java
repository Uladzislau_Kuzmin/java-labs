package com.epam.newsmanagement.dao.implementation;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

/**
 * Public class <code>NewsDaoImplTest</code> content list of public Data Base
 * Unit tests, which check right work of Data Access layer class
 * <code>NewsDao</code> methods.
 *
 * @author Uladzislau Kuzmin
 * @version 1.0
 * @see com.epam.newsmanagement.dao.NewsDao
 * @see com.epam.newsmanagement.domain.News
 * @since 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@ContextConfiguration(locations = { "/spring/spring.test.config.xml"})
@DatabaseSetup(value = "data.before.operations.xml")
public class NewsDaoImplTest {

	/**
	 * reference on NewsDao class, to use it all public methods
	 */
	@Autowired
	private NewsDao newsDao;

	/**
	 * path to dataSets for comparison after CREATE operation
	 */
	private static final String DATA_AFTER_CREATE_PATH = "data.after.create.xml";
	
	/**
	 * NEWS_AUTHOR and NEWS_TAG tables name titles
	 */
	private static final String NEWS_AUTHOR_TABLE_NAME = "NEWS_AUTHOR";
	private static final String NEWS_TAG_TABLE_NAME = "NEWS_TAG";
	
	/**
	 * Data Base Unit test, that verify work create() method in the NewsDaoImpl
	 * class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void create() throws Exception {
		Long newsId = null;
		String shortText = "SH4";
		String fullText = "FL4";
		String title = "T4";
		java.util.Date creationDate = Timestamp.valueOf("2015-03-30 03:45:24");
		java.util.Date modificationDate = Date.valueOf("2015-03-30");

		News news = new News();
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setTitle(title);
		news.setCreationDate(creationDate);
		news.setModificationDate(modificationDate);
		newsId = newsDao.create(news);

		news = newsDao.read(newsId);
		assertEquals(news.getNewsId(), newsId);
		assertEquals(news.getShortText(), shortText);
		assertEquals(news.getFullText(), fullText);
		assertEquals(news.getTitle(), title);
		assertEquals(news.getCreationDate(), creationDate);
		assertEquals(news.getModificationDate(), modificationDate);
	}

	/**
	 * Data Base Unit test, that verify work read() method in the NewsDaoImpl
	 * class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void read() throws Exception {
		Long newsId = 1L;
		String shortText = "SH1";
		String fullText = "FL1";
		String title = "T1";
		java.util.Date creationDate = Timestamp.valueOf("2015-03-30 03:45:24");
		java.util.Date modificationDate = Date.valueOf("2015-03-30");

		News news;
		news = newsDao.read(newsId);
		assertEquals(news.getNewsId(), newsId);
		assertEquals(news.getShortText(), shortText);
		assertEquals(news.getFullText(), fullText);
		assertEquals(news.getTitle(), title);
		assertEquals(news.getCreationDate(), creationDate);
		assertEquals(news.getModificationDate(), modificationDate);
	}

	/**
	 * Public Data Base Unit test, that verify work readAll() method in the
	 * NewsDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void readAll() throws Exception {
		Long newsId = 3L;
		String shortText = "SH3";
		String fullText = "FL3";
		String title = "T3";
		java.util.Date creationDate = Timestamp.valueOf("2015-03-30 03:45:24");
		java.util.Date modificationDate = Date.valueOf("2015-03-30");
		int excpectedSize = 3;

		List<News> newsList = newsDao.readAll();

		News news;
		news = newsList.get(2);

		assertEquals(newsList.size(), excpectedSize);
		assertEquals(news.getNewsId(), newsId);
		assertEquals(news.getShortText(), shortText);
		assertEquals(news.getFullText(), fullText);
		assertEquals(news.getTitle(), title);
		assertEquals(news.getCreationDate(), creationDate);
		assertEquals(news.getModificationDate(), modificationDate);
	}

	/**
	 * Data Base Unit test, that verify work update() method in the NewsDaoImpl
	 * class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void update() throws Exception {
		Long newsId = 1L;
		String shortText = "NEW_SH1";
		String fullText = "NEW_FL1";
		String title = "NEW_T1";
		java.util.Date creationDate = Timestamp.valueOf("2015-03-29 03:45:24");
		java.util.Date modificationDate = Date.valueOf("2015-03-29");

		News news = new News();
		news.setNewsId(newsId);
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setTitle(title);
		news.setCreationDate(creationDate);
		news.setModificationDate(modificationDate);
		newsDao.update(news);

		news = newsDao.read(newsId);
		assertEquals(news.getNewsId(), newsId);
		assertEquals(news.getShortText(), shortText);
		assertEquals(news.getFullText(), fullText);
		assertEquals(news.getTitle(), title);
		assertEquals(news.getCreationDate(), creationDate);
		assertEquals(news.getModificationDate(), modificationDate);
	}

	/**
	 * Data Base Unit test, that verify work delete() method in the NewsDaoImpl
	 * class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void delete() throws Exception {
		Long newsId = 1L;

		News news = new News();
		news = newsDao.read(newsId);
		assertNotNull(news);

		newsDao.delete(newsId);
		news = newsDao.read(newsId);
		assertNull(news);
	}

	/**
	 * Public Data Base Unit test, that verify work searchNewsByAuthor() method
	 * in the NewsDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchNewsByAuthor() throws Exception {
		Long authorId = 1L;
		Long newsId = 1L;

		Author author = new Author();
		author.setAuthorId(authorId);

		List<News> newsList = null;
		newsList = newsDao.searchNewsByAuthor(author);

		News news = newsList.get(0);
		assertEquals(news.getNewsId(), newsId);
	}

	/**
	 * Data Base Unit test, that verify work searchNewsByTag() method in
	 * the NewsDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void searchNewsByTag() throws Exception {
		Long tagId = 1L;
		Long newsId = 1L;

		Tag tag = new Tag();
		tag.setTagId(tagId);

		List<News> newsList = null;
		newsList = newsDao.searchNewsByTag(tag);

		News news = newsList.get(0);
		assertEquals(news.getNewsId(), newsId);
	}

	/**
	 * Public Data Base Unit test, that verify work sortedNewsByComments()
	 * method in the NewsDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	public void sortNewsByComments() throws Exception {
		List<News> newsList = null;

		newsList = newsDao.sortNewsByComments();
		assertThat(newsList.get(0).getNewsId(), is(2L));
		assertThat(newsList.get(1).getNewsId(), is(3L));
		assertThat(newsList.get(2).getNewsId(), is(1L));
	}
	
	/**
	 * Data Base Unit test, that verify work createAuthorForNews() method
	 * in the AuthorDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	@ExpectedDatabase(value = DATA_AFTER_CREATE_PATH, table = NEWS_AUTHOR_TABLE_NAME, assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	public void bindNewsAuthor() throws Exception {
		Long newsId = 3L;
		Long authorId = 3L;
		News news = new News();
		Author author = new Author();
		
		news.setNewsId(newsId);
		author.setAuthorId(authorId);
		newsDao.bindNewsAuthor(news, author);
	}
	
	/**
	 * Data Base Unit test, that checks work createTagForNews() method in
	 * the TagDaoImpl class.
	 *
	 * @throws Exception
	 *             any exception that might throw up
	 */
	@Test
	@ExpectedDatabase(value = DATA_AFTER_CREATE_PATH, table = NEWS_TAG_TABLE_NAME, assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	public void bindNewsTag() throws Exception {
		Long newsId = 3L;
		Long tagId = 3L;
		
		News news = new News();
		news.setNewsId(newsId);
		
		Tag tag = new Tag();
		tag.setTagId(tagId);
		newsDao.bindNewsTag(news, tag);
	}
}