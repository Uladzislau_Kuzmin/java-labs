package com.epam.newsmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.FullNewsVO;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.SearchCriteriaVO;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.util.NewsUtilManager;

@Controller
@SessionAttributes({ "filter", "pageNumber" })
public class NewsListController {

	@Autowired
	private NewsManagementService newsManagementService;

	private static final int newsOnPage = 4;

	private static final String PAGE_NUMBER_ATTR = "pageNumber";

	private static final String AUTHOR_LIST_ATTR = "authorList";

	private static final String TAG_LIST_ATTR = "tagList";

	private static final String FULL_NEWS_LIST_ATTR = "fullNewsList";

	private static final String PAGE_COUNT_ATTR = "pagesCount";

	@ModelAttribute("filter")
	public SearchCriteriaVO createCommand() {
		return new SearchCriteriaVO();
	}

	@RequestMapping("/show-news")
	public String showNews(@RequestParam(defaultValue = "1") int pageNumber,
			@ModelAttribute("filter") SearchCriteriaVO searchCriteriaVO,
			Model model) throws ServiceException {

		List<News> newsList = null;
		List<FullNewsVO> fullNewsList = null;

		if (searchCriteriaVO.getAuthorId() == null
				&& searchCriteriaVO.getTagsIdList() == null) {
			fullNewsList = newsManagementService.getLimitNewsVO(pageNumber,
					newsOnPage);
			newsList = newsManagementService.readAllNews();
		} else {
			fullNewsList = newsManagementService.getFilterLimitNews(pageNumber,
					newsOnPage, searchCriteriaVO);
			newsList = newsManagementService
					.readAllNewsByFilter(searchCriteriaVO);
		}

		setPagesCount(newsList, model);
		setNewsList(fullNewsList, model);

		model.addAttribute(PAGE_NUMBER_ATTR, pageNumber);
		return "show-news";
	}

	@RequestMapping(value = "/delete-news", method = RequestMethod.POST)
	public String deleteNews(
			@RequestParam(value = "edit") List<Long> deletedNewsId, Model model)
			throws ServiceException {

		newsManagementService.deleteNewsList(deletedNewsId);
		return "redirect:/show-news";
	}

	@RequestMapping("/filter-news")
	public String filterNews(
			@RequestParam Long authorId,
			@RequestParam(value = "tagId", required = false) List<Long> tagsIdList,
			Model model) throws ServiceException {

		SearchCriteriaVO searchCriteriaVO = new SearchCriteriaVO(authorId,
				tagsIdList);
		model.addAttribute("filter", searchCriteriaVO);
		model.addAttribute("pageNumber", 1);
		return "redirect:/show-news";
	}

	@RequestMapping("/reset-filter")
	public String resetFilter(
			@ModelAttribute("filter") SearchCriteriaVO searchCriteriaVO,
			Model model) {
		model.addAttribute("filter", new SearchCriteriaVO());
		return "redirect:/show-news";

	}

	private void setNewsList(List<FullNewsVO> fullNewsList, Model model)
			throws ServiceException {

		List<Author> authorList = newsManagementService.readAllAuthors();
		List<Tag> tagList = newsManagementService.readAllTags();

		model.addAttribute(AUTHOR_LIST_ATTR, authorList);
		model.addAttribute(TAG_LIST_ATTR, tagList);
		model.addAttribute(FULL_NEWS_LIST_ATTR, fullNewsList);

	}

	private void setPagesCount(List<News> newsList, Model model)
			throws ServiceException {
		int pagesCount = 0;

		pagesCount = NewsUtilManager.getPagesCount(newsList, newsOnPage);

		model.addAttribute(PAGE_COUNT_ATTR, pagesCount);
	}

}
