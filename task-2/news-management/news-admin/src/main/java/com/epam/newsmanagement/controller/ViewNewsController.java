package com.epam.newsmanagement.controller;

import java.util.Calendar;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsmanagement.domain.Comment;
import com.epam.newsmanagement.domain.FullNewsVO;
import com.epam.newsmanagement.domain.SearchCriteriaVO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManagementService;

@Controller
@SessionAttributes({ "filter", "newsId" })
public class ViewNewsController {

	@Autowired
	private NewsManagementService newsManagementService;

	private static final String FULL_NEWS_ATTR = "fullNews";

	@RequestMapping("/view-selected-news/{newsId}")
	public String viewNews(@PathVariable Long newsId, Model model,
			HttpSession session) throws ServiceException {

		FullNewsVO fullNewsVO = newsManagementService.getCurrentNewsVO(newsId);
		model.addAttribute(FULL_NEWS_ATTR, fullNewsVO);
		model.addAttribute("newsId", newsId);
		return "view-news";
	}

	@RequestMapping("/delete-comment/{commentId}")
	public String deleteComment(@PathVariable Long commentId, Model model,
			@ModelAttribute("newsId") Long newsId) throws ServiceException {

		newsManagementService.deleteComment(commentId);
		return "redirect:/view-selected-news/" + newsId;
	}

	@RequestMapping(value = "/post-comment", method = RequestMethod.POST)
	public String postComment(@RequestParam String commentText, Model model,
			@ModelAttribute("newsId") Long newsId) throws ServiceException {

		Comment comment = new Comment();
		comment.setCommentText(commentText);
		comment.setCreationDate(Calendar.getInstance().getTime());
		comment.setNewsId(newsId);

		newsManagementService.createComment(comment);

		return "redirect:/view-selected-news/" + newsId;
	}

	@RequestMapping("/next-page")
	public String showNextPage(
			@ModelAttribute("filter") SearchCriteriaVO searchCriteriaVO,
			@ModelAttribute("newsId") Long newsId) throws ServiceException {

		String redirectPage = "redirect:/view-selected-news/";
		Long nextPageId = null;
		if (searchCriteriaVO.getAuthorId() == null
				&& searchCriteriaVO.getTagsIdList() == null) {

			nextPageId = newsManagementService.readNextNews(newsId);
		} else {
			nextPageId = newsManagementService.readNextFilerNews(newsId,
					searchCriteriaVO);
		}

		if (nextPageId == null) {
			redirectPage += newsId;
		} else {
			redirectPage += nextPageId;
		}

		return redirectPage;
	}

	@RequestMapping("/prev-page")
	public String showPrevPage(
			@ModelAttribute("filter") SearchCriteriaVO searchCriteriaVO,
			@ModelAttribute("newsId") Long newsId) throws ServiceException {

		String redirectPage = "redirect:/view-selected-news/";
		Long nextPageId = null;

		if (searchCriteriaVO.getAuthorId() == null
				&& searchCriteriaVO.getTagsIdList() == null) {
			nextPageId = newsManagementService.readPrevNews(newsId);
		} else {
			nextPageId = newsManagementService.readPrevFilterNews(newsId,
					searchCriteriaVO);
		}
		if (nextPageId == null) {
			redirectPage += newsId;
		} else {
			redirectPage += nextPageId;
		}

		return redirectPage;
	}

}
