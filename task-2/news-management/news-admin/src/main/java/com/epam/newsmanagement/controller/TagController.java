package com.epam.newsmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManagementService;

@Controller
public class TagController {

	@Autowired
	private NewsManagementService newsManagementService;

	@RequestMapping("/show-tags")
	public String showTagList(Model model) throws ServiceException {
		List<Tag> tagList = newsManagementService.readAllTags();
		model.addAttribute("tagList", tagList);
		return "show-tags";
	}

	@RequestMapping("/update-tag")
	public String updateTag(@RequestParam Long tagId,
			@RequestParam String tagName, Model model) throws ServiceException {

		Tag tag = new Tag();
		tag.setTagId(tagId);
		tag.setTagName(tagName);

		newsManagementService.updateTag(tag);
		return "redirect:/show-tags";
	}

	@RequestMapping("/delete-tag/{tagId}")
	public String editTag(@PathVariable Long tagId, Model model)
			throws ServiceException {
		newsManagementService.deleteTag(tagId);
		return "redirect:/show-tags";
	}

	@RequestMapping(value = "/save-tag", method = RequestMethod.POST)
	public String saveNews(@RequestParam String tagName, Model model)
			throws ServiceException {

		Tag tag = new Tag();
		tag.setTagName(tagName);
		newsManagementService.createTag(tag);
		return "redirect:/show-tags";
	}

}
