package com.epam.newsmanagement.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.domain.FullNewsVO;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManagementService;

@Controller
public class AddNewsController {

	@Autowired
	private NewsManagementService newsManagementService;

	@RequestMapping("/show-add-news-page")
	public String showAddNewsPage(Model model) throws ServiceException {
		List<Author> authorList = newsManagementService.readAllAuthors();
		List<Tag> tagList = newsManagementService.readAllTags();
		model.addAttribute("authorList", authorList);
		model.addAttribute("tagList", tagList);
		return "show-add-news-page";
	}

	@RequestMapping(value = "/save-news", method = RequestMethod.POST)
	public String saveNews(@RequestParam String title,
			@RequestParam String creationDate, @RequestParam String shortText,
			@RequestParam String fullText, @RequestParam Long authorId,
			@RequestParam(value = "tagId") List<Long> tagListId)
			throws ServiceException, ParseException {
		News news = null;
		Author author = null;
		Tag tag = null;
		List<Tag> tagsList = new ArrayList<Tag>();

		SimpleDateFormat datePattern = new SimpleDateFormat("dd/MM/YYYY");
		java.util.Date creation = datePattern.parse(creationDate);
		news = new News(shortText, fullText, title, creation, creation);
		author = new Author();
		author.setAuthorId(authorId);
		for (Long tagId : tagListId) {
			tag = new Tag();
			tag.setTagId(tagId);
			tagsList.add(tag);
		}

		FullNewsVO fullNewsVO = new FullNewsVO();
		fullNewsVO.setNews(news);
		fullNewsVO.setAuthor(author);
		fullNewsVO.setTagList(tagsList);

		newsManagementService.saveNewsVO(fullNewsVO);
		return "redirect:/show-news";
	}

}
