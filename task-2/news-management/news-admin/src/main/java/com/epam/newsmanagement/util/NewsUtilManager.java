package com.epam.newsmanagement.util;

import java.util.List;
import com.epam.newsmanagement.domain.News;

public class NewsUtilManager {

	public static int getPagesCount(List<News> newsList, int newsOnPageCount) {
		int pagesCount = (newsList.size() + (newsOnPageCount - 1))
				/ newsOnPageCount;
		return pagesCount;
	}
}
