package com.epam.newsmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsManagementService;

@Controller
public class AuthorController {

	@Autowired
	private NewsManagementService newsManagementService;

	@RequestMapping("/show-authors")
	public String showAuthorList(Model model) throws ServiceException {
		List<Author> authorList = newsManagementService.readAllAuthors();
		model.addAttribute("authorList", authorList);

		return "show-authors";
	}

	@RequestMapping("/update-author")
	public String updateTag(@RequestParam Long authorId,
			@RequestParam String authorName, Model model)
			throws ServiceException {

		Author author = new Author();
		author.setAuthorId(authorId);
		author.setAuthorName(authorName);

		newsManagementService.updateAuthor(author);
		return "redirect:/show-authors";
	}

	@RequestMapping("/delete-author/{authorId}")
	public String editTag(@PathVariable Long authorId, Model model)
			throws ServiceException {
		newsManagementService.deleteAuthor(authorId);
		return "redirect:/show-authors";
	}

	@RequestMapping(value = "/save-author", method = RequestMethod.POST)
	public String saveAuthor(@RequestParam String authorName, Model model)
			throws ServiceException {

		Author author = new Author();
		author.setAuthorName(authorName);
		newsManagementService.createAuthor(author);
		return "redirect:/show-authors";
	}
}
