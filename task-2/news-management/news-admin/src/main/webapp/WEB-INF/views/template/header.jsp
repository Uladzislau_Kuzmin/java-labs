<%@ page contentType="text/html;charset=windows-1251" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>	
<body>
	<header>
		<div class="title">
			<h1>
				<spring:message code="label.welcome.title" />
			</h1>
		</div>
		<div class="logout">
			<span class="welcome"><spring:message
					code="label.hello.message" />, Admin Admin</span> <input type="submit"
				name="Logout"
				value="<spring:message
					code="label.logout"/>">
		</div>
		<div class="locale">
			<span style="float: right"> <a href="?lang=en">EN</a> <a
				href="?lang=ru">RU</a>
			</span>
		</div>
	</header>
</body>
</html>
