<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>show-news-list</title>
</head>
<body>
	<form action="${pageContext.request.contextPath}/filter-news">
		<div class="search">
			<select class="author-select" name="authorId">
				<c:forEach items="${authorList}" var="author">
					<c:choose>
						<c:when test="${author.authorId == filter.authorId}">
							<option value="${author.authorId}" selected>${author.authorName}</option>
						</c:when>
						<c:otherwise>
							<option value="${author.authorId}">${author.authorName}</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select>
			<div class="multiselect">
				<div class="selectBox" onclick="showCheckboxes()">
					<select>
						<option><spring:message code="label.selectTag" /></option>
					</select>
					<div class="overSelect"></div>
				</div>
				<div id="checkboxes">
					<c:choose>
						<c:when test="${fn:length(filter.tagsIdList) == 0}">
							<c:forEach items="${tagList}" var="tag">
								<label><input type="checkbox" name="tagId"
									value="${tag.tagId}" />${tag.tagName}</label>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<c:forEach items="${tagList}" var="tag">
								<c:set var="count" value="0"></c:set>
								<c:forEach items="${filter.tagsIdList}" var="selectedId">
									<c:choose>
										<c:when test="${tag.tagId == selectedId }">
											<c:set var="count" value="${count+1}"></c:set>
										</c:when>
									</c:choose>
								</c:forEach>
								<c:choose>
									<c:when test="${count != 0}">
										<label><input type="checkbox" name="tagId"
											value="${tag.tagId}" checked="checked" />${tag.tagName}</label>
									</c:when>
									<c:otherwise>
										<label><input type="checkbox" name="tagId"
											value="${tag.tagId}" />${tag.tagName}</label>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
			<input type="submit" name="filter"
				value="<spring:message code="label.filter" />">
			<button>
				<a href="/news-admin/reset-filter"><spring:message
						code="label.reset" /></a>
			</button>
		</div>
	</form>
	<form action="${pageContext.request.contextPath}/delete-news"
		id="delete-news-form" method="post">
		<div class="news-list">
			<c:forEach items="${fullNewsList}" var="fullNews">
				<div class="news">
					<div class="news-row">
						<div class="news-title">
							<a href="/news-admin/view-selected-news/${fullNews.news.newsId}"><b>${fullNews.news.title}</b></a>
						</div>
						<div class="news-author">(by ${fullNews.author.authorName })</div>
						<div class="news-date">
							<fmt:formatDate value="${fullNews.news.creationDate}" />
						</div>
					</div>
					<div class="news-row">
						<div class="news-text">${fullNews.news.shortText}</div>
					</div>
					<div class="news-row">
						<div class="news-tags">
							<c:set var="count" value="0"></c:set>
							<c:forEach items="${fullNews.tagList}" var="tag">
								<c:set var="count" value="${count + 1}"></c:set>
								${tag.tagName}
								<c:choose>
									<c:when test="${count != fn:length(fullNews.tagList)}">,</c:when>
								</c:choose>
							</c:forEach>
						</div>
						<div class="news-comments">
							<spring:message code="label.comments" />
							(${fn:length(fullNews.commentList)})
						</div>
						<div class="news-edit">
							<a href="/news-admin/view-selected-news/${fullNews.news.newsId}"><spring:message
									code="label.edit" /></a> <input type="checkbox" name="edit"
								value="${fullNews.news.newsId}">
						</div>
					</div>
					<hr />
				</div>
			</c:forEach>
		</div>
		<div class="delete-submit">
			<button type="submit" name="delete" onclick="checkboxValidate()">
				<spring:message code="label.delete" />
			</button>
		</div>
	</form>
	<div class="page-select">
		<form action="show-news" method="post">
			<c:forEach var="i" begin="1" end="${pagesCount}">
				<input class="select-submit" type="submit" value="${i}"
					name="pageNumber">
			</c:forEach>
		</form>
	</div>
	<script type="text/javascript" src="resources/scripts/js.js"></script>
	<script src="resources/scripts/jquery-1.11.3.min.js"></script>
</body>
</html>