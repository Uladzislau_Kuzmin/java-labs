<%@ page contentType="text/html;charset=windows-1251" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
	<div class="nav">
		<div class="menu">
			<ul>
				<li><a href="/news-admin/reset-filter"><spring:message
							code="label.menu.newsList" /></a></li>
				<li><a href="/news-admin/show-add-news-page"><spring:message
							code="label.menu.addNews" /></a></li>
				<li><a href="/news-admin/show-authors"><spring:message
							code="label.menu.addUpdateAuthors" /></a></li>
				<li><a href="/news-admin/show-tags"><spring:message
							code="label.menu.addUpdateTags" /></a></li>
			</ul>
		</div>
	</div>
</body>
</html>