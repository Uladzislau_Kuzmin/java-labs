<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>add-news</title>
</head>
<body>
	<jsp:useBean id="now" class="java.util.Date" />

	<form action="save-news" id="save-news" method="post">
		<div class="news-wrapper">
			<div class="add-news-title">
				<spring:message code="label.title" />
				: <input type="text" maxlength="30" name="title" size="55" required>
			</div>
			<div class="add-news-date">
				<spring:message code="label.date" />
				: <input type="text"
					pattern="(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"
					name="creationDate" size="55"
					value="<fmt:formatDate type="date" value="${now}" pattern="dd/MM/YYYY"/>"
					required>
			</div>
			<div class="add-news-brief">
				<div class="add-news-brief-title">
					<spring:message code="label.brief" />
					:
				</div>
				<textarea rows="5" cols="57" name="shortText" maxlength="30"
					required></textarea>
			</div>
			<div class="add-news-content">
				<div class="add-news-content-title">
					<spring:message code="label.content" />
					:
				</div>
				<textarea rows="10" cols="57" name="fullText" maxlength="30"
					required></textarea>
			</div>
			<div class="add-search-list">
				<select class="author-select" name="authorId">
					<c:forEach items="${authorList}" var="author">
						<option value="${author.authorId}">${author.authorName}</option>
					</c:forEach>
				</select>
				<div class="fix-multi-select">
					<div class="selectBox" onclick="showCheckboxes()">
						<select>
							<option><spring:message code="label.selectTag" /></option>
						</select>
						<div class="overSelect"></div>
					</div>
					<div id="checkboxes">
						<c:forEach items="${tagList}" var="tag">
							<label><input type="checkbox" name="tagId"
								value="${tag.tagId}" />${tag.tagName}</label>
						</c:forEach>
					</div>
				</div>
			</div>
			<div class="add-news-submit">
				<input type="submit"
					value="<spring:message code="label.saveButton" />"
					onclick="saveNewsValidation()">
			</div>
		</div>
	</form>
	<script src="resources/scripts/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="resources/scripts/js.js"></script>
</body>
</html>