<%@ page contentType="text/html;charset=windows-1251" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>show-authors</title>
</head>
<body>
	<div class="items-wrapper">
		<c:forEach items="${authorList}" var="author">
			<form action="update-author" class="author-list">
				<b>Author : </b><input class="authorName" type="text"
					name="authorName" id="${author.authorId}"
					value="${author.authorName}" disabled="disabled">
				<div class="edit-button" id="${author.authorId}">
					<input class="href-btn" type="button" id="${author.authorId}"
						value="edit" onclick="showEditButtons(this)">
				</div>
				<div class="show-buttons" id="${author.authorId}">

					<input class="href-btn" type="submit" value="update">
					<button class="href-btn" value="delete">
						<a href="/news-admin/delete-author/${author.authorId}"> delete
						</a>
					</button>
					<input class="href-btn" id="${author.authorId}" type="button"
						value="cancel" onclick="showEditButtons(this)"> <input
						type="hidden" value="${author.authorId}" name="authorId">
					<br>
				</div>
			</form>
		</c:forEach>
	</div>
	<div class="save-wrapper">
		<form action="save-author" method="post">
			<b>Save author : </b><input type="text" name="authorName" required>
			<input class="href-btn" type="submit" value="save">
		</form>
	</div>
	<script type="text/javascript" src="resources/scripts/js.js"></script>
	<script src="resources/scripts/jquery-1.11.3.min.js"></script>
</body>
</html>