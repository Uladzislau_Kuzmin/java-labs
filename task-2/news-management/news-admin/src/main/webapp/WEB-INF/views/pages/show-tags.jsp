<%@ page contentType="text/html;charset=windows-1251" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>show-tags</title>
</head>
<body>
	<div class="items-wrapper">
		<c:forEach items="${tagList}" var="tag">
			<form action="update-tag" class="author-list">
				<b><spring:message code="label.tag" /> : </b> <input
					class="authorName" type="text" name="tagName" id="${tag.tagId}"
					value="${tag.tagName}" disabled="disabled">

				<div class="edit-button" id="${tag.tagId}">
					<input class="href-btn" type="button" id="${tag.tagId}"
						value="<spring:message code="label.editButton" />"
						onclick="showEditButtons(this)">
				</div>

				<div class="show-buttons" id="${tag.tagId}">
					<input class="href-btn" type="submit"
						value="<spring:message code="label.updateButton" />">
						
				
						<button class="href-btn">
							<a href="/news-admin/delete-tag/${tag.tagId}">
							<spring:message code="label.deleteButton" />
							</a>
						</button>

					 <input class="href-btn" id="${tag.tagId}" type="button"
						value="<spring:message code="label.cancelButton" />"
						onclick="showEditButtons(this)"> <input type="hidden"
						value="${tag.tagId}" name="tagId"> <br>
				</div>
			</form>
		</c:forEach>
	</div>

	<div class="save-wrapper">
		<form action="save-tag" method="post">
			<b><spring:message code="label.saveTag" /> : </b><input type="text"
				name="tagName" required> <input class="href-btn"
				type="submit" value="<spring:message code="label.saveButton" />">
		</form>
	</div>
	<script type="text/javascript" src="resources/scripts/js.js"></script>
	<script src="resources/scripts/jquery-1.11.3.min.js"></script>
</body>
</html>