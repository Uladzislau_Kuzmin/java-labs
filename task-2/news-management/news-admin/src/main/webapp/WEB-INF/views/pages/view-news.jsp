<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>view-news</title>
</head>

<body>
	<div class="full-news-wrapper">
		<div class="full-news-title">${fullNews.news.title}</div>
		<div class="full-news-author">(by ${fullNews.author.authorName})</div>
		<div class="full-news-date">
			<fmt:formatDate value="${fullNews.news.creationDate}" />
		</div>
		<div class="full-news-text">${fullNews.news.fullText}</div>
	</div>

	<div class="comments-wrapper">
		<c:forEach var="comment" items="${ fullNews.commentList}">
			<div class="comment">
				<div class="comment-date">
					<fmt:formatDate value="${comment.creationDate}" />
				</div>
				<div class="comment-content">
					<div class="comment-delete">
						<a href="/news-admin/delete-comment/${comment.commentId}">
							<button type="submit" name="commentId"
								value="${comment.commentId}">X</button>
						</a>
					</div>
					<div class="comment-text">${comment.commentText}</div>
				</div>
			</div>
		</c:forEach>
		<form action="${pageContext.request.contextPath}/post-comment"
			method="post">
			<div class="comment-post">
				<textarea rows="5" name="commentText" required></textarea>
				<input type="submit" value="Post comment">
			</div>
		</form>
	</div>

	<div class="next-prev-wrapper">
		<a href="/news-admin/prev-page"><input type="submit" class="prev"
			value="PREVIOUS"></a> <a href="/news-admin/next-page"><input
			type="submit" class="next" value="NEXT"></a>
	</div>
</body>

</html>
