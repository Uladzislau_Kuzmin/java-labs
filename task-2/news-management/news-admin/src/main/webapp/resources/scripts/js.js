var expanded = false;
function showCheckboxes() {
	var checkboxes = document.getElementById('checkboxes');
	if (!expanded) {
		checkboxes.style.display = "inline-block";
		expanded = true;
	} else {
		checkboxes.style.display = "none";
		expanded = false;
	}
}

function showEditButtons(clickedButton) {
	var clickedValue = clickedButton.value;
	var clickedId = clickedButton.id;
	if (clickedValue == "edit") {

		$("#" + clickedId + ".edit-button").css("display", "none").css(
				'z-index', 20);
		$("#" + clickedId + ".show-buttons").css("display", "block").css(
				'z-index', 21);
		$("#" + clickedId + ".authorName").removeAttr("disabled");

	} else if (clickedValue == "cancel") {

		$("#" + clickedId + ".show-buttons").css("display", "none").css(
				'z-index', 20);
		$("#" + clickedId + ".edit-button").css("display", "block").css(
				'z-index', 21);
		$("#" + clickedId + ".authorName").attr("disabled", "disabled");

	}
}

function checkboxValidate() {
	var clickCount = 0;
	$("#delete-news-form").submit(function(event) {
		clickCount++;
		event.preventDefault();

		var validateStatus = true;
		if ($("input:checked").length == 0) {
			if (clickCount == 1) {
				alert('You must select at least 1 checkbox!');
			}
			validateStatus = false;
		}

		if (validateStatus) {
			this.submit();
		}
	});
	clickCount = 0;
}

function saveNewsValidation() {
	var clickCount = 0;
	$("#save-news").submit(function(event) {
		clickCount++;
		event.preventDefault();

		var validateStatus = true;
		if ($("#checkboxes input:checked").length == 0) {
			if (clickCount == 1) {
				alert('You must select at least 1 tag for your news!');
			}
			validateStatus = false;
		}

		if (validateStatus) {
			this.submit();
		}
	});
	clickCount = 0;
}